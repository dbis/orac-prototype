package info.dbis.orac.view.rest.helper;

import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.AuthorizationException;
import info.dbis.orac.exception.ControllerException;
import info.dbis.orac.exception.PersistenceException;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

/**
 * RestApiExceptionHandling
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Stateless
public class RestApiExceptionHandling {

    @Resource
    private EJBContext context;

    public Response handleException(Exception e){

        // rollback transaction -> no data will be persisted if context.setRollbackOnly() is invoked
        this.context.setRollbackOnly();

        if (e instanceof PersistenceException){
            e.printStackTrace();
            PersistenceException pe = (PersistenceException) e;
            if (pe.getCode().equals(PersistenceException.ErrorCode.DATA_NOT_FOUND)){
                return Response.status(Response.Status.NOT_FOUND).entity("Persistence Exception: Data not found!").build();
            }
            else if (pe.getCode().equals(PersistenceException.ErrorCode.DATA_INCOMPLETE) || pe.getCode().equals(PersistenceException.ErrorCode.DATA_INVALID)){
                return Response.status(Response.Status.BAD_REQUEST).entity("Persistence Exception: Data is incomplete or invalid!").build();
            }
            else {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Persistence Exception: "+e.getMessage()).build();
            }
        }
        else if (e instanceof ControllerException) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity("Controller Exception: "+e.getMessage()).build();
        }
        else if (e instanceof AuthenticationException || e instanceof AuthorizationException) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }
        else {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }
}
