package info.dbis.orac.view.rest;

import info.dbis.orac.control.AuthenticationService;
import info.dbis.orac.control.UseCaseService;
import info.dbis.orac.datamodel.dto.UseCaseDTO;
import info.dbis.orac.view.rest.helper.RestApiExceptionHandling;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * UseCaseServiceEndpoint
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
@Path("/system/useCase")
@Stateless
public class UseCaseServiceEndpoint {

    @Inject
    private UseCaseService useCaseService;

    @Inject
    private AuthenticationService authenticationService;

    @Inject
    private RestApiExceptionHandling restApiExceptionHandling;

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response initiateUseCase(@QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            UseCaseDTO useCaseDTO = this.useCaseService.createUseCase(agentId, roleAssignmentId);
            return Response.status(Response.Status.OK).entity(useCaseDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }
}
