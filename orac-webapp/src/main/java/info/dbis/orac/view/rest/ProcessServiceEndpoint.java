package info.dbis.orac.view.rest;

import info.dbis.orac.control.AuthenticationService;
import info.dbis.orac.control.ProcessService;
import info.dbis.orac.control.SystemService;
import info.dbis.orac.datamodel.dto.*;
import info.dbis.orac.view.rest.helper.RestApiExceptionHandling;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * ProcessServiceEndpoint
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Path("/system/recruitmentProcess")
@Stateless
public class ProcessServiceEndpoint {

    @Inject
    private SystemService systemService;

    @Inject
    private ProcessService processService;

    @Inject
    private AuthenticationService authenticationService;

    @Inject
    private RestApiExceptionHandling restApiExceptionHandling;

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addRecruitmentProcess(@QueryParam("roleAssignmentId") Long roleAssignmentId, RecruitmentProcessDTO recruitmentProcessDTO){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            RecruitmentProcessDTO createdRecruitmentProcessDTO = this.processService.addRecruitmentProcess(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), recruitmentProcessDTO);
            return Response.status(Response.Status.OK).entity(createdRecruitmentProcessDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/{recruitmentProcessId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRecruitmentProcess(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            RecruitmentProcessDTO recruitmentProcessDTO = this.processService.getRecruitmentProcess(agentId, roleAssignmentId, recruitmentProcessId);
            return Response.status(Response.Status.OK).entity(recruitmentProcessDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @DELETE
    @Path("/{recruitmentProcessId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeRecruitmentProcess(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            this.processService.removeRecruitmentProcess(agentId, roleAssignmentId, recruitmentProcessId, this.systemService.getHumanResourceSystemId());
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRecruitmentProcesses(@QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            List<RecruitmentProcessDTO> recruitmentProcessDTOList = this.processService.getRecruitmentProcesses(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId());
            return Response.status(Response.Status.OK).entity(recruitmentProcessDTOList).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/{recruitmentProcessId}/jobOffer/{jobOfferId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJobOffer(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @PathParam("jobOfferId") Long jobOfferId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            JobOfferDTO jobOfferDTO = this.processService.getJobOffer(agentId, roleAssignmentId, jobOfferId);
            return Response.status(Response.Status.OK).entity(jobOfferDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @PUT
    @Path("/{recruitmentProcessId}/jobOffer/{jobOfferId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateJobOffer(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @PathParam("jobOfferId") Long jobOfferId, @QueryParam("roleAssignmentId") Long roleAssignmentId, JobOfferDTO jobOfferDTO){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            JobOfferDTO updatedJobOfferDTO = this.processService.updateJobOffer(agentId, roleAssignmentId, jobOfferId, jobOfferDTO);
            return Response.status(Response.Status.OK).entity(updatedJobOfferDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/{recruitmentProcessId}/jobOffer/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJobOffers(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            List<JobOfferDTO> jobOfferDTOList = this.processService.getJobOffers(agentId, roleAssignmentId, recruitmentProcessId);
            return Response.status(Response.Status.OK).entity(jobOfferDTOList).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @POST
    @Path("/{recruitmentProcessId}/jobApplication/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addJobApplication(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @QueryParam("roleAssignmentId") Long roleAssignmentId, JobApplicationDTO jobApplicationDTO){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            JobApplicationDTO createdJobApplicationDTO = this.processService.addJobApplication(agentId, roleAssignmentId, recruitmentProcessId, jobApplicationDTO);
            return Response.status(Response.Status.OK).entity(createdJobApplicationDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/{recruitmentProcessId}/jobApplication/{jobApplicationId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJobApplication(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @PathParam("jobApplicationId") Long jobApplicationId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            JobApplicationDTO jobApplicationDTO = this.processService.getJobApplication(agentId, roleAssignmentId, jobApplicationId);
            return Response.status(Response.Status.OK).entity(jobApplicationDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @PUT
    @Path("/{recruitmentProcessId}/jobApplication/{jobApplicationId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateJobApplication(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @PathParam("jobApplicationId") Long jobApplicationId, @QueryParam("roleAssignmentId") Long roleAssignmentId, JobApplicationDTO jobApplicationDTO){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            JobApplicationDTO updatedJobApplicationDTO = this.processService.updateJobApplication(agentId, roleAssignmentId, jobApplicationId, jobApplicationDTO);
            return Response.status(Response.Status.OK).entity(updatedJobApplicationDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/{recruitmentProcessId}/jobApplication/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJobApplications(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            List<JobApplicationDTO> jobApplicationDTOList = this.processService.getJobApplications(agentId, roleAssignmentId, recruitmentProcessId);
            return Response.status(Response.Status.OK).entity(jobApplicationDTOList).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @POST
    @Path("/{recruitmentProcessId}/jobApplication/{jobApplicationId}/jobApplicationReview")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addJobApplicationReview(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @QueryParam("roleAssignmentId") Long roleAssignmentId, JobApplicationReviewDTO jobApplicationReviewDTO){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            JobApplicationReviewDTO createdJobApplicationReviewDTO = this.processService.addJobApplicationReview(agentId, roleAssignmentId, recruitmentProcessId, jobApplicationReviewDTO);
            return Response.status(Response.Status.OK).entity(createdJobApplicationReviewDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/{recruitmentProcessId}/jobApplication/{jobApplicationId}/jobApplicationReview/{jobApplicationReviewId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJobApplicationReview(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @PathParam("jobApplicationId") Long jobApplicationId, @PathParam("jobApplicationReviewId") Long jobApplicationReviewId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            JobApplicationReviewDTO jobApplicationReviewDTO = this.processService.getJobApplicationReview(agentId, roleAssignmentId, jobApplicationReviewId);
            return Response.status(Response.Status.OK).entity(jobApplicationReviewDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @PUT
    @Path("/{recruitmentProcessId}/jobApplication/{jobApplicationId}/jobApplicationReview/{jobApplicationReviewId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateJobApplicationReview(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @PathParam("jobApplicationId") Long jobApplicationId, @PathParam("jobApplicationReviewId") Long jobApplicationReviewId, @QueryParam("roleAssignmentId") Long roleAssignmentId, JobApplicationReviewDTO jobApplicationReviewDTO){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            JobApplicationReviewDTO updatedJobApplicationReviewDTO = this.processService.updateJobApplicationReview(agentId, roleAssignmentId, jobApplicationReviewId, jobApplicationReviewDTO);
            return Response.status(Response.Status.OK).entity(updatedJobApplicationReviewDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/{recruitmentProcessId}/jobApplication/{jobApplicationId}/jobApplicationReview/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJobApplicationReviews(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @PathParam("jobApplicationId") Long jobApplicationId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            List<JobApplicationReviewDTO> jobApplicationReviewDTOS = this.processService.getJobApplicationReviews(agentId, roleAssignmentId, jobApplicationId);
            return Response.status(Response.Status.OK).entity(jobApplicationReviewDTOS).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @POST
    @Path("/{recruitmentProcessId}/contractOffer/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addContractOffer(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @QueryParam("roleAssignmentId") Long roleAssignmentId, @QueryParam("applicantId") Long applicantId, ContractOfferDTO contractOfferDTO){
        try {
            if (roleAssignmentId == null || applicantId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            ContractOfferDTO createdContractOfferDTO = this.processService.addContractOffer(agentId, roleAssignmentId, recruitmentProcessId, contractOfferDTO, applicantId);
            return Response.status(Response.Status.OK).entity(createdContractOfferDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/{recruitmentProcessId}/contractOffer/{contractOfferId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContractOffer(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @PathParam("contractOfferId") Long contractOfferId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            ContractOfferDTO contractOfferDTO = this.processService.getContractOffer(agentId, roleAssignmentId, contractOfferId);
            return Response.status(Response.Status.OK).entity(contractOfferDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @PUT
    @Path("/{recruitmentProcessId}/contractOffer/{contractOfferId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateContractOffer(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @PathParam("contractOfferId") Long contractOfferId, @QueryParam("roleAssignmentId") Long roleAssignmentId, ContractOfferDTO contractOfferDTO){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            ContractOfferDTO updatedContractOfferDTO = this.processService.updateContractOffer(agentId, roleAssignmentId, contractOfferId, contractOfferDTO);
            return Response.status(Response.Status.OK).entity(updatedContractOfferDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/{recruitmentProcessId}/contractOffer/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContractOffers(@PathParam("recruitmentProcessId") Long recruitmentProcessId, @QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            List<ContractOfferDTO> contractOfferDTOList = this.processService.getContractOffers(agentId, roleAssignmentId, recruitmentProcessId);
            return Response.status(Response.Status.OK).entity(contractOfferDTOList).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

}
