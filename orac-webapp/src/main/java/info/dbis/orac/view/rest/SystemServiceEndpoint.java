package info.dbis.orac.view.rest;

import info.dbis.orac.control.AuthenticationService;
import info.dbis.orac.control.SystemService;
import info.dbis.orac.datamodel.dto.HumanResourceSystemDTO;
import info.dbis.orac.datamodel.dto.authentication.UserDTO;
import info.dbis.orac.view.rest.helper.RestApiExceptionHandling;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Set;

/**
 * RestServiceEndpoint
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Path("/system")
@Stateless
public class SystemServiceEndpoint {

    @Inject
    private SystemService systemService;

    @Inject
    private AuthenticationService authenticationService;

    @Inject
    private RestApiExceptionHandling restApiExceptionHandling;

    @GET
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHumanResourceSystem(@QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            HumanResourceSystemDTO humanResourceSystemDTO = this.systemService.getHumanResourceSystem(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId());
            return Response.status(Response.Status.OK).entity(humanResourceSystemDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @POST
    @Path("/user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@QueryParam("roleAssignmentId") Long roleAssignmentId, UserDTO userDTO){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            UserDTO persistedUserDTO = this.systemService.addUser(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), userDTO);
            return Response.status(Response.Status.OK).entity(persistedUserDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/user/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@QueryParam("roleAssignmentId") Long roleAssignmentId, @PathParam("userId") Long userId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            UserDTO userDTO = this.systemService.getUser(agentId, roleAssignmentId, userId);
            return Response.status(Response.Status.OK).entity(userDTO).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

    @GET
    @Path("/user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(@QueryParam("roleAssignmentId") Long roleAssignmentId){
        try {
            if (roleAssignmentId == null){
                return Response.status(Response.Status.BAD_REQUEST).entity("You must set the roleAssignmentId").build();
            }
            Long agentId = this.authenticationService.getCurrentUserId();
            List<UserDTO> userDTOList = this.systemService.getUsers(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId());
            return Response.status(Response.Status.OK).entity(userDTOList).build();
        } catch (Exception e) {
            return this.restApiExceptionHandling.handleException(e);
        }
    }

}
