package info.dbis.orac.view.security;

import info.dbis.orac.control.AuthenticationService;
import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.PersistenceException;

import java.io.IOException;
import java.util.Base64;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * RestAuthenticationFilter
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class RestAuthenticationFilter implements javax.servlet.Filter {

    public static final String AUTHENTICATION_HEADER = "Authorization";

    @NotNull
    private AuthenticationService authenticationService;

    @Inject
    private void setAuthenticationService(AuthenticationService authenticationService){
        this.authenticationService = authenticationService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filter) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            String authCredentials = httpServletRequest.getHeader(AUTHENTICATION_HEADER);

            boolean authenticationStatus = false;
            String errorMessage = "";

            if (null != authCredentials) {
                // header value format will be "Basic encodedstring" for Basic
                // authentication. Example "Basic YWRtaW46YWRtaW4="
                final String encodedUserPassword = authCredentials.replaceFirst("Basic"
                        + " ", "");
                String usernameAndPassword = null;
                try {
                    byte[] decodedBytes = Base64.getDecoder().decode(
                            encodedUserPassword);
                    usernameAndPassword = new String(decodedBytes, "UTF-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
                final String email = tokenizer.nextToken();
                final String password = tokenizer.nextToken();

                try {
                    this.authenticationService.authenticate(email, password);
                    authenticationStatus = true;
                    System.out.println("User authenticated!");
                    System.out.println("User "+this.authenticationService.getCurrentUserId());
                }
                catch (AuthenticationException | PersistenceException ae){
                    errorMessage = ae.getMessage();
                }

            }

            if (authenticationStatus) {
                filter.doFilter(request, response);
            } else {
                if (response instanceof HttpServletResponse) {
                    HttpServletResponse httpServletResponse = (HttpServletResponse) response;
                    httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, errorMessage);
                }
            }
        }
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }
}
