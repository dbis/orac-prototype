package info.dbis.orac.authorization;

import info.dbis.orac.common.QueryParameter;
import info.dbis.orac.datamodel.dto.GuardedEntityDTO;
import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authentication.User;
import info.dbis.orac.datamodel.authorization.*;
import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.AuthorizationException;
import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.validation.constraints.NotNull;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.logging.Logger;

/**
 * AccessControlInterceptor
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Interceptor
@RequiresAccessControl
public class AccessControlInterceptor {

    @NotNull
    private Logger logger;

    @NotNull
    private PersistenceService persistenceService;

    @NotNull
    private AuthorizationEntityGraphCache authorizationEntityGraphCache;

    @Inject
    private void setLogger(final Logger logger) {
        this.logger = logger;
    }

    @Inject
    private void setPersistenceService(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Inject
    private void setAuthorizationEntityGraphCache(final AuthorizationEntityGraphCache authorizationEntityGraphCache) {
        this.authorizationEntityGraphCache = authorizationEntityGraphCache;
    }

    @AroundInvoke
    public Object performOAAC(InvocationContext invocationContext) throws Exception {

        long startInterceptorCall = System.nanoTime();

        Class<? extends GuardedEntity> targetEntityClazz = null;
        Class<? extends GuardedEntity> contextEntityClazz = null;
        Privilege.ActionType actionType = null;
        String action = null;

        Object[] parameters = null;
        Agent agent = null;
        RoleAssignment roleAssignment = null;
        GuardedEntity consideredGuardedEntity = null;
        Class<? extends GuardedEntity> consideredGuardedEntityClazz = null;

        Privilege identifiedPrivilege = null;

        if (invocationContext.getMethod() != null) {

            // get Privilege details
            RequiresAccessControl rac = invocationContext.getMethod().getAnnotation(RequiresAccessControl.class);
            parameters = invocationContext.getParameters();
            action = invocationContext.getMethod().getName();

            if (parameters.length >= 3) {

                // get the attributes of the RequiresAccessControl annotation
                targetEntityClazz = rac.targetObjectClazz();
                contextEntityClazz = rac.contextObjectClazz();
                actionType = Privilege.ActionType.valueOf(rac.actionType().name());
                action = invocationContext.getMethod().getName();

                // get the essential ids
                Long agentId = (Long) parameters[0];
                if (agentId == null){
                    throw new AuthorizationException("The id of the Agent must be set properly!");
                }

                Long roleAssignmentId = (Long) parameters[1];
                if (roleAssignmentId == null) {
                    throw new AuthorizationException("The id of the RoleAssignment must be set properly!");
                }

                // get Agent, RoleAssignment and RoleScope
                try {
                    agent = this.persistenceService.get(Agent.class, agentId);
                }
                catch (PersistenceException pe){
                    throw new AuthenticationException("There is a problem with authenticating you!");
                }

                List<RoleAssignment> roleAssignments;
                try {
                    // retrieve RoleAssignment and fetching all required entities
                    roleAssignments = this.persistenceService.findWithNamedQuery(RoleAssignment.QUERY__ID, QueryParameter.with("persistenceId", roleAssignmentId).parameters());
                }
                catch (PersistenceException pe){
                    pe.printStackTrace();
                    throw new AuthorizationException("There is a severe error in the authorization system! Please contact an administrator!");
                }

                // logger.info("RoleAssignments loaded");

                if (roleAssignments.size() == 1){
                    roleAssignment = roleAssignments.get(0);
                }
                else {
                    if (roleAssignments.size() == 0){
                        throw new AuthorizationException("This RoleAssignment does not exist!");
                    }
                    else {
                        throw new AuthorizationException("There is a severe error in the authorization system! Please contact an administrator!");
                    }
                }

                GuardedEntity givenTargetGuardedEntity = null;
                GuardedEntity givenContextualGuardedEntity = null;

                if (actionType == Privilege.ActionType.ADD_LINK || actionType == Privilege.ActionType.READ || actionType == Privilege.ActionType.UPDATE ||
                        actionType == Privilege.ActionType.UPDATE_STATE || actionType == Privilege.ActionType.REMOVE || actionType == Privilege.ActionType.REMOVE_LINK ||
                        actionType == Privilege.ActionType.ADVANCED) {
                    givenTargetGuardedEntity = this.persistenceService.get(targetEntityClazz, parameters[2]);
                    consideredGuardedEntity = givenTargetGuardedEntity;
                    consideredGuardedEntityClazz = targetEntityClazz;

                    if (actionType == Privilege.ActionType.ADD_LINK || actionType == Privilege.ActionType.REMOVE || actionType == Privilege.ActionType.REMOVE_LINK) {
                        givenContextualGuardedEntity = this.persistenceService.get(contextEntityClazz, parameters[3]);
                    }
                    if (actionType == Privilege.ActionType.ADVANCED){
                        if (parameters.length > 3){
                            try {
                                givenContextualGuardedEntity = this.persistenceService.get(contextEntityClazz, parameters[3]);
                            }
                            catch (PersistenceException pe){
                                givenContextualGuardedEntity = givenTargetGuardedEntity;
                            }
                        }
                        else {
                            givenContextualGuardedEntity = givenTargetGuardedEntity;
                        }
                    }
                } else {
                    givenContextualGuardedEntity = this.persistenceService.get(contextEntityClazz, parameters[2]);
                    consideredGuardedEntity = givenContextualGuardedEntity;
                    consideredGuardedEntityClazz = contextEntityClazz;
                }

                // we need to go through all the RoleScopeAssignments of the RoleAssignment
                Long referencedGuardedEntityId = -1L;

                for (RoleScopeAssignment roleScopeAssignment : roleAssignment.getRoleScopeAssignments()){
                    // Check if the required Privilege has been found in the last round of this loop
                    if (identifiedPrivilege != null){
                        break;
                    }

                    // get the RoleScope of the RoleScopeAssignment
                    RoleScope roleScope = roleScopeAssignment.getRoleScope();

                    // Case 1: the entity id referenced by the RoleAssignment matches the considered entity id
                    // -> check first whether the given RoleAssignment has the right RoleScope
                    // -> the privilege should be in the set of entity privileges of the corresponding RoleScopePrivilegeAssociation
                    // important: case 1 is impossible for privileges to add, list and search entities (cause these privileges reference the contextual entity)
                    if ((roleScopeAssignment.getGuardedEntityId() == consideredGuardedEntity.getId()) && givenTargetGuardedEntity != null) {
                        if (roleScope.getTargetEntityClazz().equals(consideredGuardedEntityClazz) || consideredGuardedEntityClazz.isAssignableFrom(roleScope.getTargetEntityClazz())) {
                            if (roleScope.getContextEntitiesClazzes().size() > 0) {
                                if (givenContextualGuardedEntity == null || !roleScope.getContextEntitiesClazzes().get(0).equals(contextEntityClazz)) {
                                    continue;
                                }
                            }
                            // Status quo: the RoleScope is proper
                            // check now if the privilege is properly set

                            // get the actual Privilege as well as the Privileges regarding reading and updating the attributes of the considered
                            Set<Privilege.ActionType> actionTypes = new HashSet<>();
                            actionTypes.add(actionType);
                            actionTypes.add(Privilege.ActionType.READ_ATTRIBUTE);
                            actionTypes.add(Privilege.ActionType.UPDATE_ATTRIBUTE);
                            Map<Privilege.ActionType, Set<String>> actionMap = new HashMap<>();
                            Set<String> actions = new HashSet<>();
                            actions.add(action);
                            actionMap.put(actionType, actions);

                            Map<Privilege.ActionType, Set<Privilege>> privilegesMap = this.lookForValidPrivileges(roleScope.getRoleScopePrivilegeAssociation().getEntityPrivileges(), targetEntityClazz, contextEntityClazz, actionTypes, actionMap);

                            Set<Privilege> requiredPrivileges = privilegesMap.get(actionType);

                            if (requiredPrivileges.size() == 0){
                                throw new AuthorizationException("The given RoleAssignment does not allow to perform this action!");
                            }

                            if (requiredPrivileges.size() > 1){
                                throw new AuthorizationException("An error occurred while performing Access Control. Please contact an administrator.");
                            }

                            referencedGuardedEntityId = consideredGuardedEntity.getId();
                            identifiedPrivilege = requiredPrivileges.iterator().next();
                          }
                    }

                    // Case 2: the entity id referenced by the RoleAssignment does not match the considered entity id
                    // -> we need to go up in the hierarchy of contextual GuardedEntities of the considered GuardedEntity in order
                    // to find the one referenced by the RoleAssignment
                    // then check whether the given RoleAssignment has the proper RoleScope with the required hierarchical Privilege set
                    // if this is the case everything is fine!
                    else {
                        if (consideredGuardedEntity.getId() == roleScopeAssignment.getGuardedEntityId()){
                            referencedGuardedEntityId = roleScopeAssignment.getGuardedEntityId();
                        }
                        else {

                            Set<List<Long>> parentGuardedEntitiesIdsTraces = this.authorizationEntityGraphCache.getParentGuardedEntityIdTraces(consideredGuardedEntity, roleScopeAssignment.getGuardedEntityId());
                            if (parentGuardedEntitiesIdsTraces.size() == 0) {
                                continue;
                            }
                            else {
                                referencedGuardedEntityId = roleScopeAssignment.getGuardedEntityId();
                            }
                        }
                        // get the actual Privilege as well as the Privileges regarding reading and updating the attributes of the considered
                        Set<Privilege.ActionType> actionTypes = new HashSet<>();

                        // add required actionType as well as the actionType READ_ATTRIBUTE, UPDATE_ATTRIBUTE, and READ (if not already included by required actionType)
                        actionTypes.add(actionType);
                        actionTypes.add(Privilege.ActionType.READ_ATTRIBUTE);
                        actionTypes.add(Privilege.ActionType.UPDATE_ATTRIBUTE);

                        // define that we look for a certain action
                        Map<Privilege.ActionType, Set<String>> actionMap = new HashMap<>();
                        if (!actionType.equals(Privilege.ActionType.READ)){
                            actionTypes.add(Privilege.ActionType.READ);

                            Set<String> actions = new HashSet<>();
                            actions.add(action);
                            actionMap.put(actionType, actions);
                        }


                        Map<Privilege.ActionType, Set<Privilege>> privilegesMap = this.lookForValidPrivileges(roleScope.getRoleScopePrivilegeAssociation().getHierarchicalPrivileges(), targetEntityClazz, contextEntityClazz, actionTypes, actionMap);

                        Set<Privilege> requiredPrivileges = privilegesMap.get(actionType);



                        if (requiredPrivileges.size() == 0){
                            throw new AuthorizationException("The given RoleAssignment does not allow to perform this action!");
                        }

                        if (!actionType.equals(Privilege.ActionType.READ)){
                            if (requiredPrivileges.size() > 1){
                                throw new AuthorizationException("An error occurred while performing Access Control. Please contact an administrator.");
                            }
                            identifiedPrivilege = requiredPrivileges.iterator().next();

                        }
                        else {
                            for (Privilege privilege : requiredPrivileges) {
                                if (privilege.getAction().equals(action)) {
                                    identifiedPrivilege = privilege;
                                    break;
                                }
                            }
                            if (identifiedPrivilege == null){
                                throw new AuthorizationException("The given RoleAssignment does not allow to perform this action!");
                            }

                        }
                    }

                    if (identifiedPrivilege != null){
                        break;
                    }
                }

                //long gatherPrivilegesEnd = HumanResourceSystem.nanoTime();
                //logger.info("Time spent to gather privileges: "+((gatherPrivilegesEnd-gatherPrivilegesStart)/1000000)+" milliseconds");

                if (identifiedPrivilege == null){
                    throw new AuthorizationException("The given RoleAssignment does not allow to perform this action!");
                }

                //long checkAgentsRelationToRoleAssignmentBegin = HumanResourceSystem.nanoTime();

                // check if Agent is really connected to the RoleAssignment
                this.checkAgentsRelationToRoleAssignment(agent, roleAssignment, referencedGuardedEntityId);

                //long checkAgentsRelationToRoleAssignmentEnd = HumanResourceSystem.nanoTime();
                //logger.info("Time spent to gather privileges: "+((checkAgentsRelationToRoleAssignmentEnd-checkAgentsRelationToRoleAssignmentBegin)/1000000)+" milliseconds");

                // now execute the method and log all relevant information
                Object methodResult;
                
                try{
                    long endInterceptorCall = System.nanoTime();
                    logger.info("Time spent to conduct entire interceptor call: "+((endInterceptorCall-startInterceptorCall)/1000000)+" milliseconds");

                    // let method be executed -> when the execution was successful, the action is logged properly
                    methodResult = invocationContext.proceed();
                    logger.info("Time spent to perform the requested method: "+((System.nanoTime()-endInterceptorCall)/1000000)+" milliseconds");

                    // If the Privilege has Privilege.ActionType.ADD, we need to check whether a new RoleScopeAssignment has to be created
                    // parameters: Privilege, Agent, RoleAssignment,
                    // Result: creation of a new

                    if (identifiedPrivilege.getActionType().equals(Privilege.ActionType.ADD)){
                        GuardedEntityDTO guardedEntityDTO = (GuardedEntityDTO) methodResult;
                        Class<? extends GuardedEntity> targetClazz = identifiedPrivilege.getTargetEntityClazz();
                        GuardedEntity createdGuardedEntity = this.persistenceService.get(targetClazz, guardedEntityDTO.getId());

                        this.checkForAutomaticRoleAssignmentCreation(agent, roleAssignment, consideredGuardedEntity, identifiedPrivilege, createdGuardedEntity);
                    }

                    if (identifiedPrivilege.getActionType().equals(Privilege.ActionType.REMOVE) || identifiedPrivilege.getActionType().equals(Privilege.ActionType.ADVANCED)){
                        authorizationEntityGraphCache.removeGuardedEntity(consideredGuardedEntity);
                    }

                    // log the execution of the method
                    // parameters: agentId, roleAssignmentId, privilegeId, date and time, ...

                    return methodResult;
                }
                catch (Exception e){
                    throw e;
                }
            }
            else {
                throw new AuthorizationException("There is a problem regarding the definition of the requested action. Please contact an administrator!");
            }
        }
        else {
            throw new AuthorizationException("There is a problem regarding the definition of the requested action. Please contact an administrator!");
        }
    }

    /**
     * This method checks whether a given set of Privileges contains privileges matching the given parameters; method finally returns these privileges
     *
     * @param givenPrivileges
     * @param targetEntityClazz
     * @param contextEntityClazz
     * @param actionTypes
     * @param actions
     * @return Privilege identifiedPrivilege
     */
    private Map<Privilege.ActionType, Set<Privilege>> lookForValidPrivileges(Set<Privilege> givenPrivileges, Class<? extends GuardedEntity> targetEntityClazz, Class<? extends GuardedEntity> contextEntityClazz, Set<Privilege.ActionType> actionTypes, Map<Privilege.ActionType, Set<String>> actions){
        Map<Privilege.ActionType, Set<Privilege>> result = new LinkedHashMap<>();

        for (Privilege.ActionType actionType : actionTypes){
            result.put(actionType, new HashSet<>());
        }

        for (Privilege privilege : givenPrivileges) {
            if (actionTypes.contains(privilege.getActionType())){
                boolean actionCheckOk = true;
                if (actions.keySet().contains(privilege.getActionType())){
                    if (!actions.get(privilege.getActionType()).contains(privilege.getAction())){
                        actionCheckOk = false;
                    }
                }

                if (actionCheckOk){
                    if (Modifier.isAbstract(targetEntityClazz.getModifiers())) {
                        if (!targetEntityClazz.isAssignableFrom(privilege.getTargetEntityClazz())) {
                            continue;
                        }
                    } else {
                        if (!privilege.getTargetEntityClazz().equals(targetEntityClazz)) {
                            continue;
                        }
                    }

                    if (Modifier.isAbstract(contextEntityClazz.getModifiers())) {
                        if (contextEntityClazz.isAssignableFrom(privilege.getContextEntityClazz())) {
                            // Privilege has been found!
                            if (result.keySet().contains(privilege.getActionType())){
                                Set<Privilege> resultingPrivileges = result.get(privilege.getActionType());
                                resultingPrivileges.add(privilege);
                            }
                            else {
                                Set<Privilege> resultingPrivileges = new LinkedHashSet<>();
                                resultingPrivileges.add(privilege);
                                result.put(privilege.getActionType(), resultingPrivileges);
                            }

                        }
                    } else {
                        if (privilege.getContextEntityClazz().equals(contextEntityClazz)) {
                            // Privilege has been found!
                            if (result.keySet().contains(privilege.getActionType())){
                                Set<Privilege> resultingPrivileges = result.get(privilege.getActionType());
                                resultingPrivileges.add(privilege);
                            }
                            else {
                                Set<Privilege> resultingPrivileges = new LinkedHashSet<>();
                                resultingPrivileges.add(privilege);
                                result.put(privilege.getActionType(), resultingPrivileges);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }



    /**
     * This method checks whether a given agent is properly connected to a given RoleAssignment (directly or indirectly using organizational Entities)
     *
     * @param agent
     * @param roleAssignment
     * @throws AuthorizationException
     */
    private void checkAgentsRelationToRoleAssignment(Agent agent, RoleAssignment roleAssignment, Long referencedGuardedEntityId) throws PersistenceException, AuthorizationException{
        // check if Agent is really connected to the RoleAssignment
        List<RoleAssignment> roleAssignments;
        if (agent instanceof User) {
            User user = (User) agent;
            roleAssignments = this.persistenceService.findWithNamedQuery(RoleAssignment.QUERY__USER__GUARDED_ENTITY_ID, QueryParameter.with("guardedEntityId", referencedGuardedEntityId).and("user", user).parameters());
        }
        else {
            roleAssignments = this.persistenceService.findWithNamedQuery(RoleAssignment.QUERY__AGENT__GUARDED_ENTITY_ID, QueryParameter.with("guardedEntityId", referencedGuardedEntityId).and("agent", agent).parameters());
        }

        if (!roleAssignments.contains(roleAssignment)){
            throw new AuthorizationException("The given RoleAssignment does not reference the given Agent!");
        }
    }

    /**
     * After the creation of a GuardedEntity, this method automatically creates a new RoleScopeAssignment or even an entire RoleAssignment if the parameter OnCreationDefault is set accordingly
     *
     * @param agent
     * @param givenRoleAssignment
     * @param consideredGuardedEntity
     * @param identifiedPrivilege
     */
    private void checkForAutomaticRoleAssignmentCreation(Agent agent, RoleAssignment givenRoleAssignment, GuardedEntity consideredGuardedEntity, Privilege identifiedPrivilege, GuardedEntity createdGuardedEntity) throws PersistenceException{
        // check if the Privilege denotes the right to add an object
        // only for this case we need to check whether a new RoleAssignment or a new RoleScopeAssignment of an existing RoleAssignment has to be created
        if (identifiedPrivilege.getActionType().equals(Privilege.ActionType.ADD)){
            // check the additional RoleScopes of the Role connected to the given RoleAssginment
            for (RoleScope additionalRoleScope : givenRoleAssignment.getRole().getAdditionalRoleScopes()){
                if (additionalRoleScope.getTargetEntityClazz().equals(identifiedPrivilege.getTargetEntityClazz()) && additionalRoleScope.isOnCreationDefault()){
                    // check the contextEntityClazzes properly
                    boolean contextEntityClazzesMatch = true;
                    List<Class<? extends  GuardedEntity>> contextEntityClazzes = additionalRoleScope.getContextEntitiesClazzes();
                    Set<GuardedEntity> parentGuardedEntities = new HashSet<>();
                    for (int i = 0; i < contextEntityClazzes.size(); i++){
                        if (i == 0){
                            if (!contextEntityClazzes.get(i).isInstance(consideredGuardedEntity)){
                                contextEntityClazzesMatch = false;
                                break;
                            }
                            else {
                                parentGuardedEntities = consideredGuardedEntity.getContextGuardedEntities();
                            }
                        }
                        else {
                            boolean parentFound = false;
                            Set<GuardedEntity> newParentGuardedEntities = new HashSet<>();
                            for (GuardedEntity parentGuardedEntity : parentGuardedEntities){
                                if (contextEntityClazzes.get(i).isInstance(parentGuardedEntity)){
                                    parentFound = true;
                                    newParentGuardedEntities.addAll(parentGuardedEntity.getContextGuardedEntities());
                                }
                            }
                            if (!parentFound){
                                contextEntityClazzesMatch = false;
                                break;
                            }
                            else {
                                parentGuardedEntities = newParentGuardedEntities;
                            }
                        }
                    }

                    // In case the contextEntityClazzes match the given one correctly
                    // create a new RoleScopeAssignment
                    if (contextEntityClazzesMatch){
                        RoleScopeAssignment roleScopeAssignment = new RoleScopeAssignment(agent, givenRoleAssignment, additionalRoleScope, createdGuardedEntity);
                        this.persistenceService.persist(roleScopeAssignment);
                    }

                }
            }

            // check for Roles having an key RoleScope with OnCreationDefault and matching the targetEntityClazz + contextEntityClazz
            List<Role> roles = this.persistenceService.findWithNamedQuery(Role.QUERY__KEY_SCOPE__CREATION_DEFAULT__TARGET_CLAZZ__CONTEXT_CLAZZ,
                    QueryParameter.with("targetEntityClazz", identifiedPrivilege.getTargetEntityClazz()).and("contextEntityClazz", identifiedPrivilege.getContextEntityClazz()).and("onCreationDefault", true).parameters());
            // there can be only one role having a key RoleScope with OnCreationDefault for given targetEntityClazz and contextEntityClazz
            if (roles.size() == 1){
                Role role = roles.get(0);
                RoleAssignment roleAssignment;
                if (!givenRoleAssignment.getAgents().isEmpty()){
                    roleAssignment = new RoleAssignment(agent, givenRoleAssignment.getHumanResourceSystem(), role, givenRoleAssignment.getAgents());
                }
                else {
                    roleAssignment = new RoleAssignment(agent, givenRoleAssignment.getHumanResourceSystem(), role, givenRoleAssignment.getOrgUnits(), givenRoleAssignment.getOrgRoles(), givenRoleAssignment.getAbilities());
                }

                RoleScopeAssignment roleScopeAssignment = new RoleScopeAssignment(agent, roleAssignment, role.getKeyRoleScope(), createdGuardedEntity);

                this.persistenceService.persist(roleAssignment);
                this.persistenceService.persist(roleScopeAssignment);
            }

        }
    }
    
}
