package info.dbis.orac.authorization;

import info.dbis.orac.common.HibernateUtils;
import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authorization.GuardedEntity;
import info.dbis.orac.datamodel.authorization.RoleAssignment;
import info.dbis.orac.datamodel.authorization.RoleScopeAssignment;
import info.dbis.orac.exception.ControllerException;
import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;

import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.*;

/**
 * AuthorizationEntityGraph
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Singleton
@AccessTimeout(value=120000)
public class AuthorizationEntityGraphCache {

    private PersistenceService persistenceService;

    private AuthorizationEntityGraphNode rootNode;

    private Map<Long, AuthorizationEntityGraphNode> nodeMap = new LinkedHashMap<>();

    public AuthorizationEntityGraphCache() {
        super();
    }

    @Inject
    private void setPersistenceService(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Lock(LockType.READ)
    public boolean containsGuardedEntity(GuardedEntity ge){
        return nodeMap.keySet().contains(ge.getId());
    }

    @Lock(LockType.READ)
    public Set<Long> getParentGuardedEntityIds(GuardedEntity ge) throws PersistenceException, ControllerException {
        return this.getParentGuardedEntityIds(ge, null);
    }

    @Lock(LockType.READ)
    public Set<Long> getParentGuardedEntityIds(GuardedEntity ge, Long targetGuardedEntityId) throws PersistenceException, ControllerException{
        // go through all parents and look for the referenced entity
        if (!this.containsGuardedEntity(ge)){
            this.addGuardedEntity(ge);
        }

        // is the node already be considered?
        if (nodeMap.keySet().contains(ge.getId())){
            Set<Long> ids = new LinkedHashSet<>();

            Queue<AuthorizationEntityGraphNode> nodeQueue = new LinkedList<>();
            nodeQueue.add(nodeMap.get(ge.getId()));

            while (!nodeQueue.isEmpty()){
                AuthorizationEntityGraphNode currentNode = nodeQueue.remove();
                ids.add(currentNode.id);
                for (AuthorizationEntityGraphNode furtherNode : currentNode.parentNodes){
                    if (targetGuardedEntityId != null){
                        if (!furtherNode.id.equals(targetGuardedEntityId)){
                            nodeQueue.add(furtherNode);
                        }
                    }
                    else {
                        nodeQueue.add(furtherNode);
                    }
                }
            }
            return ids;
        }
        else {
            return new LinkedHashSet<>();
        }
    }

    @Lock(LockType.READ)
    public Set<List<Long>> getParentGuardedEntityIdTraces(GuardedEntity ge, Long targetGuardedEntityId) throws PersistenceException, ControllerException{
        // go through all parents and look for the referenced entity
        if (!this.containsGuardedEntity(ge)){
            this.addGuardedEntity(ge);
        }

        Set<List<Long>> traces = new LinkedHashSet<>();
        for (GuardedEntity parent : ge.getContextGuardedEntities()){
            traces.addAll(this.getParentGuardedEntityIdTracesHelper(parent.getId(), targetGuardedEntityId, new LinkedList<>()));
        }

        return traces;
    }

    @Lock(LockType.READ)
    private Set<List<Long>> getParentGuardedEntityIdTracesHelper(Long guardedEntityId, Long targetGuardedEntityId, List<Long> trace) throws PersistenceException, ControllerException{
        if (nodeMap.keySet().contains(guardedEntityId)){
            AuthorizationEntityGraphNode node = nodeMap.get(guardedEntityId);

            trace.add(guardedEntityId);

            Set<List<Long>> traces = new LinkedHashSet<>();

            // did we find the targetGuardedEntityId?
            // then just return the traces with the current trace
            if (targetGuardedEntityId != null){
                if (targetGuardedEntityId.equals(guardedEntityId)){
                    traces.add(trace);
                    return traces;
                }
            }

            for (AuthorizationEntityGraphNode furtherNode : node.parentNodes){
                List<Long> newTrace = new LinkedList<>(trace);
                traces.addAll(this.getParentGuardedEntityIdTracesHelper(furtherNode.id, targetGuardedEntityId, newTrace));
            }

            return traces;
        }
        else {
            return new LinkedHashSet<>();
        }
    }

    @Lock(LockType.READ)
    public Set<List<GuardedEntity>> getParentalGuardedEntityTraces(GuardedEntity ge, Long targetGuardedEntityId) throws PersistenceException, ControllerException {
        return getParentalGuardedEntityTracesHelper(ge, null, null, new LinkedList<>(), targetGuardedEntityId);
    }

    @Lock(LockType.READ)
    private Set<List<GuardedEntity>> getParentalGuardedEntityTracesHelper(GuardedEntity ge, GuardedEntity child, AuthorizationEntityGraphNode childnode, List<GuardedEntity> trace, Long targetGuardedEntityId) throws PersistenceException, ControllerException {

        // trace already contains ge
        // that should not happen at all -> loop
        if (trace.contains(ge)){
            return new HashSet<>();
        }

        // add GuardedEntity to the nodeMap
        AuthorizationEntityGraphNode node;
        if (!nodeMap.keySet().contains(ge.getId())){
            // create a new AuthorizationEntityGraphNode
            node = new AuthorizationEntityGraphNode();
            node.id = ge.getId();
            node.clazz = HibernateUtils.getClassWithoutInitializingProxy(ge);
        }
        else { // -> yes
            node = nodeMap.get(ge.getId());
        }

        // set relationships of the AuthorizationEntityGraphNodes in the nodeMap
        if (child != null && childnode != null){
            if (!node.childNodes.contains(childnode)){
                node.childNodes.add(childnode);
            }
            if (!childnode.parentNodes.contains(node)){
                childnode.parentNodes.add(node);
            }
        }

        // set root node if possible
        if (ge instanceof HumanResourceSystem){
            rootNode = node;
        }

        // add ge to the trace if it is not the very first iteration
        if (child != null){
            trace.add(ge);
        }

        // prepare further iterations
        Set<List<GuardedEntity>> traces = new LinkedHashSet<>();

        // did we find the targetGuardedEntityId?
        // then just return the traces with the current trace
        if (targetGuardedEntityId != null){
            if (targetGuardedEntityId.equals(ge.getId())){
                traces.add(trace);
                return traces;
            }
        }
        // prepare the consideration of the parental entities
        Set<GuardedEntity> parentGuardedEntites;

        // Special case: ge is a RoleAssignment
        if (ge instanceof RoleAssignment) {
            parentGuardedEntites = new HashSet<>();
            RoleAssignment ra = (RoleAssignment) ge;
            for (RoleScopeAssignment currentRoleScopeAssignment : ra.getRoleScopeAssignments()){
                GuardedEntity contextEntity = this.persistenceService.get(currentRoleScopeAssignment.getRoleScope().getTargetEntityClazz(), currentRoleScopeAssignment.getGuardedEntityId());
                parentGuardedEntites.add(contextEntity);
            }
        }
        else {
            parentGuardedEntites = ge.getContextGuardedEntities();
        }

        // no parental GuardedEntities found?
        // that means that the targetGuardedEntityId has not found either
        if (parentGuardedEntites.size() < 1){
            // return emptry trace
            return new HashSet<>();
        }
        else {
            // further parents available
            for (GuardedEntity parentOfParent : parentGuardedEntites){
                List<GuardedEntity> newTrace = new LinkedList<>(trace);
                traces.addAll(this.getParentalGuardedEntityTracesHelper(parentOfParent, ge, node, newTrace, targetGuardedEntityId));
            }
        }

        return traces;
    }

    @Lock(LockType.WRITE)
    public void addGuardedEntity(GuardedEntity ge) throws PersistenceException, ControllerException {
        // is the GuardedEntity available in the AuthorizationEntityGraph
        if (!nodeMap.keySet().contains(ge.getId())){
            // create a new AuthorizationEntityGraphNode
            AuthorizationEntityGraphNode geNode = new AuthorizationEntityGraphNode();
            geNode.id = ge.getId();
            geNode.clazz = HibernateUtils.getClassWithoutInitializingProxy(ge);

            // create a map to retrieve the GuardedEntities that are considered in the while loop
            Map<Long, GuardedEntity> guardedEntitiesMap = new HashMap<>();
            guardedEntitiesMap.put(geNode.id, ge);

            // create List with AuthorizationEntityGraphNodes to be inserted
            Queue<AuthorizationEntityGraphNode> nodesToInsert = new LinkedList<>();

            // add the recently created node
            nodesToInsert.add(geNode);

            // set up a list that keeps track of all visited nodes (identified by their ids). This enables a loop prevention
            List<Long> consideredNodeIds = new LinkedList<>();

            // as long as there are nodes to consider
            while (!nodesToInsert.isEmpty()){
                // get the current node
                AuthorizationEntityGraphNode currentNode = nodesToInsert.remove();

                // did we already deal with a node having this id? if yes, there is a loop in the data model -> very bad
                if (consideredNodeIds.contains(currentNode.id)){
                    throw new ControllerException("There is a severe error in the data model! Please contact an administrator!");
                }
                else {
                    consideredNodeIds.add(currentNode.id);
                }

                // add the current AuthorizationEntityGraphNode to the nodeMap
                nodeMap.put(currentNode.id, currentNode);

                // get the GuardedEntity that is related to this node
                GuardedEntity currentGuardedEntity = guardedEntitiesMap.get(currentNode.id);

                // set the root node if possible
                if (currentGuardedEntity instanceof HumanResourceSystem){
                    rootNode = currentNode;
                }

                Set<GuardedEntity> parentGuardedEntites;

                if (ge instanceof RoleAssignment) {
                    parentGuardedEntites = new HashSet<>();
                    RoleAssignment ra = (RoleAssignment) ge;
                    for (RoleScopeAssignment currentRoleScopeAssignment : ra.getRoleScopeAssignments()){
                        GuardedEntity contextEntity = this.persistenceService.get(currentRoleScopeAssignment.getRoleScope().getTargetEntityClazz(), currentRoleScopeAssignment.getGuardedEntityId());
                        parentGuardedEntites.add(contextEntity);
                    }
                }
                else {
                    parentGuardedEntites = currentGuardedEntity.getContextGuardedEntities();
                }

                // check all parental GuardedEntities of the considered one
                for (GuardedEntity parentGE : parentGuardedEntites){
                    // does the nodeMap already contain the current node? -> no
                    if (!nodeMap.keySet().contains(parentGE.getId())){
                        // create a new AuthorizationEntityGraphNode
                        AuthorizationEntityGraphNode parentEntityGraphNode = new AuthorizationEntityGraphNode();
                        parentEntityGraphNode.id = parentGE.getId();
                        parentEntityGraphNode.clazz = HibernateUtils.getClassWithoutInitializingProxy(parentGE);

                        // create the relationships
                        currentNode.parentNodes.add(parentEntityGraphNode);
                        parentEntityGraphNode.childNodes.add(currentNode);

                        // add the node to the list iterator
                        nodesToInsert.add(parentEntityGraphNode);

                        // save the GuardedEntity in the map to make it retrievable in the next round of the while loop
                        guardedEntitiesMap.put(parentEntityGraphNode.id, parentGE);
                    }
                    else { // -> yes
                        currentNode.parentNodes.add(nodeMap.get(parentGE.getId()));
                        nodeMap.get(parentGE.getId()).childNodes.add(currentNode);
                    }
                }
            }
        }
        else {
            throw new ControllerException("The AuthorizationEntityGraph already contains this node!");
        }
    }

    @Lock(LockType.WRITE)
    public void removeGuardedEntity(GuardedEntity ge) throws ControllerException {
        // is the GuardedEntity available in the AuthorizationEntityGraph
        if (nodeMap.keySet().contains(ge.getId())){
            // we need to remove this node and all child ones
            AuthorizationEntityGraphNode authorizationEntityGraphNode = this.nodeMap.get(ge.getId());

            Queue<AuthorizationEntityGraphNode> nodesToDelete = new LinkedList<>();
            nodesToDelete.add(authorizationEntityGraphNode);

             while (!nodesToDelete.isEmpty()){
                 AuthorizationEntityGraphNode nodeToBeRemoved = nodesToDelete.remove();
                 nodeMap.remove(nodeToBeRemoved);

                 if (rootNode.equals(nodeToBeRemoved)){
                     rootNode = null;
                 }

                 for (AuthorizationEntityGraphNode node : nodeToBeRemoved.childNodes){
                     nodesToDelete.add(node);
                 }
             }
        }
        else {
            throw new ControllerException("The AuthorizationEntityGraph does not contain this node!");
        }
    }

    public class AuthorizationEntityGraphNode {
        Long id;
        Class<? extends GuardedEntity> clazz;

        Set<AuthorizationEntityGraphNode> parentNodes = new LinkedHashSet<>();
        Set<AuthorizationEntityGraphNode> childNodes = new LinkedHashSet<>();
    }
}
