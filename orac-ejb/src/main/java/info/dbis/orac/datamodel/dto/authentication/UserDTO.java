package info.dbis.orac.datamodel.dto.authentication;

import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authentication.User;
import info.dbis.orac.datamodel.dto.GuardedEntityDTO;

/**
 * UserDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class UserDTO extends GuardedEntityDTO {

    private String firstName = "";

    private String lastName = "";

    private String email = "";

    private String password = "";

    private boolean male = false;

    private String title = "";

    private String location = "";

    private String phone = "";

    private String website = "";

    private boolean newsletterSubscription;

    private String biography;

    public UserDTO() {
        super();
    }

    public UserDTO(User user) {
        super(user);
        this.setFirstName(user.getFirstName());
        this.setLastName(user.getLastName());
        this.setEmail(user.getEmail());
        this.setMale(user.isMale());
        this.setTitle(user.getTitle());
        this.setLocation(user.getLocation());
        this.setPhone(user.getPhone());
        this.setWebsite(user.getWebsite());
        this.setNewsletterSubscription(user.hasNewsletterSubscription());
        this.setBiography(user.getBiography());
    }

    public UserDTO(Agent creator, String firstName, String lastName, String email, boolean male,
                String title, String location, String phone, String website, boolean newsletterSubscription, String biography) {
        super(creator);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setEmail(email);
        this.setMale(male);
        this.setTitle(title);
        this.setLocation(location);
        this.setPhone(phone);
        this.setWebsite(website);
        this.setNewsletterSubscription(newsletterSubscription);
        this.setBiography(biography);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public boolean hasNewsletterSubscription() {
        return newsletterSubscription;
    }

    public void setNewsletterSubscription(boolean newsletterSubscription) {
        this.newsletterSubscription = newsletterSubscription;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }
}
