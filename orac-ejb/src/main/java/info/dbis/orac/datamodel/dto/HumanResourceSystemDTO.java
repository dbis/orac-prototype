package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.HumanResourceSystem;

/**
 * HumanResourceSystemDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class HumanResourceSystemDTO extends GuardedEntityDTO {

    public HumanResourceSystemDTO() {
        super();
    }

    public HumanResourceSystemDTO(HumanResourceSystem humanResourceSystem) {
        super(humanResourceSystem);
    }
}
