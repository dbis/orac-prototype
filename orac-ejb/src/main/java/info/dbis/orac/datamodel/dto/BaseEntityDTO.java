package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.auxiliaries.BaseEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * BaseEntityDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

public abstract class BaseEntityDTO implements Serializable {

    private Long id = 0L;
    private Long version = 0L;
    private Date dateCreated;
    private Date dateUpdated;

    public BaseEntityDTO() {
    }

    public BaseEntityDTO(BaseEntity baseEntity) {
        this.setId(baseEntity.getId());
        this.setVersion(baseEntity.getVersion());
        this.setDateCreated(baseEntity.getDateCreated());
        this.setDateUpdated(baseEntity.getDateUpdated());
    }

    public Long getId() {
        return id;
    }

    // not to be changed in client -> no privilege needed
    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    // not to be changed in client -> no privilege needed
    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    // not to be changed in client -> no privilege needed
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    // not to be changed in client -> no privilege needed
    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof BaseEntityDTO))
            return false;
        return ((getId().equals(((BaseEntityDTO) obj).getId())) && (getVersion().equals(((BaseEntityDTO) obj).getVersion())));
    }
}
