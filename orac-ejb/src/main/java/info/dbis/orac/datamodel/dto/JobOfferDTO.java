package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.JobOffer;

/**
 * JobOfferDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class JobOfferDTO extends GuardedEntityDTO {

    private String jobOfferText;

    private Long recruitmentProcessId;

    public JobOfferDTO() {
    }

    public JobOfferDTO(JobOffer jobOffer) {
        super(jobOffer);
        this.setJobOfferText(jobOffer.getJobOfferText());
        this.setRecruitmentProcessId(jobOffer.getRecruitmentProcess().getId());
    }

    public String getJobOfferText() {
        return jobOfferText;
    }

    public void setJobOfferText(String jobOfferText) {
        this.jobOfferText = jobOfferText;
    }

    public Long getRecruitmentProcessId() {
        return recruitmentProcessId;
    }

    public void setRecruitmentProcessId(Long recruitmentProcessId) {
        this.recruitmentProcessId = recruitmentProcessId;
    }
}
