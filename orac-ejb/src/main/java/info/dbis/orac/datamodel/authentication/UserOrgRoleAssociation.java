package info.dbis.orac.datamodel.authentication;


import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * UserOrgRoleAssociation
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = UserOrgRoleAssociation.QUERY__ALL,
                query = "select uora from UserOrgRoleAssociation uora"
        )
})
public class UserOrgRoleAssociation extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authentication.UserOrgRoleAssociation.QUERY__ALL";

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private OrgUnit orgUnit;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private User user;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private OrgRole orgRole;

    public UserOrgRoleAssociation() {
        super();
    }

    public UserOrgRoleAssociation(Agent creator, OrgUnit orgUnit, User user, OrgRole orgRole) {
        super(creator);
        this.setOrgUnit(orgUnit);
        this.setUser(user);
        this.setOrgRole(orgRole);
    }

    public OrgUnit getOrgUnit() {
        return this.orgUnit;
    }

    public void setOrgUnit(OrgUnit orgUnit) {
        if (this.orgUnit != null){
            this.orgUnit.removeUserOrgRoleAssociation(this);
        }
        this.orgUnit = orgUnit;
        if (this.orgUnit != null){
            if (!this.orgUnit.getUserOrgRoleAssociations().contains(this)){
                this.orgUnit.addUserOrgRoleAssociation(this);
            }
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        if (this.user != null){
            this.user.removeUserOrgRoleAssociation(this);
        }
        this.user = user;
        if (this.user != null){
            if (!this.user.getUserOrgRoleAssociations().contains(this)){
                this.user.addUserOrgRoleAssociation(this);
            }
        }
    }

    public OrgRole getOrgRole() {
        return orgRole;
    }

    public void setOrgRole(OrgRole orgRole) {
        if (this.orgRole != null){
            this.orgRole.removeUserOrgRoleAssociation(this);
        }
        this.orgRole = orgRole;
        if (this.orgRole != null){
            if (!this.orgRole.getUserOrgRoleAssociations().contains(this)){
                this.orgRole.addUserOrgRoleAssociation(this);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new LinkedHashSet<>();
        result.add(this.orgUnit);
        return result;
    }
}
