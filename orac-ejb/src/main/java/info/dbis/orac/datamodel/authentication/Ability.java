package info.dbis.orac.datamodel.authentication;


import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * An ability describes both a qualification as well as expertise of a person or role.
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = Ability.QUERY__ALL,
                query = "select a from Ability a"
        )
})
public class Ability extends OrgEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authentication.Ability.QUERY__ALL";

    @NotNull
    @Size(min = 1)
    private String name = "";

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private HumanResourceSystem humanResourceSystem;

    @OneToMany(mappedBy = "ability", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Set<UserAbilityAssociation> userAbilityAssociations = new LinkedHashSet<>();

    @ManyToMany(mappedBy = "abilities", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<OrgRole> orgRoles = new LinkedHashSet<>();

    public Ability() {
        super();
    }

    public Ability(Agent creator, String name, HumanResourceSystem humanResourceSystem){
        super(creator);
        this.setName(name);
        this.setHumanResourceSystem(humanResourceSystem);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (this.name != null){
            this.name = name;
        }
        else {
            this.name = "";
        }
    }

    public HumanResourceSystem getHumanResourceSystem() {
        return this.humanResourceSystem;
    }

    public void setHumanResourceSystem(HumanResourceSystem humanResourceSystem) {
        if (this.humanResourceSystem != null){
            this.humanResourceSystem.removeAbility(this);
        }
        this.humanResourceSystem = humanResourceSystem;
        if (humanResourceSystem != null){
            if (!humanResourceSystem.getAbilities().contains(this)){
                humanResourceSystem.addAbility(this);
            }
        }
    }

    public Set<UserAbilityAssociation> getUserAbilityAssociations() {
        return Collections.unmodifiableSet(userAbilityAssociations);
    }

    private void setUserAbilityAssociations(Set<UserAbilityAssociation> userAbilityAssociations) {
        this.userAbilityAssociations = userAbilityAssociations;
    }

    public void addUserAbilityAssociation(UserAbilityAssociation userAbilityAssociation){
        if (this.userAbilityAssociations.add(userAbilityAssociation)){
            if (!this.equals(userAbilityAssociation.getAbility())){
                userAbilityAssociation.setAbility(this);
            }
        }
    }

    public void removeUserAbilityAssociation(UserAbilityAssociation userAbilityAssociation){
        if (this.userAbilityAssociations.remove(userAbilityAssociation)){
            if (this.equals(userAbilityAssociation.getAbility())){
                userAbilityAssociation.setAbility(null);
            }
        }

    }

    public Set<OrgRole> getOrgRoles(){
        return Collections.unmodifiableSet(this.orgRoles);
    }

    private void setOrgRoles(Set<OrgRole> orgRoles) {
        this.orgRoles = orgRoles;
    }

    public void addOrgRole(OrgRole orgRole){
        if (this.orgRoles.add(orgRole)){
            if (!orgRole.getAbilities().contains(this)){
                orgRole.addAbility(this);
            }
        }
    }

    public void removeOrgRole(OrgRole orgRole){
        if (this.orgRoles.remove(orgRole)){
            if (orgRole.getAbilities().contains(this)) {
                orgRole.removeAbility(this);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new LinkedHashSet<>();
        result.add(this.humanResourceSystem);
        return result;
    }
}
