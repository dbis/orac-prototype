package info.dbis.orac.datamodel.auxiliaries;

import info.dbis.orac.common.ORACGlobalConstants;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Abstract BaseEntity class for all ORAC entities
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@MappedSuperclass
@Cacheable
public abstract class BaseEntity implements Serializable {

    private static final long serialVersionUID = ORACGlobalConstants.SERIAL_VERSION;

    /**
    *   For explanations regarding the generation of the ids please consider:
    *   http://supportmycode.com/2014/08/21/generating-the-identifier-property-generatedvaluestrategygenerationtype-table/
    */
    @TableGenerator(name = "EntityIdGenerate", allocationSize = 100, table = "idgen", pkColumnName = "sequence_name", pkColumnValue = "sequence", valueColumnName = "sequence_number")
    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "EntityIdGenerate")
    private long persistenceID;

    // Versioning ID
    @Version
    private Long version;

    // UUID for distinguishing entities before persisting them
    // see http://stackoverflow.com/questions/5031614/the-jpa-hashcode-equals-dilemma
    // This in only required before the entity is persisted!
    private UUID applicationID = UUID.randomUUID();

    // Date created
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;

    // Date the object was updated the very last time
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdated;

    // Standard Constructor
    public BaseEntity() {
        super();
    }

    // Before persisting the object the very first time, date_created and date_updated are initialized
    @PrePersist
    protected void onCreate() {
        dateUpdated = dateCreated = new Date();
    }

    // Before updating the object, we refresh the date_updated
    @PreUpdate
    protected void onUpdate() {
        dateUpdated = new Date();
    }

    // Getter
    public long getId() {
        return persistenceID;
    }

    public Long getVersion() {
        return version;
    }

    private UUID getApplicationId() {
        return applicationID;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    @Override
    public int hashCode() {
        return getApplicationId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        // this is needed as Hibernate is configured with lazy loading. Otherwise, we could compare persisted objects with proxies
        if (!(obj instanceof BaseEntity))
            return false;
        return ((getApplicationId().equals(((BaseEntity) obj).getApplicationId())) && (getVersion().equals(((BaseEntity) obj).getVersion())));
    }
}
