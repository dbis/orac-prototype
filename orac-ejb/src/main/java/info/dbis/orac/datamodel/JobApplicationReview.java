package info.dbis.orac.datamodel;

import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * JobApplicationReview
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = JobApplicationReview.QUERY__ALL,
                query = "select jar from JobApplicationReview jar"
        ),
        @NamedQuery(
                name = JobApplicationReview.QUERY__BY__JOB_APPLICATION,
                query = "select jar from JobApplicationReview jar join fetch jar.jobApplication ja " +
                        "where ja.persistenceID = :jobApplicationId"
        )
})
public class JobApplicationReview extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.JobApplicationReview.QUERY__ALL";
    public final static String QUERY__BY__JOB_APPLICATION = "info.dbis.orac.datamodel.JobApplicationReview.QUERY__BY__JOB_APPLICATION";

    @Lob
    private String jobApplicationReviewText;

    @OneToMany(mappedBy = "jobApplicationReview", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Document> documents = new LinkedHashSet<>();

    @NotNull
    @ManyToOne(cascade = {CascadeType.PERSIST})
    private JobApplication jobApplication;

    public JobApplicationReview() {
        super();
    }

    public JobApplicationReview(Agent creator, String jobApplicationReviewText, JobApplication jobApplication) {
        super(creator);
        this.setJobApplication(jobApplication);
        this.setJobApplicationReviewText(jobApplicationReviewText);
    }

    public String getJobApplicationReviewText() {
        return jobApplicationReviewText;
    }

    public void setJobApplicationReviewText(String jobApplicationReviewText) {
        this.jobApplicationReviewText = jobApplicationReviewText;
    }

    public Set<Document> getDocuments() {
        return Collections.unmodifiableSet(this.documents);
    }

    private void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    public void addDocument(Document document){
        if (this.documents.add(document)){
            if (!this.equals(document.getJobApplicationReview())){
                document.setJobApplicationReview(this);
            }
        }
    }

    public void removeDocument(Document document){
        if (this.documents.remove(document)){
            if (this.equals(document.getJobApplicationReview())){
                document.setJobApplicationReview(null);
            }
        }
    }

    public JobApplication getJobApplication() {
        return this.jobApplication;
    }

    public void setJobApplication(JobApplication jobApplication) {
        if (this.jobApplication != null){
            if (this.jobApplication.getJobApplicationReviews().contains(this)){
                this.jobApplication.removeJobApplicationReview(this);
            }
        }
        this.jobApplication = jobApplication;
        if (jobApplication != null){
            if (!jobApplication.getJobApplicationReviews().contains(this)){
                jobApplication.addJobApplicationReview(this);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> contextGuardedEntities = new LinkedHashSet<>();
        contextGuardedEntities.add(this.jobApplication);
        return contextGuardedEntities;
    }
}
