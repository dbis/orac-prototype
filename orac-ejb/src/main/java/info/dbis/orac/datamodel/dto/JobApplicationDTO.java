package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.JobApplication;

/**
 * JobApplicationDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class JobApplicationDTO extends GuardedEntityDTO {

    private String jobApplicationText;

    private Long recruitmentProcessId;

    public JobApplicationDTO() {
        super();
    }

    public JobApplicationDTO(JobApplication jobApplication) {
        super(jobApplication);
        this.setJobApplicationText(jobApplication.getJobApplicationText());
        this.setRecruitmentProcessId(jobApplication.getRecruitmentProcess().getId());
    }

    public String getJobApplicationText() {
        return jobApplicationText;
    }

    public void setJobApplicationText(String jobApplicationText) {
        this.jobApplicationText = jobApplicationText;
    }

    public Long getRecruitmentProcessId() {
        return recruitmentProcessId;
    }

    public void setRecruitmentProcessId(Long recruitmentProcessId) {
        this.recruitmentProcessId = recruitmentProcessId;
    }
}
