package info.dbis.orac.datamodel.dto.authorization;

import info.dbis.orac.datamodel.authorization.Role;
import info.dbis.orac.datamodel.dto.GuardedEntityDTO;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * RoleDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class RoleDTO extends GuardedEntityDTO {

    private String name;

    private String description;

    private Set<Long> requiredContextRolesIds;

    private Long refinedParentalRoleId;

    private RoleScopeDTO keyRoleScope;

    private Set<Long> additionalRoleScopesIds;

    public RoleDTO() {
        super();
    }

    public RoleDTO(Role role){
        super(role);
        this.name = role.getName();
        this.description = role.getDescription();
        this.requiredContextRolesIds = new LinkedHashSet<>();
        role.getRequiredContextRoles().forEach(aRole -> this.requiredContextRolesIds.add(aRole.getId()));
        if (role.getRefinedParentalRole() != null){
            this.refinedParentalRoleId = role.getRefinedParentalRole().getId();
        }
        this.keyRoleScope = new RoleScopeDTO(role.getKeyRoleScope());
        this.additionalRoleScopesIds = new LinkedHashSet<>();
        role.getAdditionalRoleScopes().forEach(roleScope -> this.additionalRoleScopesIds.add(roleScope.getId()));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Long> getRequiredContextRolesIds() {
        return requiredContextRolesIds;
    }

    public void setRequiredContextRolesIds(Set<Long> requiredContextRolesIds) {
        this.requiredContextRolesIds = requiredContextRolesIds;
    }

    public Long getRefinedParentalRoleId() {
        return refinedParentalRoleId;
    }

    public void setRefinedParentalRoleId(Long refinedParentalRoleId) {
        this.refinedParentalRoleId = refinedParentalRoleId;
    }

    public RoleScopeDTO getKeyRoleScope() {
        return keyRoleScope;
    }

    public void setKeyRoleScope(RoleScopeDTO keyRoleScope) {
        this.keyRoleScope = keyRoleScope;
    }

    public Set<Long> getAdditionalRoleScopesIds() {
        return additionalRoleScopesIds;
    }

    public void setAdditionalRoleScopesIds(Set<Long> additionalRoleScopesIds) {
        this.additionalRoleScopesIds = additionalRoleScopesIds;
    }
}