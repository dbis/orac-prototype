package info.dbis.orac.datamodel.authorization;

import info.dbis.orac.datamodel.authentication.Agent;

import javax.persistence.*;
import java.util.*;

/**
 * RoleScopePrivilegeAssociation
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = RoleScopePrivilegeAssociation.QUERY__ALL,
                query = "select rspa from RoleScopePrivilegeAssociation rspa"
        )
})
public class RoleScopePrivilegeAssociation extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authorization.RoleScopePrivilegeAssociation.QUERY__ALL";

    @OneToOne(mappedBy = "roleScopePrivilegeAssociation", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private RoleScope roleScope;

    @OneToOne(mappedBy = "additionalRoleScopePrivilegeAssociation", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private RoleScopeAssignment roleScopeAssignmentWithAdditionalRPA;

    @OneToOne(mappedBy = "restrictingRoleScopePrivilegeAssociation", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private RoleScopeAssignment roleScopeAssignmentWithRestrictingRPA;

    @ManyToMany(mappedBy = "roleScopePrivilegeAssociationsWithEntityPrivilege", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<Privilege> entityPrivileges = new LinkedHashSet<>();

    @ManyToMany(mappedBy = "roleScopePrivilegeAssociationsWithHierarchicalPrivilege", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<Privilege> hierarchicalPrivileges = new LinkedHashSet<>();

    public RoleScopePrivilegeAssociation() {
        super();
    }

    public RoleScopePrivilegeAssociation(Agent creator) {
        super(creator);
    }

    public RoleScopePrivilegeAssociation(Agent creator, RoleScope roleScope) {
        super(creator);
        this.setRoleScope(roleScope);
    }

    public RoleScopePrivilegeAssociation(Agent creator, RoleScope roleScope, Set<Privilege> entityPrivileges, Set<Privilege> hierarchicalPrivileges) {
        super(creator);
        this.setRoleScope(roleScope);
        this.setEntityPrivileges(entityPrivileges);
        this.setHierarchicalPrivileges(hierarchicalPrivileges);
    }

    public RoleScope getRoleScope() {
        return roleScope;
    }

    public void setRoleScope(RoleScope roleScope) {
        if (this.roleScope != null){
            this.roleScope.setRoleScopePrivilegeAssociation(null);
        }
        this.roleScope = roleScope;
        if (roleScope != null){
            roleScope.setRoleScopePrivilegeAssociation(this);
        }
    }

    public RoleScopeAssignment getRoleScopeAssignmentWithAdditionalRPA() {
        return roleScopeAssignmentWithAdditionalRPA;
    }

    public void setRoleScopeAssignmentWithAdditionalRPA(RoleScopeAssignment roleAssignmentWithAdditionalRolePrivilegeAssociation) {
        if (this.roleScopeAssignmentWithAdditionalRPA.getAdditionalRoleScopePrivilegeAssociation() != this){
            this.roleScopeAssignmentWithAdditionalRPA.setAdditionalRoleScopePrivilegeAssociation(null);
        }
        this.roleScopeAssignmentWithAdditionalRPA = roleAssignmentWithAdditionalRolePrivilegeAssociation;
        if (roleAssignmentWithAdditionalRolePrivilegeAssociation != null){
            roleAssignmentWithAdditionalRolePrivilegeAssociation.setAdditionalRoleScopePrivilegeAssociation(this);
        }
    }

    public RoleScopeAssignment getRoleScopeAssignmentWithRestrictingRPA() {
        return roleScopeAssignmentWithRestrictingRPA;
    }

    public void setRoleScopeAssignmentWithRestrictingRPA(RoleScopeAssignment roleAssignmentWithRestrictingRolePrivilegeAssociation) {
        if (this.roleScopeAssignmentWithRestrictingRPA.getRestrictingRoleScopePrivilegeAssociation() != this){
            this.roleScopeAssignmentWithRestrictingRPA.setRestrictingRoleScopePrivilegeAssociation(null);
        }
        this.roleScopeAssignmentWithRestrictingRPA = roleAssignmentWithRestrictingRolePrivilegeAssociation;
        if (roleAssignmentWithRestrictingRolePrivilegeAssociation != null){
            roleAssignmentWithRestrictingRolePrivilegeAssociation.setRestrictingRoleScopePrivilegeAssociation(this);
        }
    }

    public Set<Privilege> getEntityPrivileges() {
        return Collections.unmodifiableSet(this.entityPrivileges);
    }

    private void setEntityPrivileges(Set<Privilege> privileges) {
        this.entityPrivileges = privileges;
    }

    public void addEntityPrivilege(Privilege privilege){
        if (this.entityPrivileges.add(privilege)){
            if (!privilege.getRoleScopePrivilegeAssociationsWithEntityPrivilege().contains(this)){
                privilege.addRoleScopePrivilegeAssociationWithTargetEntityPrivilege(this);
            }
        }
    }

    public void addEntityPrivileges(Privilege... privileges){
        for (Privilege privilege : privileges){
            this.addEntityPrivilege(privilege);
        }
    }

    public void addEntityPrivileges(Set<Privilege> privileges){
        privileges.forEach(privilege -> this.addEntityPrivilege(privilege));
    }

    public void removeEntityPrivilege(Privilege privilege){
        if (this.entityPrivileges.remove(privilege)){
            if (privilege.getRoleScopePrivilegeAssociationsWithEntityPrivilege().contains(this)){
                privilege.removeRoleScopePrivilegeAssociationWithTargetEntityPrivilege(this);
            }
        }
    }

    public void removeEntityPrivileges(Privilege... privileges){
        for (Privilege privilege : privileges){
            this.removeEntityPrivilege(privilege);
        }
    }

    public void removeEntityPrivileges(Set<Privilege> privileges){
        privileges.forEach(privilege -> this.removeEntityPrivilege(privilege));
    }

    public Set<Privilege> getHierarchicalPrivileges() {
        return Collections.unmodifiableSet(this.hierarchicalPrivileges);
    }

    private void setHierarchicalPrivileges(Set<Privilege> privileges) {
        this.hierarchicalPrivileges = privileges;
    }

    public void addHierarchicalPrivilege(Privilege privilege){
        if (this.hierarchicalPrivileges.add(privilege)){
            if (!privilege.getRoleScopePrivilegeAssociationsWithHierarchicalPrivilege().contains(this)){
                privilege.addRoleScopePrivilegeAssociationWithHierarchicalPrivilege(this);
            }
        }
    }

    public void addHierarchicalPrivileges(Privilege... privileges){
        for (Privilege privilege : privileges){
            this.addHierarchicalPrivilege(privilege);
        }
    }

    public void addHierarchicalPrivileges(Set<Privilege> privileges){
        privileges.forEach(privilege -> this.addHierarchicalPrivilege(privilege));
    }

    public void removeHierarchicalPrivilege(Privilege privilege){
        if (this.hierarchicalPrivileges.remove(privilege)){
            if (privilege.getRoleScopePrivilegeAssociationsWithHierarchicalPrivilege().contains(this)){
                privilege.removeRoleScopePrivilegeAssociationWithHierarchicalPrivilege(this);
            }
        }
    }

    public void removeHierarchicalPrivileges(Privilege... privileges){
        for (Privilege privilege : privileges){
            this.removeHierarchicalPrivilege(privilege);
        }
    }

    public void removeHierarchicalPrivileges(Set<Privilege> privileges){
        privileges.forEach(privilege -> this.removeHierarchicalPrivilege(privilege));
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> contextGuardedEntities = new LinkedHashSet<>();
        if (roleScope != null){
            contextGuardedEntities.add(roleScope);
        }
        if (roleScopeAssignmentWithAdditionalRPA != null){
            contextGuardedEntities.add(roleScopeAssignmentWithAdditionalRPA);
        }
        if (roleScopeAssignmentWithRestrictingRPA != null){
            contextGuardedEntities.add(roleScopeAssignmentWithRestrictingRPA);
        }
        return contextGuardedEntities;
    }
}
