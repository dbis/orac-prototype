package info.dbis.orac.datamodel;

import info.dbis.orac.datamodel.authentication.*;
import info.dbis.orac.datamodel.authorization.GuardedEntity;
import info.dbis.orac.datamodel.authorization.Privilege;
import info.dbis.orac.datamodel.authorization.Role;
import info.dbis.orac.datamodel.authorization.RoleAssignment;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * HumanResourceSystem
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = HumanResourceSystem.QUERY__ALL,
                query = "select hrs from HumanResourceSystem hrs"
        )
})
public class HumanResourceSystem extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.HumanResourceSystem.QUERY__ALL";

    private boolean bootstrapped = false;

    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private SystemAgent systemAgent;

    @OneToMany(mappedBy = "humanResourceSystem", cascade = CascadeType.PERSIST)
    private Set<User> users = new LinkedHashSet<>();

    @OneToMany(mappedBy = "humanResourceSystem", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Privilege> privileges = new LinkedHashSet<>();

    @OneToMany(mappedBy = "humanResourceSystem", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Role> roles = new LinkedHashSet<>();

    @OneToMany(mappedBy = "humanResourceSystem", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<RoleAssignment> roleAssignments = new LinkedHashSet<>();

    @OneToMany(mappedBy = "humanResourceSystem", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Ability> abilities = new LinkedHashSet<>();

    @OneToMany(mappedBy = "humanResourceSystem", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<OrgUnit> orgUnits = new LinkedHashSet<>();

    @OneToMany(mappedBy = "humanResourceSystem", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<UserAbilityAssociation> userAbilityAssociations = new LinkedHashSet<>();

    @OneToMany(mappedBy = "humanResourceSystem", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<RecruitmentProcess> recruitmentProcesses = new LinkedHashSet<>();


    public HumanResourceSystem() {
        super();
    }

    public HumanResourceSystem(SystemAgent systemAgent) {
        super(systemAgent);
        this.systemAgent = systemAgent;
    }

    public boolean isBootstrapped() {
        return bootstrapped;
    }

    public void setBootstrappedTrue() {
        this.bootstrapped = true;
    }

    public SystemAgent getSystemAgent() {
        return systemAgent;
    }

    public void setSystemAgent(SystemAgent systemAgent) {
        this.systemAgent = systemAgent;
    }

    public Set<User> getUsers() {
        return Collections.unmodifiableSet(users);
    }

    private void setUsers(Set<User> users) {
        this.users = users;
    }

    public void addUser(User user){
        if (this.users.add(user)){
            if (!this.equals(user.getHumanResourceSystem())){
                user.setHumanResourceSystem(this);
            }
        }
    }

    public void removeUser(User user){
        if (this.users.remove(user)){
            if (this.equals(user.getHumanResourceSystem())){
                user.setHumanResourceSystem(null);
            }
        }
    }

    public Set<Privilege> getPrivileges() {
        return Collections.unmodifiableSet(this.privileges);
    }

    private void setPrivileges(Set<Privilege> privileges) {
        this.privileges = privileges;
    }

    public void addPrivilege(Privilege privilege) {
        if (this.privileges.add(privilege)){
            if (!this.equals(privilege.getHumanResourceSystem())){
                privilege.setHumanResourceSystem(this);
            }
        }
    }

    public void removePrivilege(Privilege privilege) {
        if (this.privileges.remove(privilege)){
            if (this.equals(privilege.getHumanResourceSystem())){
                privilege.setHumanResourceSystem(null);
            }
        }
    }

    public Set<Role> getRoles() {
        return Collections.unmodifiableSet(this.roles);
    }

    private void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        if (this.roles.add(role)){
            if (!this.equals(role.getHumanResourceSystem())){
                role.setHumanResourceSystem(this);
            }
        }
    }

    public void removeRole(Role role) {
        if (this.roles.remove(role)){
            if (this.equals(role.getHumanResourceSystem())){
                role.setHumanResourceSystem(null);
            }
        }
    }

    public Set<RoleAssignment> getRoleAssignments() {
        return Collections.unmodifiableSet(this.roleAssignments);
    }

    private void setRoleAssignments(Set<RoleAssignment> roleAssignments) {
        this.roleAssignments = roleAssignments;
    }

    public void addRoleAssignment(RoleAssignment roleAssignment) {
        if (this.roleAssignments.add(roleAssignment)){
            if (!this.equals(roleAssignment.getHumanResourceSystem())){
                roleAssignment.setHumanResourceSystem(this);
            }
        }
    }

    public void removeRoleAssignment(RoleAssignment roleAssignment) {
        if (this.roleAssignments.remove(roleAssignment)){
            if (this.equals(roleAssignment.getHumanResourceSystem())){
                roleAssignment.setHumanResourceSystem(null);
            }
        }
    }

    public Set<Ability> getAbilities() {
        return Collections.unmodifiableSet(abilities);
    }

    private void setAbilities(Set<Ability> abilities) {
        this.abilities = abilities;
    }

    public void addAbility(Ability ability){
        if (this.abilities.add(ability)){
            if (!this.equals(ability.getHumanResourceSystem())){
                ability.setHumanResourceSystem(this);
            }
        }
    }

    public void removeAbility(Ability ability){
        if (this.abilities.remove(ability)){
            if (this.equals(ability.getHumanResourceSystem())){
                ability.setHumanResourceSystem(null);
            }
        }
    }

    public Set<OrgUnit> getOrgUnits() {
        return Collections.unmodifiableSet(orgUnits);
    }

    private void setOrgUnits(Set<OrgUnit> orgUnits) {
        this.orgUnits = orgUnits;
    }

    public void addOrgUnit(OrgUnit orgUnit){
        if (this.orgUnits.add(orgUnit)){
            if (!this.equals(orgUnit.getHumanResourceSystem())){
                orgUnit.setHumanResourceSystem(this);
            }
        }
    }

    public void removeOrgUnit(OrgUnit orgUnit){
        if (this.orgUnits.remove(orgUnit)){
            if (this.equals(orgUnit.getHumanResourceSystem())){
                orgUnit.setHumanResourceSystem(null);
            }
        }
    }

    public Set<UserAbilityAssociation> getUserAbilityAssociations() {
        return Collections.unmodifiableSet(userAbilityAssociations);
    }

    private void setUserAbilityAssociations(Set<UserAbilityAssociation> userAbilityAssociations) {
        this.userAbilityAssociations = userAbilityAssociations;
    }

    public void addUserAbilityAssociation(UserAbilityAssociation userAbilityAssociation){
        if (this.userAbilityAssociations.add(userAbilityAssociation)){
            if (!this.equals(userAbilityAssociation.getHumanResourceSystem())){
                userAbilityAssociation.setHumanResourceSystem(this);
            }
        }
    }

    public void removeUserAbilityAssociation(UserAbilityAssociation userAbilityAssociation){
        if (this.userAbilityAssociations.remove(userAbilityAssociation)){
            if (this.equals(userAbilityAssociation.getHumanResourceSystem())){
                userAbilityAssociation.setHumanResourceSystem(null);
            }
        }
    }

    public Set<RecruitmentProcess> getRecruitmentProcesses() {
        return recruitmentProcesses;
    }

    private void setRecruitmentProcesses(Set<RecruitmentProcess> recruitmentProcesses) {
        this.recruitmentProcesses = recruitmentProcesses;
    }

    public void addRecruitmentProcess(RecruitmentProcess recruitmentProcess){
        if (this.recruitmentProcesses.add(recruitmentProcess)){
            if (!this.equals(recruitmentProcess.getHumanResourceSystem())){
                recruitmentProcess.setHumanResourceSystem(this);
            }
        }
    }

    public void removeRecruitmentProcess(RecruitmentProcess recruitmentProcess){
        if (this.recruitmentProcesses.remove(recruitmentProcess)){
            if (this.equals(recruitmentProcess.getHumanResourceSystem())){
                recruitmentProcess.setHumanResourceSystem(null);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        return new HashSet<>();
    }
}
