package info.dbis.orac.datamodel.authentication;

import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * SystemAgent
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = SystemAgent.QUERY__ALL,
                query = "select sa from SystemAgent sa"
        ),
})
public class SystemAgent extends Agent {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authentication.SystemAgent.QUERY__ALL";

    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "systemAgent")
    private HumanResourceSystem humanResourceSystem;

    public SystemAgent() {
        super();
    }

    public SystemAgent(Agent creator) {
        super(creator);
    }

    @Override
    public String getName() {
        return "SystemAgent";
    }

    public HumanResourceSystem getHumanResourceSystem() {
        return this.humanResourceSystem;
    }

    public void setHumanResourceSystem(HumanResourceSystem humanResourceSystem) {
        this.humanResourceSystem = humanResourceSystem;
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new HashSet<>();
        result.add(this.humanResourceSystem);
        return result;
    }
}
