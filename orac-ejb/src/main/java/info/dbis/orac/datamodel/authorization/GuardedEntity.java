package info.dbis.orac.datamodel.authorization;

import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.auxiliaries.BaseEntity;

import javax.persistence.MappedSuperclass;
import java.util.Set;

/**
 * A GuardedEntity is the base class for every entity requiring permission for access
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
*/

@MappedSuperclass
public abstract class GuardedEntity extends BaseEntity {

	// Creator
	private long creatorId;
    private String creatorName;

    public GuardedEntity(){
        super();
    }

    public GuardedEntity(Agent creator){
        super();
        this.creatorId = creator.getId();
        this.creatorName = creator.getName();
    }

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public abstract Set<GuardedEntity> getContextGuardedEntities();
}