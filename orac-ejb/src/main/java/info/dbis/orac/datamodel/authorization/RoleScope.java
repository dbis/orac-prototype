package info.dbis.orac.datamodel.authorization;

import info.dbis.orac.datamodel.authentication.Agent;

import javax.persistence.*;
import java.util.*;

/**
 * RoleScope
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = RoleScope.QUERY__ALL,
                query = "select rs from RoleScope rs"
        )
})
public class RoleScope extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authorization.RoleScope.QUERY__ALL";

    private Class<? extends GuardedEntity> targetEntityClazz;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<Class<? extends GuardedEntity>> contextEntitiesClazzes = new LinkedList<>(); // ascending

    private boolean isScopeManager = false;

    private boolean onCreationDefault = false;

    private boolean onAssignmentDefault = false;

    private int minimumNumberOfReferencedEntities = -1;

    private int maximumNumberOfReferencedEntities = -1;

    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Role roleWithKeyRoleScope;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Role roleWithAdditionalRoleScope;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY, optional = false)
    private RoleScopePrivilegeAssociation roleScopePrivilegeAssociation;

    @OneToMany(mappedBy = "roleScope", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Set<RoleScopeAssignment> roleScopeAssignments = new LinkedHashSet<>();

    public RoleScope() {
        super();
    }

    public RoleScope(Agent creator, Class targetEntityClazz, boolean isScopeManager, boolean onCreationDefault, boolean onAssignmentDefault) {
        super(creator);
        this.setTargetEntityClazz(targetEntityClazz);
        this.setOnCreationDefault(onCreationDefault);
        this.setOnAssignmentDefault(onAssignmentDefault);
        this.setScopeManager(isScopeManager);
    }

    public RoleScope(Agent creator, Class targetEntityClazz, List<Class<? extends GuardedEntity>> contextEntitiesClazzes, boolean isScopeManager, boolean onCreationDefault, boolean onAssignmentDefault) {
        super(creator);
        this.setTargetEntityClazz(targetEntityClazz);
        this.setContextEntitiesClazzes(contextEntitiesClazzes);
        this.setOnCreationDefault(onCreationDefault);
        this.setOnAssignmentDefault(onAssignmentDefault);
        this.setScopeManager(isScopeManager);
    }

    public Class<? extends GuardedEntity> getTargetEntityClazz() {
        return targetEntityClazz;
    }

    public void setTargetEntityClazz(Class<? extends GuardedEntity> keyScope) {
        this.targetEntityClazz = keyScope;
    }

    public List<Class<? extends GuardedEntity>> getContextEntitiesClazzes() {
        return contextEntitiesClazzes;
    }

    public void setContextEntitiesClazzes(List<Class<? extends GuardedEntity>> parentalScope) {
        this.contextEntitiesClazzes = parentalScope;
    }

    public boolean isScopeManager() {
        return this.isScopeManager;
    }

    public void setScopeManager(boolean scopeManager) {
        this.isScopeManager = scopeManager;
    }

    public boolean isOnCreationDefault() {
        return this.onCreationDefault;
    }

    public void setOnCreationDefault(boolean onCreationDefault) {
        this.onCreationDefault = onCreationDefault;
    }

    public boolean isOnAssignmentDefault() {
        return this.onAssignmentDefault;
    }

    public void setOnAssignmentDefault(boolean onUserAddingDefault) {
        this.onAssignmentDefault = onUserAddingDefault;
    }

    public int getMinimumNumberOfReferencedEntities() {
        return minimumNumberOfReferencedEntities;
    }

    public void setMinimumNumberOfReferencedEntities(int minimumNumberOfReferencedEntities) {
        this.minimumNumberOfReferencedEntities = minimumNumberOfReferencedEntities;
    }

    public int getMaximumNumberOfReferencedEntities() {
        return maximumNumberOfReferencedEntities;
    }

    public void setMaximumNumberOfReferencedEntities(int maximumNumberOfReferencedEntities) {
        this.maximumNumberOfReferencedEntities = maximumNumberOfReferencedEntities;
    }

    public Role getRoleWithKeyRoleScope() {
        return roleWithKeyRoleScope;
    }

    public void setRoleWithKeyRoleScope(Role roleWithKeyRoleScope) {
        this.roleWithKeyRoleScope = roleWithKeyRoleScope;
    }

    public Role getRoleWithAdditionalRoleScope() {
        return roleWithAdditionalRoleScope;
    }

    public void setRoleWithAdditionalRoleScope(Role roleWithAdditionalRoleScope) {
        this.roleWithAdditionalRoleScope = roleWithAdditionalRoleScope;
    }

    public RoleScopePrivilegeAssociation getRoleScopePrivilegeAssociation() {
        return roleScopePrivilegeAssociation;
    }

    public void setRoleScopePrivilegeAssociation(RoleScopePrivilegeAssociation roleScopePrivilegeAssociation) {
        this.roleScopePrivilegeAssociation = roleScopePrivilegeAssociation;
    }

    public Set<RoleScopeAssignment> getRoleScopeAssignments() {
        return Collections.unmodifiableSet(this.roleScopeAssignments);
    }

    private void setRoleScopeAssignments(Set<RoleScopeAssignment> roleAssignments) {
        this.roleScopeAssignments = roleScopeAssignments;
    }

    public void addRoleScopeAssignment(RoleScopeAssignment roleScopeAssignment){
        if (this.roleScopeAssignments.add(roleScopeAssignment)){
            if (!roleScopeAssignment.getRoleScope().equals(this)){
                roleScopeAssignment.setRoleScope(this);
            }
        }
    }

    public void removeRoleScopeAssignment(RoleScopeAssignment roleScopeAssignment){
        if (this.roleScopeAssignments.remove(roleScopeAssignment)){
            if (roleScopeAssignment.getRoleScope().equals(this)){
                roleScopeAssignment.setRoleScope(null);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new LinkedHashSet<>();
        if (roleWithKeyRoleScope != null){
            result.add(roleWithKeyRoleScope);
        }
        if (roleWithAdditionalRoleScope != null){
            result.add(roleWithAdditionalRoleScope);
        }
        return result;
    }
}
