package info.dbis.orac.datamodel.authentication;

import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * An OrgRole describes an organizational function. For instance, an employee of an institute may be webmaster
 * at this institute as well as person responsible for PR.
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = OrgRole.QUERY__ALL,
                query = "select o from OrgRole o"
        )
})
public class OrgRole extends OrgEntity{

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authentication.OrgRole.QUERY__ALL";

    // Attributes

    @NotNull
    @Size(min = 1)
    private String name = "";

    private String description = "";

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private OrgUnit orgUnit;

    @OneToMany(mappedBy = "orgRole", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<UserOrgRoleAssociation> userOrgRoleAssociations = new LinkedHashSet<>();

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<Ability> abilities = new LinkedHashSet<>();

    public OrgRole() {
        super();
    }

    public OrgRole(Agent creator, String name, String description, OrgUnit orgUnit){
        super(creator);
        this.setName(name);
        this.setDescription(description);
        this.setOrgUnit(orgUnit);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null){
            this.name = name;
        }
        else {
            this.name = "";
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description != null){
            this.description = description;
        }
        else {
            this.description = "";
        }
    }

    public OrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(OrgUnit orgunit) {
        if (this.orgUnit != null){
            this.orgUnit.removeOrgRole(this);
        }
        this.orgUnit = orgunit;
        if (orgunit != null){
            if (!orgunit.getOrgRoles().contains(this)) {
                orgunit.addOrgRole(this);
            }
        }
    }

    public Set<UserOrgRoleAssociation> getUserOrgRoleAssociations() {
        return Collections.unmodifiableSet(userOrgRoleAssociations);
    }

    private void setUserOrgRoleAssociations(Set<UserOrgRoleAssociation> userOrgRoleAssociations) {
        this.userOrgRoleAssociations = userOrgRoleAssociations;
    }

    public void addUserOrgRoleAssociation(UserOrgRoleAssociation userOrgRoleAssociation){
        if (this.userOrgRoleAssociations.add(userOrgRoleAssociation)){
            if (!this.equals(userOrgRoleAssociation.getOrgRole())){
                userOrgRoleAssociation.setOrgRole(this);
            }
        }
    }

    public void removeUserOrgRoleAssociation(UserOrgRoleAssociation userOrgRoleAssociation){
        if (this.userOrgRoleAssociations.remove(userOrgRoleAssociation)){
            if (this.equals(userOrgRoleAssociation.getOrgRole())){
                userOrgRoleAssociation.setOrgRole(null);
            }
        }
    }

    public Set<Ability> getAbilities() {
        return Collections.unmodifiableSet(abilities);
    }

    private void setAbilities(Set<Ability> abilities) {
        this.abilities = abilities;
    }

    public void addAbility(Ability ability){
        if (this.abilities.add(ability)){
            if (!ability.getOrgRoles().contains(this)){
                ability.addOrgRole(this);
            }
        }
    }

    public void removeAbility(Ability ability){
        if (this.abilities.remove(ability)){
            if (ability.getOrgRoles().contains(this)){
                ability.removeOrgRole(this);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new LinkedHashSet<>();
        result.add(this.orgUnit);
        return result;
    }
}
