package info.dbis.orac.datamodel.authorization;

import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authentication.Agent;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 *	A Privilege allows users with a connected RoleAssignment to access pre-defined functionality
 *
 *  @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Table(indexes = {@Index(columnList = "targetEntityClazz"), @Index(columnList = "contextEntityClazz"), @Index(columnList = "actionType"), @Index(columnList = "action")})
@Entity
@NamedQueries({
        @NamedQuery(
                name = Privilege.QUERY__ALL,
                query = "select p from Privilege p"
        ),
        @NamedQuery(
                name = Privilege.QUERY__TARGET_ENTITY_CLAZZ,
                query = "select p from Privilege p " +
                        "where p.targetEntityClazz = :targetEntityClazz"
        ),
        @NamedQuery(
                name = Privilege.QUERY__TARGET_ENTITY_CLAZZES,
                query = "select distinct p.targetEntityClazz from Privilege p"
        ),
        @NamedQuery(
                name = Privilege.QUERY__CONTEXT_ENTITY_CLAZZ,
                query = "select p from Privilege p " +
                "where p.contextEntityClazz = :contextEntityClazz"
        ),
        @NamedQuery(
                name = Privilege.QUERY__CONTEXT_ENTITY_CLAZZES,
                query = "select distinct p.contextEntityClazz from Privilege p"
        ),
        @NamedQuery(
                name = Privilege.QUERY__SEARCH,
                query = "select p from Privilege p " +
                        "where p.name = :name and p.description = :description and p.targetEntityClazz = :targetEntityClazz " +
                        "and p.contextEntityClazz = :contextEntityClazz and p.actionType = :actionType and p.action = :action"
        )
})
public class Privilege extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authorization.Privilege.QUERY__ALL";
    public final static String QUERY__TARGET_ENTITY_CLAZZ = "info.dbis.orac.datamodel.authorization.Privilege.QUERY__TARGET_ENTITY_CLAZZ";
    public final static String QUERY__TARGET_ENTITY_CLAZZES = "info.dbis.orac.datamodel.authorization.Privilege.QUERY__TARGET_ENTITY_CLAZZES";
    public final static String QUERY__CONTEXT_ENTITY_CLAZZ = "info.dbis.orac.datamodel.authorization.Privilege.QUERY__CONTEXT_ENTITY_CLAZZ";
    public final static String QUERY__CONTEXT_ENTITY_CLAZZES = "info.dbis.orac.datamodel.authorization.Privilege.QUERY__CONTEXT_ENTITY_CLAZZES";
    public final static String QUERY__SEARCH = "info.dbis.orac.datamodel.authorization.Privilege.QUERY__SEARCH";

    public enum ActionType {
        ADD, ADD_LINK, READ, READ_ATTRIBUTE, UPDATE, UPDATE_STATE, UPDATE_ATTRIBUTE, REMOVE, REMOVE_LINK, LISTING, SEARCH, ADVANCED
    }

    @NotNull
    private String name = "";

    private String description = "";

    private Class targetEntityClazz;

    private Class contextEntityClazz;

    @Enumerated(EnumType.STRING)
    private ActionType actionType;

    private String action = "";

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private HumanResourceSystem humanResourceSystem;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(
            name = "entityPrivilege_roleScopePrivilegeAssociation",
            joinColumns = @JoinColumn(name = "privilege_persistenceID"),
            inverseJoinColumns = @JoinColumn(name = "roleScopePrivilegeAssociation_persistenceID")
    )
    private Set<RoleScopePrivilegeAssociation> roleScopePrivilegeAssociationsWithEntityPrivilege = new LinkedHashSet<>();

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(
            name = "hierarchicalPrivilege_roleScopePrivilegeAssociation",
            joinColumns = @JoinColumn(name = "privilege_persistenceID"),
            inverseJoinColumns = @JoinColumn(name = "roleScopePrivilegeAssociation_persistenceID")
    )
    private Set<RoleScopePrivilegeAssociation> roleScopePrivilegeAssociationsWithHierarchicalPrivilege = new LinkedHashSet<>();

    // Constructors
    public Privilege() {
        super();
    }

    public Privilege(Agent creator, String name) {
        super(creator);
        this.setName(name);
    }

    public Privilege(Agent creator, String name, Class targetEntityClazz, Class contextEntityClazz, ActionType actionType, String action) {
        super(creator);
        this.setName(name);
        this.setTargetEntityClazz(targetEntityClazz);
        this.setContextEntityClazz(contextEntityClazz);
        this.setActionType(actionType);
        this.setAction(action);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null){
            this.name = name;
        }
        else {
            this.name = "";
        }
    }

    public void setDescription(String description) {
        if (description != null){
            this.description = description;
        }
        else {
            this.description = "";
        }
    }

    public String getDescription() {
        return description;
    }

    public Class getTargetEntityClazz() {
        return targetEntityClazz;
    }

    public void setTargetEntityClazz(Class targetScope) {
        this.targetEntityClazz = targetScope;
    }

    public Class getContextEntityClazz() {
        return contextEntityClazz;
    }

    public void setContextEntityClazz(Class contextScope) {
        this.contextEntityClazz = contextScope;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPermissionCode() {
        return this.targetEntityClazz.getName() + ":" + this.contextEntityClazz.getName() + ":" + this.actionType.name() + ":" + this.action;
    }

    public HumanResourceSystem getHumanResourceSystem() {
        return humanResourceSystem;
    }

    public void setHumanResourceSystem(HumanResourceSystem humanResourceSystem) {
        if (this.humanResourceSystem != null){
            this.humanResourceSystem.removePrivilege(this);
        }
        this.humanResourceSystem = humanResourceSystem;
        if (!humanResourceSystem.getPrivileges().contains(this)){
            humanResourceSystem.addPrivilege(this);
        }
    }

    public Set<RoleScopePrivilegeAssociation> getRoleScopePrivilegeAssociationsWithEntityPrivilege() {
        return Collections.unmodifiableSet(this.roleScopePrivilegeAssociationsWithEntityPrivilege);
    }

    private void setRoleScopePrivilegeAssociationsWithEntityPrivilege(Set<RoleScopePrivilegeAssociation> roleScopePrivilegeAssociations) {
        this.roleScopePrivilegeAssociationsWithEntityPrivilege = roleScopePrivilegeAssociations;
    }

    public void addRoleScopePrivilegeAssociationWithTargetEntityPrivilege(RoleScopePrivilegeAssociation roleScopePrivilegeAssociation){
        if (this.roleScopePrivilegeAssociationsWithEntityPrivilege.add(roleScopePrivilegeAssociation)){
            if (!roleScopePrivilegeAssociation.getEntityPrivileges().contains(this)){
                roleScopePrivilegeAssociation.addEntityPrivilege(this);
            }
        }
    }

    public void removeRoleScopePrivilegeAssociationWithTargetEntityPrivilege(RoleScopePrivilegeAssociation roleScopePrivilegeAssociation){
        if (this.roleScopePrivilegeAssociationsWithEntityPrivilege.remove(roleScopePrivilegeAssociation)){
            if (roleScopePrivilegeAssociation.getEntityPrivileges().contains(this)){
                roleScopePrivilegeAssociation.removeEntityPrivilege(this);
            }
        }
    }

    public Set<RoleScopePrivilegeAssociation> getRoleScopePrivilegeAssociationsWithHierarchicalPrivilege() {
        return Collections.unmodifiableSet(this.roleScopePrivilegeAssociationsWithHierarchicalPrivilege);
    }

    private void setRoleScopePrivilegeAssociationsWithHierarchicalPrivilege(Set<RoleScopePrivilegeAssociation> roleScopePrivilegeAssociations) {
        this.roleScopePrivilegeAssociationsWithHierarchicalPrivilege = roleScopePrivilegeAssociations;
    }

    public void addRoleScopePrivilegeAssociationWithHierarchicalPrivilege(RoleScopePrivilegeAssociation roleScopePrivilegeAssociation){
        if (this.roleScopePrivilegeAssociationsWithHierarchicalPrivilege.add(roleScopePrivilegeAssociation)){
            if (!roleScopePrivilegeAssociation.getHierarchicalPrivileges().contains(this)){
                roleScopePrivilegeAssociation.addHierarchicalPrivilege(this);
            }
        }
    }

    public void removeRoleScopePrivilegeAssociationWithHierarchicalPrivilege(RoleScopePrivilegeAssociation roleScopePrivilegeAssociation){
        if (this.roleScopePrivilegeAssociationsWithHierarchicalPrivilege.remove(roleScopePrivilegeAssociation)){
            if (roleScopePrivilegeAssociation.getHierarchicalPrivileges().contains(this)){
                roleScopePrivilegeAssociation.removeHierarchicalPrivilege(this);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new LinkedHashSet<>();
        result.add(this.humanResourceSystem);
        return result;
    }
}
