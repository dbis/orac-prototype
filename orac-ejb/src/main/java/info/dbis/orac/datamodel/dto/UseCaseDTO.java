package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.dto.authentication.UserDTO;
import info.dbis.orac.datamodel.dto.authorization.RoleAssignmentDTO;
import info.dbis.orac.datamodel.dto.authorization.RoleDTO;

import java.util.Set;

/**
 * UseCaseDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class UseCaseDTO {

    private RecruitmentProcessDTO recruitmentProcessDTO;

    private Set<JobApplicationDTO> jobApplicationDTOS;

    private Set<JobApplicationReviewDTO> jobApplicationReviewDTOS;

    private Set<ContractOfferDTO> contractOfferDTOS;

    private Set<UserDTO> users;

    private Set<RoleDTO> roles;

    private Set<RoleAssignmentDTO> roleAssignmentDTOS;

    public UseCaseDTO(RecruitmentProcessDTO recruitmentProcessDTO, Set<JobApplicationDTO> jobApplicationDTOS, Set<JobApplicationReviewDTO> jobApplicationReviewDTOS, Set<ContractOfferDTO> contractOfferDTOS, Set<UserDTO> users, Set<RoleDTO> roles, Set<RoleAssignmentDTO> roleAssignmentDTOS) {
        this.recruitmentProcessDTO = recruitmentProcessDTO;
        this.jobApplicationDTOS = jobApplicationDTOS;
        this.jobApplicationReviewDTOS = jobApplicationReviewDTOS;
        this.contractOfferDTOS = contractOfferDTOS;
        this.users = users;
        this.roles = roles;
        this.roleAssignmentDTOS = roleAssignmentDTOS;
    }

    public RecruitmentProcessDTO getRecruitmentProcessDTO() {
        return recruitmentProcessDTO;
    }

    public void setRecruitmentProcessDTO(RecruitmentProcessDTO recruitmentProcessDTO) {
        this.recruitmentProcessDTO = recruitmentProcessDTO;
    }

    public Set<JobApplicationDTO> getJobApplicationDTOS() {
        return jobApplicationDTOS;
    }

    public void setJobApplicationDTOS(Set<JobApplicationDTO> jobApplicationDTOS) {
        this.jobApplicationDTOS = jobApplicationDTOS;
    }

    public Set<JobApplicationReviewDTO> getJobApplicationReviewDTOS() {
        return jobApplicationReviewDTOS;
    }

    public void setJobApplicationReviewDTOS(Set<JobApplicationReviewDTO> jobApplicationReviewDTOS) {
        this.jobApplicationReviewDTOS = jobApplicationReviewDTOS;
    }

    public Set<ContractOfferDTO> getContractOfferDTOS() {
        return contractOfferDTOS;
    }

    public void setContractOfferDTOS(Set<ContractOfferDTO> contractOfferDTOS) {
        this.contractOfferDTOS = contractOfferDTOS;
    }

    public Set<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDTO> users) {
        this.users = users;
    }

    public Set<RoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleDTO> roles) {
        this.roles = roles;
    }

    public Set<RoleAssignmentDTO> getRoleAssignmentDTOS() {
        return roleAssignmentDTOS;
    }

    public void setRoleAssignmentDTOS(Set<RoleAssignmentDTO> roleAssignmentDTOS) {
        this.roleAssignmentDTOS = roleAssignmentDTOS;
    }
}
