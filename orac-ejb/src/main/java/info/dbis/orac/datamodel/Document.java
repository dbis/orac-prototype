package info.dbis.orac.datamodel;

import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Document
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = Document.QUERY__ALL,
                query = "select d from Document d"
        ),
        @NamedQuery(
                name = Document.QUERY__BY__JOB_APPLICATION,
                query = "select d from Document d join fetch d.jobApplication ja " +
                        "where ja.persistenceID = :jobApplicationId"
        ),
        @NamedQuery(
                name = Document.QUERY__BY__JOB_OFFER,
                query = "select d from Document d join fetch d.jobOffer jo " +
                        "where jo.persistenceID = :jobOfferId"
        )
})
public class Document extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.Document.QUERY__ALL";
    public final static String QUERY__BY__JOB_APPLICATION = "info.dbis.orac.datamodel.Document.QUERY__BY__JOB_APPLICATION";
    public final static String QUERY__BY__JOB_OFFER = "info.dbis.orac.datamodel.Document.QUERY__BY__JOB_OFFER";

    @NotNull
    @Size(min = 1)
    private String name;

    @Lob
    private String documentContent;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private JobOffer jobOffer;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private JobApplication jobApplication;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private JobApplicationReview jobApplicationReview;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private ContractOffer contractOffer;

    public Document() {
        super();
    }

    public Document(Agent creator, String name, String documentContent, JobApplication jobApplication) {
        super(creator);
        this.name = name;
        this.documentContent = documentContent;
        this.setJobApplication(jobApplication);
    }

    public Document(Agent creator, String name, String documentContent, JobOffer jobOffer) {
        super(creator);
        this.name = name;
        this.documentContent = documentContent;
        this.setJobOffer(jobOffer);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocumentContent() {
        return documentContent;
    }

    public void setDocumentContent(String documentContent) {
        this.documentContent = documentContent;
    }

    public JobOffer getJobOffer() {
        return jobOffer;
    }

    public void setJobOffer(JobOffer jobOffer) {
        if (this.jobOffer != null){
            if (this.jobOffer.getDocuments().contains(this)){
                this.jobOffer.removeDocument(this);
            }
        }
        this.jobOffer = jobOffer;
        if (jobOffer != null){
            if (!jobOffer.getDocuments().contains(this)){
                jobOffer.addDocument(this);
            }
        }
    }

    public JobApplication getJobApplication() {
        return jobApplication;
    }

    public void setJobApplication(JobApplication jobApplication) {
        if (this.jobApplication != null){
            if (this.jobApplication.getDocuments().contains(this)){
                this.jobApplication.removeDocument(this);
            }
        }
        this.jobApplication = jobApplication;
        if (jobApplication != null){
            if (!jobApplication.getDocuments().contains(this)){
                jobApplication.addDocument(this);
            }
        }
    }

    public JobApplicationReview getJobApplicationReview() {
        return jobApplicationReview;
    }

    public void setJobApplicationReview(JobApplicationReview jobApplicationReview) {
        if (this.jobApplicationReview != null){
            if (this.jobApplicationReview.getDocuments().contains(this)){
                this.jobApplicationReview.removeDocument(this);
            }
        }
        this.jobApplicationReview = jobApplicationReview;
        if (jobApplicationReview != null){
            if (!jobApplicationReview.getDocuments().contains(this)){
                jobApplicationReview.addDocument(this);
            }
        }
    }

    public ContractOffer getContractOffer() {
        return contractOffer;
    }

    public void setContractOffer(ContractOffer contractOffer) {
        if (this.contractOffer != null){
            if (this.contractOffer.getDocuments().contains(this)){
                this.contractOffer.removeDocument(this);
            }
        }
        this.contractOffer = contractOffer;
        if (contractOffer != null){
            if (!contractOffer.getDocuments().contains(this)){
                contractOffer.addDocument(this);
            }
        }
    }


    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> contextGuardedEntities = new LinkedHashSet<>();
        if (this.jobApplication != null) {
            contextGuardedEntities.add(this.jobApplication);
        }
        if (this.jobOffer != null) {
            contextGuardedEntities.add(this.jobOffer);
        }
        return contextGuardedEntities;
    }
}
