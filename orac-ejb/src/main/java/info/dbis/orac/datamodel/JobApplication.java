package info.dbis.orac.datamodel;

import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * JobApplication
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = JobApplication.QUERY__ALL,
                query = "select ja from JobApplication ja"
        ),
        @NamedQuery(
                name = JobApplication.QUERY__BY__RECRUITMENT_PROCESS,
                query = "select ja from JobApplication ja join fetch ja.recruitmentProcess rp " +
                        "where rp.persistenceID = :recruitmentProcessId"
        )
})
public class JobApplication extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.JobApplication.QUERY__ALL";
    public final static String QUERY__BY__RECRUITMENT_PROCESS = "info.dbis.orac.datamodel.JobApplication.QUERY__BY__RECRUITMENT_PROCESS";

    @Lob
    private String jobApplicationText;

    @NotNull
    @ManyToOne
    private RecruitmentProcess recruitmentProcess;

    @OneToMany(mappedBy = "jobApplication", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<JobApplicationReview> jobApplicationReviews = new LinkedHashSet<>();

    @OneToMany(mappedBy = "jobApplication", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Document> documents = new LinkedHashSet<>();

    public JobApplication() {
        super();
    }

    public JobApplication(Agent creator, String jobApplicationText, RecruitmentProcess recruitmentProcess) {
        super(creator);
        this.setJobApplicationText(jobApplicationText);
        this.setRecruitmentProcess(recruitmentProcess);
    }

    public String getJobApplicationText() {
        return jobApplicationText;
    }

    public void setJobApplicationText(String jobApplicationText) {
        this.jobApplicationText = jobApplicationText;
    }

    public RecruitmentProcess getRecruitmentProcess() {
        return recruitmentProcess;
    }

    public void setRecruitmentProcess(RecruitmentProcess recruitmentProcess) {
        if (this.recruitmentProcess != null){
            if (this.recruitmentProcess.getJobApplications().contains(this)){
                this.recruitmentProcess.removeJobApplication(this);
            }
        }
        this.recruitmentProcess = recruitmentProcess;
        if (recruitmentProcess != null){
            if (!recruitmentProcess.getJobApplications().contains(this)){
                recruitmentProcess.addJobApplication(this);
            }
        }
    }

    public Set<JobApplicationReview> getJobApplicationReviews() {
        return Collections.unmodifiableSet(this.jobApplicationReviews);
    }

    private void setJobApplicationReviews(Set<JobApplicationReview> jobApplicationReviews) {
        this.jobApplicationReviews = jobApplicationReviews;
    }

    public void addJobApplicationReview(JobApplicationReview jobApplicationReview){
        if (this.jobApplicationReviews.add(jobApplicationReview)){
            if (!this.equals(jobApplicationReview.getJobApplication())){
                jobApplicationReview.setJobApplication(this);
            }
        }
    }

    public void removeJobApplicationReview(JobApplicationReview jobApplicationReview){
        if (this.jobApplicationReviews.remove(jobApplicationReview)){
            if (this.equals(jobApplicationReview.getJobApplication())){
                jobApplicationReview.setJobApplication(null);
            }
        }
    }

    public Set<Document> getDocuments() {
        return Collections.unmodifiableSet(this.documents);
    }

    private void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    public void addDocument(Document document){
        if (this.documents.add(document)){
            if (!this.equals(document.getJobApplication())){
                document.setJobApplication(this);
            }
        }
    }

    public void removeDocument(Document document){
        if (this.documents.remove(document)){
            if (this.equals(document.getJobApplication())){
                document.setJobApplication(null);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> contextGuardedEntities = new LinkedHashSet<>();
        contextGuardedEntities.add(this.recruitmentProcess);
        return contextGuardedEntities;
    }
}
