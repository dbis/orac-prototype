package info.dbis.orac.datamodel.logging;

/**
 * PersistenceLogEventType
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public enum PersistenceLogEventType {
    ADDED, UPDATED, REMOVED
}
