package info.dbis.orac.datamodel.authorization;

import javax.enterprise.util.Nonbinding;
import javax.interceptor.InterceptorBinding;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * RequiresAccessControl
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Inherited
@InterceptorBinding
@Retention(RUNTIME)
@Target({METHOD, TYPE})
public @interface RequiresAccessControl {
    enum ActionType { ADD, ADD_LINK, READ, READ_ATTRIBUTE, UPDATE, UPDATE_ATTRIBUTE, REMOVE, REMOVE_LINK, LISTING, SEARCH, ADVANCED }

    @Nonbinding Class<? extends GuardedEntity> targetObjectClazz() default GuardedEntity.class;
    @Nonbinding Class<? extends GuardedEntity> contextObjectClazz() default GuardedEntity.class;
    @Nonbinding ActionType actionType() default ActionType.ADVANCED;
}
