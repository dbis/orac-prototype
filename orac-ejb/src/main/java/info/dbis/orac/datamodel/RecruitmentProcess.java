package info.dbis.orac.datamodel;

import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.ws.Holder;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * RecruitmentProcess
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = RecruitmentProcess.QUERY__ALL,
                query = "select rp from RecruitmentProcess rp"
        ),
        @NamedQuery(
                name = RecruitmentProcess.QUERY__BY__HUMAN_RESOURCE_SYSTEM,
                query = "select rp from RecruitmentProcess rp join fetch rp.humanResourceSystem hrs " +
                        "where hrs.persistenceID = :humanResourceSystemId"
        )
})
public class RecruitmentProcess extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.RecruitmentProcess.QUERY__ALL";
    public final static String QUERY__BY__HUMAN_RESOURCE_SYSTEM = "info.dbis.orac.datamodel.RecruitmentProcess.QUERY__BY__HUMAN_RESOURCE_SYSTEM";

    @NotNull
    @ManyToOne(cascade = {CascadeType.PERSIST})
    private HumanResourceSystem humanResourceSystem;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private JobOffer jobOffer;

    @OneToMany(mappedBy = "recruitmentProcess", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<JobApplication> jobApplications = new LinkedHashSet<>();

    @OneToMany(mappedBy = "recruitmentProcess", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<ContractOffer> contractOffers = new LinkedHashSet<>();

    public RecruitmentProcess() {
        super();
    }

    public RecruitmentProcess(Agent creator, HumanResourceSystem humanResourceSystem) {
        super(creator);
        this.humanResourceSystem = humanResourceSystem;
    }

    public HumanResourceSystem getHumanResourceSystem() {
        return this.humanResourceSystem;
    }

    public void setHumanResourceSystem(HumanResourceSystem humanResourceSystem) {
        if (this.humanResourceSystem != null){
            this.humanResourceSystem.removeRecruitmentProcess(this);
        }
        this.humanResourceSystem = humanResourceSystem;
        if (humanResourceSystem != null){
            if (!humanResourceSystem.getRecruitmentProcesses().contains(this)){
                humanResourceSystem.addRecruitmentProcess(this);
            }
        }
    }

    public JobOffer getJobOffer() {
        return this.jobOffer;
    }

    public void setJobOffer(JobOffer jobOffer) {
        this.jobOffer = jobOffer;
        if (jobOffer != null){
            if (!this.equals(jobOffer.getRecruitmentProcess())){
                jobOffer.setRecruitmentProcess(this);
            }
        }
    }

    public Set<JobApplication> getJobApplications() {
        return Collections.unmodifiableSet(jobApplications);
    }

    private void setJobApplications(Set<JobApplication> jobApplications) {
        this.jobApplications = jobApplications;
    }

    public void addJobApplication(JobApplication jobApplication){
        if (this.jobApplications.add(jobApplication)){
            if (!this.equals(jobApplication.getRecruitmentProcess())){
                jobApplication.setRecruitmentProcess(this);
            }
        }
    }

    public void removeJobApplication(JobApplication jobApplication){
        if (this.jobApplications.remove(jobApplication)){
            if (this.equals(jobApplication.getRecruitmentProcess())){
                jobApplication.setRecruitmentProcess(null);
            }
        }
    }

    public Set<ContractOffer> getContractOffers() {
        return Collections.unmodifiableSet(this.contractOffers);
    }

    private void setContractOffers(Set<ContractOffer> contractOffers) {
        this.contractOffers = contractOffers;
    }

    public void addContractOffer(ContractOffer contractOffer){
        if (this.contractOffers.add(contractOffer)){
            if (!this.equals(contractOffer.getRecruitmentProcess())){
                contractOffer.setRecruitmentProcess(this);
            }
        }
    }

    public void removeContractOffer(ContractOffer contractOffer){
        if (this.contractOffers.remove(contractOffer)){
            if (this.equals(contractOffer.getRecruitmentProcess())){
                contractOffer.setRecruitmentProcess(null);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> contextGuardedEntities = new LinkedHashSet<>();
        contextGuardedEntities.add(this.humanResourceSystem);
        return contextGuardedEntities;
    }
}
