package info.dbis.orac.datamodel.authentication;

import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * UserAbilityAssociation
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = UserAbilityAssociation.QUERY__ALL,
                query = "select uaa from UserAbilityAssociation uaa"
        )
})
public class UserAbilityAssociation extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authentication.UserAbilityAssociation.QUERY__ALL";

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private HumanResourceSystem humanResourceSystem;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private User user;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private Ability ability;

    public UserAbilityAssociation() {
        super();
    }

    public UserAbilityAssociation(Agent creator, HumanResourceSystem humanResourceSystem, User user, Ability ability) {
        super(creator);
        this.setHumanResourceSystem(humanResourceSystem);
        this.setUser(user);
        this.setAbility(ability);
    }

    public HumanResourceSystem getHumanResourceSystem() {
        return humanResourceSystem;
    }

    public void setHumanResourceSystem(HumanResourceSystem humanResourceSystem) {
        if (this.humanResourceSystem != null){
            this.humanResourceSystem.removeUserAbilityAssociation(this);
        }
        this.humanResourceSystem = humanResourceSystem;
        if (this.humanResourceSystem != null){
            if (!this.humanResourceSystem.getUserAbilityAssociations().contains(this)){
                this.humanResourceSystem.addUserAbilityAssociation(this);
            }
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        if (this.user != null){
            this.user.removeUserAbilityAssociation(this);
        }
        this.user = user;
        if (this.user != null){
            if (!this.user.getUserAbilityAssociations().contains(this)){
                this.user.addUserAbilityAssociation(this);
            }
        }
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        if (this.ability != null){
            this.ability.removeUserAbilityAssociation(this);
        }
        this.ability = ability;
        if (this.ability != null){
            if (!this.ability.getUserAbilityAssociations().contains(this)){
                this.ability.addUserAbilityAssociation(this);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new LinkedHashSet<>();
        result.add(this.humanResourceSystem);
        return result;
    }
}
