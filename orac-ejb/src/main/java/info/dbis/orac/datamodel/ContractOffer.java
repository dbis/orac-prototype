package info.dbis.orac.datamodel;

import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * ContractOffer
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = ContractOffer.QUERY__ALL,
                query = "select co from ContractOffer co"
        ),
        @NamedQuery(
                name = ContractOffer.QUERY__BY__RECRUITMENT_PROCESS,
                query = "select co from ContractOffer co join fetch co.recruitmentProcess rp " +
                        "where rp.persistenceID = :recruitmentProcessId"
        )
})
public class ContractOffer extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.ContractOffer.QUERY__ALL";
    public final static String QUERY__BY__RECRUITMENT_PROCESS = "info.dbis.orac.datamodel.ContractOffer.QUERY__BY__RECRUITMENT_PROCESS";

    @Lob
    private String contractOfferText;

    @OneToMany(mappedBy = "contractOffer", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Document> documents = new LinkedHashSet<>();

    @NotNull
    @ManyToOne(cascade = {CascadeType.PERSIST})
    private RecruitmentProcess recruitmentProcess;

    public ContractOffer() {
        super();
    }

    public ContractOffer(Agent creator, String contractOfferText, RecruitmentProcess recruitmentProcess) {
        super(creator);
        this.contractOfferText = contractOfferText;
        this.setRecruitmentProcess(recruitmentProcess);
    }

    public String getContractOfferText() {
        return contractOfferText;
    }

    public void setContractOfferText(String contractOfferText) {
        this.contractOfferText = contractOfferText;
    }

    public Set<Document> getDocuments() {
        return Collections.unmodifiableSet(this.documents);
    }

    private void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    public void addDocument(Document document){
        if (this.documents.add(document)){
            if (!this.equals(document.getContractOffer())){
                document.setContractOffer(this);
            }
        }
    }

    public void removeDocument(Document document){
        if (this.documents.remove(document)){
            if (this.equals(document.getContractOffer())){
                document.setContractOffer(null);
            }
        }
    }

    public RecruitmentProcess getRecruitmentProcess() {
        return this.recruitmentProcess;
    }

    public void setRecruitmentProcess(RecruitmentProcess recruitmentProcess) {
        if (this.recruitmentProcess != null){
            if (this.recruitmentProcess.getContractOffers().contains(this)){
                this.recruitmentProcess.removeContractOffer(this);
            }
        }
        this.recruitmentProcess = recruitmentProcess;
        if (recruitmentProcess != null){
            if (!recruitmentProcess.getContractOffers().contains(this)){
                recruitmentProcess.addContractOffer(this);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> contextGuardedEntities = new LinkedHashSet<>();
        contextGuardedEntities.add(this.recruitmentProcess);
        return contextGuardedEntities;
    }
}
