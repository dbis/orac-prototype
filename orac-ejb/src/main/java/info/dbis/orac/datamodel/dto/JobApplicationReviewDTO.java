package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.JobApplicationReview;

/**
 * JobApplicationReviewDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class JobApplicationReviewDTO extends GuardedEntityDTO {

    private String jobApplicationReviewText;

    private Long jobApplicationId;

    public JobApplicationReviewDTO() {
        super();
    }

    public JobApplicationReviewDTO(JobApplicationReview jobApplicationReview) {
        super(jobApplicationReview);
        this.setJobApplicationReviewText(jobApplicationReview.getJobApplicationReviewText());
        this.setJobApplicationId(jobApplicationReview.getJobApplication().getId());
    }

    public String getJobApplicationReviewText() {
        return jobApplicationReviewText;
    }

    public void setJobApplicationReviewText(String jobApplicationReviewText) {
        this.jobApplicationReviewText = jobApplicationReviewText;
    }

    public Long getJobApplicationId() {
        return jobApplicationId;
    }

    public void setJobApplicationId(Long jobApplicationId) {
        this.jobApplicationId = jobApplicationId;
    }
}
