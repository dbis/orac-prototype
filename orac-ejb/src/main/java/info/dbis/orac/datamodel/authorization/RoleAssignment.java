package info.dbis.orac.datamodel.authorization;

import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authentication.Ability;
import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authentication.OrgRole;
import info.dbis.orac.datamodel.authentication.OrgUnit;

import javax.persistence.*;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * RoleAssignment
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = RoleAssignment.QUERY__ALL,
                query = "select ra from RoleAssignment ra"
        ),
        @NamedQuery(
                name = RoleAssignment.QUERY__ID,
                query = "select distinct ra from RoleAssignment ra " +
                        "join fetch ra.roleScopeAssignments rsa join fetch rsa.roleScope rs join fetch rs.roleScopePrivilegeAssociation rspa " +
                        "left join fetch ra.role r join fetch r.keyRoleScope krs left join fetch r.additionalRoleScopes ars " +
                        "join fetch krs.roleScopePrivilegeAssociation krspa left join fetch ars.roleScopePrivilegeAssociation arspa " +
                        "where ra.persistenceID = :persistenceId"
        ),
        @NamedQuery(
                name = RoleAssignment.QUERY__AGENT_ID,
                query = "select distinct ra from RoleAssignment ra " +
                        "join fetch ra.roleScopeAssignments rsa " +
                        "left join fetch ra.agents ag " +
                        "left join ra.orgUnits raou left join ra.orgRoles raor left join ra.abilities raa " +
                        "where (select a from Agent a where a.persistenceID = :agentId) member of ra.agents"
        ),
        @NamedQuery(
                name = RoleAssignment.QUERY__AGENT__TARGET_ENTITY_CLAZZ__ON_ASSIGNMENT_DEFAULT,
                query = "select distinct ra from RoleAssignment ra " +
                        "join fetch ra.roleScopeAssignments rsa " +
                        "join fetch ra.role r " +
                        "left join fetch ra.agents ag " +
                        "where :agent in ag and r.keyRoleScope.targetEntityClazz = :targetEntityClazz and r.keyRoleScope.onAssignmentDefault = :onAssignmentDefault",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")}
        ),
        @NamedQuery(
                name = RoleAssignment.QUERY__AGENT__GUARDED_ENTITY_ID,
                query = "select distinct ra from RoleAssignment ra " +
                        "join fetch ra.roleScopeAssignments rsa " +
                        "left join fetch ra.agents ag " +
                        "where :agent in ag and rsa.guardedEntityId = :guardedEntityId",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")}
        ),
        @NamedQuery(
                name = RoleAssignment.QUERY__USER__GUARDED_ENTITY_ID,
                query = "select distinct ra from RoleAssignment ra " +
                        "join fetch ra.roleScopeAssignments rsa left join ra.agents ag " +
                        "left join ra.orgUnits raou left join ra.orgRoles raor left join ra.abilities raa " +
                        "where rsa.guardedEntityId = :guardedEntityId and " +
                        "(:user in ag or (raa in (select a from Ability a join a.userAbilityAssociations uaa where uaa.user = :user) and " +
                        "raor in (select orgr from OrgRole orgr join orgr.userOrgRoleAssociations uora where uora.user = :user) and " +
                        "raou in (select ou from OrgUnit ou join ou.orgRoles our join our.userOrgRoleAssociations ouruora where ouruora.user = :user)))",
                hints = {@QueryHint(name = "org.hibernate.cacheable", value = "true")}
        )
})
public class RoleAssignment extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authorization.RoleAssignment.QUERY__ALL";
    public final static String QUERY__ID = "info.dbis.orac.datamodel.authorization.RoleAssignment.QUERY__ID";
    public final static String QUERY__AGENT_ID = "info.dbis.orac.datamodel.authorization.RoleAssignment.QUERY__AGENT_ID";
    public final static String QUERY__AGENT__TARGET_ENTITY_CLAZZ__ON_ASSIGNMENT_DEFAULT = "info.dbis.orac.datamodel.authorization.RoleAssignment.QUERY__AGENT__TARGET_ENTITY_CLAZZ__ON_ASSIGNMENT_DEFAULT";
    public final static String QUERY__AGENT__GUARDED_ENTITY_ID = "info.dbis.orac.datamodel.authorization.RoleAssignment.QUERY__AGENT__GUARDED_ENTITY_ID";
    public final static String QUERY__USER__GUARDED_ENTITY_ID = "info.dbis.orac.datamodel.authorization.RoleAssignment.QUERY__USER__GUARDED_ENTITY_ID";

    // Relationships

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private HumanResourceSystem humanResourceSystem;

    // either s set of users or a set of (abilities, orgRoles and OrgUnits). For the latter, the users are determined based on the OrgEntities
    // all relationships are unidirectional
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<Agent> agents = new LinkedHashSet<>();

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<Ability> abilities = new LinkedHashSet<>();

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<OrgRole> orgRoles = new LinkedHashSet<>();

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Set<OrgUnit> orgUnits = new LinkedHashSet<>();

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER, optional = false)
    private Role role;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "roleAssignment", fetch = FetchType.LAZY)
    private Set<RoleScopeAssignment> roleScopeAssignments = new LinkedHashSet<>();

    public RoleAssignment() {
        super();
    }

    public RoleAssignment(Agent creator, HumanResourceSystem humanResourceSystem, Role role, Agent agent) {
        super(creator);
        this.setCreatorId(creator.getId());
        this.setHumanResourceSystem(humanResourceSystem);
        this.addAgent(agent);
        this.setRole(role);
    }

    public RoleAssignment(Agent creator, HumanResourceSystem humanResourceSystem, Role role, Set<Agent> agents) {
        super(creator);
        this.setCreatorId(creator.getId());
        this.setHumanResourceSystem(humanResourceSystem);
        this.setAgents(agents);
        this.setRole(role);
    }

    public RoleAssignment(Agent creator, HumanResourceSystem humanResourceSystem, Role role, Set<OrgUnit> orgUnits, Set<OrgRole> orgRoles, Set<Ability> abilities) {
        super(creator);
        this.setCreatorId(creator.getId());
        this.setHumanResourceSystem(humanResourceSystem);
        this.setRole(role);
        this.setOrgUnits(orgUnits);
        this.setOrgRoles(orgRoles);
        this.setAbilities(abilities);
    }

    // Basic methods

    public HumanResourceSystem getHumanResourceSystem() {
        return humanResourceSystem;
    }

    public void setHumanResourceSystem(HumanResourceSystem humanResourceSystem) {
        if (this.humanResourceSystem != null){
            this.humanResourceSystem.removeRoleAssignment(this);
        }
        this.humanResourceSystem = humanResourceSystem;
        if (humanResourceSystem != null){
            if (!humanResourceSystem.getRoleAssignments().contains(this)){
                humanResourceSystem.addRoleAssignment(this);
            }
        }
    }

    public Set<Agent> getAgents() {
        return Collections.unmodifiableSet(agents);
    }

    private void setAgents(Set<Agent> agents) {
        this.agents = agents;
    }

    public void addAgent(Agent agent){
        this.agents.add(agent);
    }


    public void removeAgent(Agent agent){
        this.agents.remove(agent);
    }

    public Set<Ability> getAbilities() {
        return Collections.unmodifiableSet(abilities);
    }

    private void setAbilities(Set<Ability> abilities) {
        this.abilities = abilities;
    }

    public void addAbility(Ability ability){
        this.abilities.add(ability);
    }


    public void removeAbility(Ability ability){
        this.abilities.remove(ability);
    }

    public Set<OrgRole> getOrgRoles() {
        return Collections.unmodifiableSet(orgRoles);
    }

    private void setOrgRoles(Set<OrgRole> orgRoles) {
        this.orgRoles = orgRoles;
    }

    public void addOrgRole(OrgRole orgRole){
        this.orgRoles.add(orgRole);
    }

    public void removeOrgRole(OrgRole orgRole){
        this.orgRoles.remove(orgRole);
    }

    public Set<OrgUnit> getOrgUnits() {
        return Collections.unmodifiableSet(orgUnits);
    }

    private void setOrgUnits(Set<OrgUnit> orgUnits) {
        this.orgUnits = orgUnits;
    }

    public void addOrgUnit(OrgUnit orgUnit){
        this.orgUnits.add(orgUnit);
    }


    public void removeOrgUnit(OrgUnit orgUnit){
        this.orgUnits.remove(orgUnit);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        if (this.role != null){
            this.role.removeRoleAssignment(this);
        }
        this.role = role;
        if (role != null){
            if (!role.getRoleAssignments().contains(this)){
                role.addRoleAssignment(this);
            }
        }
    }

    public Set<RoleScopeAssignment> getRoleScopeAssignments() {
        return Collections.unmodifiableSet(roleScopeAssignments);
    }

    private void setRoleScopeAssignments(Set<RoleScopeAssignment> roleScopeAssignments) {
        this.roleScopeAssignments = roleScopeAssignments;
    }

    public void addRoleScopeAssignment(RoleScopeAssignment roleScopeAssignment){
        if (this.roleScopeAssignments.add(roleScopeAssignment)){
            if (!this.equals(roleScopeAssignment.getRoleAssignment())){
                roleScopeAssignment.setRoleAssignment(this);
            }
        }
    }

    public void removeRoleScopeAssignment(RoleScopeAssignment roleScopeAssignment){
        if (this.roleScopeAssignments.remove(roleScopeAssignment)){
            if (this.equals(roleScopeAssignment.getRoleAssignment())){
                roleScopeAssignment.setRoleAssignment(null);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        // this method cannot return the GuardedEntity since we can only persist the id of the referenced GuardedEntities in the RoleScopeAssignments (IMPORTANT)
        Set<GuardedEntity> result = new LinkedHashSet<>();
        return result;
    }
}