package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.Document;

/**
 * DocumentDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class DocumentDTO extends GuardedEntityDTO {

    private String name;

    private String documentContent;

    public DocumentDTO() {
    }

    public DocumentDTO(Document document) {
        super(document);
        this.setName(document.getName());
        this.setDocumentContent(document.getDocumentContent());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocumentContent() {
        return documentContent;
    }

    public void setDocumentContent(String documentContent) {
        this.documentContent = documentContent;
    }
}
