package info.dbis.orac.datamodel.dto.authorization;

import info.dbis.orac.datamodel.authorization.RoleScope;
import info.dbis.orac.datamodel.dto.GuardedEntityDTO;

import java.util.LinkedList;
import java.util.List;

/**
 * RoleScopeDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class RoleScopeDTO extends GuardedEntityDTO {

    private String targetEntityClazz;

    private List<String> contextEntitiesClazzes;

    private Boolean isScopeManager;

    private Boolean onCreationDefault;

    private Boolean onAssignmentDefault;

    private Integer minimumNumberOfReferencedEntities = -1;

    private int maximumNumberOfReferencedEntities = -1;

    public RoleScopeDTO() {
        super();
    }

    public RoleScopeDTO(RoleScope roleScope) {
        super(roleScope);
        this.targetEntityClazz = roleScope.getTargetEntityClazz().getName();
        this.contextEntitiesClazzes = new LinkedList<>();
        roleScope.getContextEntitiesClazzes().forEach(aClass -> this.contextEntitiesClazzes.add(aClass.getName()));
        this.isScopeManager = roleScope.isScopeManager();
        this.onCreationDefault = roleScope.isOnCreationDefault();
        this.onAssignmentDefault = roleScope.isOnAssignmentDefault();
        this.minimumNumberOfReferencedEntities = roleScope.getMinimumNumberOfReferencedEntities();
        this.maximumNumberOfReferencedEntities = roleScope.getMaximumNumberOfReferencedEntities();
    }

    public String getTargetEntityClazz() {
        return targetEntityClazz;
    }

    public void setTargetEntityClazz(String targetEntityClazz) {
        this.targetEntityClazz = targetEntityClazz;
    }

    public List<String> getContextEntitiesClazzes() {
        return contextEntitiesClazzes;
    }

    public void setContextEntitiesClazzes(List<String> contextEntitiesClazzes) {
        this.contextEntitiesClazzes = contextEntitiesClazzes;
    }

    public Boolean getScopeManager() {
        return isScopeManager;
    }

    public void setScopeManager(Boolean scopeManager) {
        isScopeManager = scopeManager;
    }

    public Boolean getOnCreationDefault() {
        return onCreationDefault;
    }

    public void setOnCreationDefault(Boolean onCreationDefault) {
        this.onCreationDefault = onCreationDefault;
    }

    public Boolean getOnAssignmentDefault() {
        return onAssignmentDefault;
    }

    public void setOnAssignmentDefault(Boolean onAssignmentDefault) {
        this.onAssignmentDefault = onAssignmentDefault;
    }

    public Integer getMinimumNumberOfReferencedEntities() {
        return minimumNumberOfReferencedEntities;
    }

    public void setMinimumNumberOfReferencedEntities(Integer minimumNumberOfReferencedEntities) {
        this.minimumNumberOfReferencedEntities = minimumNumberOfReferencedEntities;
    }

    public int getMaximumNumberOfReferencedEntities() {
        return maximumNumberOfReferencedEntities;
    }

    public void setMaximumNumberOfReferencedEntities(int maximumNumberOfReferencedEntities) {
        this.maximumNumberOfReferencedEntities = maximumNumberOfReferencedEntities;
    }
}
