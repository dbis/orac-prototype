package info.dbis.orac.datamodel.authentication;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Agent
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Agent extends OrgEntity{

    public Agent() {
        super();
    }

    public Agent(Agent creator) {
        super(creator);
    }

    public abstract String getName();
}
