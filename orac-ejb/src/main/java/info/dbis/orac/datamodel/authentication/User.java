package info.dbis.orac.datamodel.authentication;

import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * This is the basic class representing Users.
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
		@NamedQuery(
		        name = User.QUERY__ALL,
                query = "select u from User u"
        ),
        @NamedQuery(
                name = User.QUERY__EMAIL,
                query = "select u from User u where u.email = :email"
        )
})
public class User extends Agent {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authentication.User.QUERY__ALL";
    public final static String QUERY__EMAIL = "info.dbis.orac.datamodel.authentication.User.QUERY__EMAIL";

	// Basic attributes

    @Column(nullable = false)
    @Size(min = 1)
	private String firstName = "";

    @Column(nullable = false)
    @Size(min = 1)
	private String lastName = "";

	@Column(nullable = false, unique = true)
	@Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
			+"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
			+"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
			message="{invalid.email}")
	private String email = "";

	@Column(nullable = false, length = 256)
	private String password;

    private boolean male = false;

    private String title = "";

    private String location = "";

    private String phone = "";

    private String website = "";

    private boolean newsletterSubscription = false;

    @Lob
    private String biography;


    // the HumanResourceSystem the user belongs to
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private HumanResourceSystem humanResourceSystem;

    // Organizational aspects

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Set<UserOrgRoleAssociation> userOrgRoleAssociations = new LinkedHashSet<>();

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Set<UserAbilityAssociation> userAbilityAssociations = new LinkedHashSet<>();

	// Constructor

	public User(){
        super();
	}


    public User(Agent creator, String firstName, String lastName, String email, boolean male,
                String title, String location, String phone, String website, boolean newsletterSubscription, String biography, HumanResourceSystem humanResourceSystem) {
        super(creator);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setEmail(email);
        this.setMale(male);
        this.setTitle(title);
        this.setLocation(location);
        this.setPhone(phone);
        this.setWebsite(website);
        this.setNewsletterSubscription(newsletterSubscription);
        this.setBiography(biography);
        this.setHumanResourceSystem(humanResourceSystem);
    }

    // Basic methods

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String prename) {
        if (prename != null){
            this.firstName = prename;
        }
        else {
            this.firstName = "";
        }
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String surname) {
        if (surname != null){
            this.lastName = surname;
        }
        else {
            this.lastName = "";
        }
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
	    if (email != null){
            this.email = email;
        }
        else {
            this.email = "";
        }
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (title != null){
            this.title = title;
        }
        else {
            this.title = "";
        }
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        if (location != null){
            this.location = location;
        }
        else {
            this.location = "";
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        if (phone != null){
            this.phone = phone;
        }
        else {
            this.phone = "";
        }
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        if (website != null){
            this.website = website;
        }
        else {
            this.website = "";
        }
    }

    public boolean hasNewsletterSubscription() {
        return newsletterSubscription;
    }

    public void setNewsletterSubscription(boolean newsletterSubscription) {
        this.newsletterSubscription = newsletterSubscription;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        if (biography != null){
            this.biography = biography;
        }
        else {
            this.biography = "";
        }
    }

    // Relationships

    public HumanResourceSystem getHumanResourceSystem() {
        return humanResourceSystem;
    }

    public void setHumanResourceSystem(HumanResourceSystem humanResourceSystem) {
        if (this.humanResourceSystem != null){
            this.humanResourceSystem.removeUser(this);
        }
        this.humanResourceSystem = humanResourceSystem;
        if (humanResourceSystem != null){
            if (!humanResourceSystem.getUsers().contains(this)){
                humanResourceSystem.addUser(this);
            }
        }
    }

    public Set<UserOrgRoleAssociation> getUserOrgRoleAssociations() {
        return Collections.unmodifiableSet(userOrgRoleAssociations);
    }

    private void setUserOrgRoleAssociations(Set<UserOrgRoleAssociation> userOrgRoleAssociations) {
        this.userOrgRoleAssociations = userOrgRoleAssociations;
    }

    public void addUserOrgRoleAssociation(UserOrgRoleAssociation userOrgRoleAssociation){
        if (this.userOrgRoleAssociations.add(userOrgRoleAssociation)){
            if (!this.equals(userOrgRoleAssociation.getUser())){
                userOrgRoleAssociation.setUser(this);
            }
        }
    }

    public void removeUserOrgRoleAssociation(UserOrgRoleAssociation userOrgRoleAssociation){
        if (this.userOrgRoleAssociations.remove(userOrgRoleAssociation)){
            if (this.equals(userOrgRoleAssociation.getUser())){
                userOrgRoleAssociation.setUser(null);
            }
        }
    }

    public Set<UserAbilityAssociation> getUserAbilityAssociations() {
        return Collections.unmodifiableSet(userAbilityAssociations);
    }

    private void setUserAbilityAssociations(Set<UserAbilityAssociation> userAbilityAssociations) {
        this.userAbilityAssociations = userAbilityAssociations;
    }

    public void addUserAbilityAssociation(UserAbilityAssociation userAbilityAssociation){
        if (this.userAbilityAssociations.add(userAbilityAssociation)){
            if (!this.equals(userAbilityAssociation.getUser())){
                userAbilityAssociation.setUser(this);
            }
        }
    }

    public void removeUserAbilityAssociation(UserAbilityAssociation userAbilityAssociation){
        if (this.userAbilityAssociations.remove(userAbilityAssociation)){
            if (this.equals(userAbilityAssociation.getUser())){
                userAbilityAssociation.setUser(null);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new HashSet<>();
        result.add(this.humanResourceSystem);
        return result;
    }

    @Override
    public String getName() {
        return this.firstName +" "+this.lastName;
    }

    @Override
	public String toString() {
		return "User [ID=" + this.getId() + ", prename=" + this.getFirstName() + ", surname=" + this.getLastName() + ", email=" + email + "]";
	}

}