package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.authorization.GuardedEntity;

/**
 * GuardedEntityDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

public abstract class GuardedEntityDTO extends BaseEntityDTO {

    private Long creatorId;
    private String creatorName;

    public GuardedEntityDTO() {
        super();
    }

    public GuardedEntityDTO(GuardedEntity guardedEntity){
        super(guardedEntity);
        this.setCreatorId(guardedEntity.getCreatorId());
        this.setCreatorName(guardedEntity.getCreatorName());
    }

    public Long getCreatorId() {
        return creatorId;
    }

    // not to be changed in client -> no privilege needed
    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    // not to be changed in client -> no privilege needed
    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }
}
