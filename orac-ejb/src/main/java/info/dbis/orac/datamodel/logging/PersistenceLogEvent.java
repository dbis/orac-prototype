package info.dbis.orac.datamodel.logging;

import info.dbis.orac.datamodel.authorization.GuardedEntity;

/**
 * PersistenceLogEvent
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class PersistenceLogEvent {

    private Long guardedEntityId;
    private Class<? extends GuardedEntity> guardedEntityClazz;
    private PersistenceLogEventType persistenceLogEventType;
    private GuardedEntity guardedEntity;

    public PersistenceLogEvent(Long guardedEntityId, Class<? extends GuardedEntity> guardedEntityClazz, PersistenceLogEventType persistenceLogEventType) {
        this.guardedEntityId = guardedEntityId;
        this.guardedEntityClazz = guardedEntityClazz;
        this.persistenceLogEventType = persistenceLogEventType;
    }

    public Long getGuardedEntityId() {
        return guardedEntityId;
    }

    public Class<? extends GuardedEntity> getGuardedEntityClazz() {
        return guardedEntityClazz;
    }

    public PersistenceLogEventType getPersistenceLogEventType() {
        return persistenceLogEventType;
    }

    public GuardedEntity getGuardedEntity() {
        return guardedEntity;
    }

    public void setGuardedEntity(GuardedEntity guardedEntity) {
        this.guardedEntity = guardedEntity;
    }
}