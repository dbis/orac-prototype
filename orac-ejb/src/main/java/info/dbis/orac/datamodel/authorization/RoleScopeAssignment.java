package info.dbis.orac.datamodel.authorization;


import info.dbis.orac.datamodel.authentication.Agent;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * RoleScopeAssignment
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = RoleScopeAssignment.QUERY__ALL,
                query = "select rsa from RoleScopeAssignment rsa"
        ),
        @NamedQuery(
                name = RoleScopeAssignment.QUERY__GUARDED_ENTITY_ID,
                query = "select distinct rsa from RoleScopeAssignment rsa join fetch rsa.roleAssignment ra " +
                        "join fetch rsa.roleScope rs " +
                        "where rsa.guardedEntityId = :guardedEntityId"
        ),
})
public class RoleScopeAssignment extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authorization.RoleScopeAssignment.QUERY__ALL";
    public final static String QUERY__GUARDED_ENTITY_ID = "info.dbis.orac.datamodel.authorization.RoleScopeAssignment.QUERY__GUARDED_ENTITY_ID";

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private RoleAssignment roleAssignment;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = false)
    private RoleScope roleScope;

    private long guardedEntityId;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private RoleScopePrivilegeAssociation additionalRoleScopePrivilegeAssociation;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private RoleScopePrivilegeAssociation restrictingRoleScopePrivilegeAssociation;

    public RoleScopeAssignment() {
        super();
    }

    public RoleScopeAssignment(Agent creator, RoleAssignment roleAssignment, RoleScope roleScope, GuardedEntity guardedEntity) {
        super(creator);
        this.setRoleAssignment(roleAssignment);
        this.setRoleScope(roleScope);
        this.setGuardedEntityId(guardedEntity.getId());
    }

    public RoleAssignment getRoleAssignment() {
        return roleAssignment;
    }

    public void setRoleAssignment(RoleAssignment roleAssignment) {
        if (this.roleAssignment != null){
            this.roleAssignment.removeRoleScopeAssignment(this);
        }
        this.roleAssignment = roleAssignment;
        if (roleAssignment != null){
            if (!roleAssignment.getRoleScopeAssignments().contains(this)){
                roleAssignment.addRoleScopeAssignment(this);
            }
        }
    }

    public RoleScope getRoleScope() {
        return roleScope;
    }

    public void setRoleScope(RoleScope roleScope) {
        if (this.roleScope != null){
            this.roleScope.removeRoleScopeAssignment(this);
        }
        this.roleScope = roleScope;
        if (roleScope != null){
            if (!roleScope.getRoleScopeAssignments().contains(this)){
                roleScope.addRoleScopeAssignment(this);
            }
        }
    }

    public long getGuardedEntityId() {
        return guardedEntityId;
    }

    public void setGuardedEntityId(long guardedEntityId) {
        this.guardedEntityId = guardedEntityId;
    }

    public RoleScopePrivilegeAssociation getAdditionalRoleScopePrivilegeAssociation() {
        return this.additionalRoleScopePrivilegeAssociation;
    }

    public void setAdditionalRoleScopePrivilegeAssociation(RoleScopePrivilegeAssociation additionalRoleScopePrivilegeAssociation) {
        if (this.additionalRoleScopePrivilegeAssociation.getRoleScopeAssignmentWithAdditionalRPA() == this){
            this.additionalRoleScopePrivilegeAssociation.setRoleScopeAssignmentWithAdditionalRPA(null);
        }
        this.additionalRoleScopePrivilegeAssociation = additionalRoleScopePrivilegeAssociation;
        if (additionalRoleScopePrivilegeAssociation.getRoleScopeAssignmentWithAdditionalRPA() != null){
            additionalRoleScopePrivilegeAssociation.setRoleScopeAssignmentWithAdditionalRPA(this);
        }
    }

    public RoleScopePrivilegeAssociation getRestrictingRoleScopePrivilegeAssociation() {
        return this.restrictingRoleScopePrivilegeAssociation;
    }

    public void setRestrictingRoleScopePrivilegeAssociation(RoleScopePrivilegeAssociation restrictingRoleScopePrivilegeAssociation) {
        if (this.restrictingRoleScopePrivilegeAssociation.getRoleScopeAssignmentWithRestrictingRPA() == this){
            this.restrictingRoleScopePrivilegeAssociation.setRoleScopeAssignmentWithRestrictingRPA(null);
        }
        this.restrictingRoleScopePrivilegeAssociation = restrictingRoleScopePrivilegeAssociation;
        if (restrictingRoleScopePrivilegeAssociation.getRoleScopeAssignmentWithRestrictingRPA() != null){
            restrictingRoleScopePrivilegeAssociation.setRoleScopeAssignmentWithRestrictingRPA(this);
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new LinkedHashSet<>();
        result.add(roleAssignment);
        return result;
    }


}
