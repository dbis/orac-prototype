package info.dbis.orac.datamodel.authorization;

import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authentication.Agent;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Role
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = Role.QUERY__ALL,
                query = "select r from Role r"
        ),
        @NamedQuery(
                name = Role.QUERY__ID,
                query = "select distinct r from Role r join fetch r.keyRoleScope krs " +
                        "left join fetch r.additionalRoleScopes ars " +
                        "left join fetch r.refinedParentalRole rpr left join fetch r.requiredContextRoles rcr " +
                        "left join fetch r.refinableChildrenRoles rchr " +
                        "where r.persistenceID = :roleId"
        ),
        @NamedQuery(
                name = Role.QUERY__HUMAN_RESOURCE_SYSTEM_ID,
                query = "select distinct r from Role r join fetch r.keyRoleScope krs " +
                        "left join fetch r.additionalRoleScopes ars " +
                        "left join fetch r.refinedParentalRole rpr left join fetch r.requiredContextRoles rcr " +
                        "left join fetch r.refinableChildrenRoles rchr join fetch r.humanResourceSystem hrs " +
                        "where hrs.persistenceID = :humanResourceSystemId"
        ),
        @NamedQuery(
                name = Role.QUERY__ASSIGNMENT_DEFAULT__TARGET_CLAZZ,
                query = "select distinct r from Role r " +
                        "join fetch r.keyRoleScope krs left join fetch r.additionalRoleScopes ars " +
                        "where (krs.targetEntityClazz = :targetEntityClazz and krs.onAssignmentDefault = :onAssignmentDefault) " +
                        "or (ars.targetEntityClazz = :targetEntityClazz and ars.onAssignmentDefault = :onAssignmentDefault)"
        ),
        @NamedQuery(
                name = Role.QUERY__KEY_SCOPE__CREATION_DEFAULT__TARGET_CLAZZ__CONTEXT_CLAZZ,
                query = "select distinct r from Role r join r.keyRoleScope krs " +
                        "where (krs.targetEntityClazz = :targetEntityClazz and " +
                        ":contextEntityClazz member of krs.contextEntitiesClazzes and krs.onCreationDefault = :onCreationDefault)"),
})
public class Role extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authorization.Role.QUERY__ALL";
    public final static String QUERY__ID = "info.dbis.orac.datamodel.authorization.Role.QUERY__ID";
    public final static String QUERY__HUMAN_RESOURCE_SYSTEM_ID = "info.dbis.orac.datamodel.authorization.Role.QUERY__HUMAN_RESOURCE_SYSTEM_ID";
    public final static String QUERY__ASSIGNMENT_DEFAULT__TARGET_CLAZZ = "info.dbis.orac.datamodel.authorization.Role.QUERY__ASSIGNMENT_DEFAULT__TARGET_CLAZZ";
    public final static String QUERY__KEY_SCOPE__CREATION_DEFAULT__TARGET_CLAZZ__CONTEXT_CLAZZ = "info.dbis.orac.datamodel.authorization.Role.QUERY__KEY_SCOPE__CREATION_DEFAULT__TARGET_CLAZZ__CONTEXT_CLAZZ";

    @NotNull
    private String name = "";

    private String description = "";

    @OneToMany(cascade = CascadeType.PERSIST) // unidirectional
    private Set<Role> requiredContextRoles = new LinkedHashSet<>();

    @ManyToOne(cascade = CascadeType.PERSIST)
    private Role refinedParentalRole;

    @OneToMany(mappedBy = "refinedParentalRole", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Set<Role> refinableChildrenRoles = new LinkedHashSet<>();

    @OneToOne(mappedBy = "roleWithKeyRoleScope", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER, optional = false)
    private RoleScope keyRoleScope;

    @OneToMany(mappedBy = "roleWithAdditionalRoleScope", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<RoleScope> additionalRoleScopes = new LinkedHashSet<>();

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private HumanResourceSystem humanResourceSystem;

    @OneToMany(mappedBy = "role", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.LAZY)
    private Set<RoleAssignment> roleAssignments;

    public Role(){
        super();
    }

    public Role(Agent creator, String name, RoleScope keyRoleScope){
        super(creator);
        this.setName(name);
        this.setKeyRoleScope(keyRoleScope);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null){
            this.name = name;
        }
        else {
            this.name = "";
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Role> getRequiredContextRoles() {
        return Collections.unmodifiableSet(requiredContextRoles);
    }

    private void setRequiredContextRoles(Set<Role> requiredContextRoles) {
        this.requiredContextRoles = requiredContextRoles;
    }

    public void addRequiredContextRole(Role requiredContextRole) {
        this.requiredContextRoles.add(requiredContextRole);
    }

    public void removeRequiredContextRole(Role requiredContextRole) {
        this.requiredContextRoles.remove(requiredContextRole);
    }

    public Role getRefinedParentalRole() {
        return refinedParentalRole;
    }

    public void setRefinedParentalRole(Role refinedParentalRole) {
        if (this.refinedParentalRole != null){
            this.refinedParentalRole.removeRefinableChildRole(this);
        }
        this.refinedParentalRole = refinedParentalRole;
        if (refinedParentalRole != null){
            refinedParentalRole.addRefinableChildRole(this);
        }
    }

    public Set<Role> getRefinableChildrenRoles() {
        return Collections.unmodifiableSet(refinableChildrenRoles);
    }

    private void setRefinableChildrenRoles(Set<Role> refinableChildrenRoles) {
        this.refinableChildrenRoles = refinableChildrenRoles;
    }

    public void addRefinableChildRole(Role refinableChildRole){
        if (this.refinableChildrenRoles.add(refinableChildRole)){
            if (!this.equals(refinableChildRole.getRefinedParentalRole())){
                refinableChildRole.setRefinedParentalRole(refinableChildRole);
            }
        }
    }

    public void removeRefinableChildRole(Role refinableChildRole){
        if (this.refinableChildrenRoles.remove(refinableChildRole)){
            if (this.equals(refinableChildRole.getRefinedParentalRole())){
                refinableChildRole.setRefinedParentalRole(null);
            }
        }

    }

    public RoleScope getKeyRoleScope() {
        return keyRoleScope;
    }

    public void setKeyRoleScope(RoleScope keyRoleScope) {
        if (this.keyRoleScope != null){
            this.keyRoleScope.setRoleWithKeyRoleScope(null);
        }
        this.keyRoleScope = keyRoleScope;
        if (keyRoleScope != null){
            keyRoleScope.setRoleWithKeyRoleScope(this);
        }
    }

    public Set<RoleScope> getAdditionalRoleScopes() {
        return Collections.unmodifiableSet(additionalRoleScopes);
    }

    private void setAdditionalRoleScopes(Set<RoleScope> additionalRoleScopes) {
        this.additionalRoleScopes = additionalRoleScopes;
    }

    public void addAdditionalScope(RoleScope additionalRoleScope){
        if (this.additionalRoleScopes.add(additionalRoleScope)){
            if (!this.equals(additionalRoleScope.getRoleWithAdditionalRoleScope())){
                additionalRoleScope.setRoleWithAdditionalRoleScope(this);
            }
        }
    }

    public void removeAdditionalScope(RoleScope additionalRoleScope){
        if (this.additionalRoleScopes.remove(additionalRoleScope)){
            if (this.equals(additionalRoleScope.getRoleWithAdditionalRoleScope())){
                additionalRoleScope.setRoleWithAdditionalRoleScope(null);
            }
        }
    }

    public HumanResourceSystem getHumanResourceSystem() {
        return humanResourceSystem;
    }

    public void setHumanResourceSystem(HumanResourceSystem humanResourceSystem) {
        if (this.humanResourceSystem != null){
            this.humanResourceSystem.removeRole(this);
        }
        this.humanResourceSystem = humanResourceSystem;
        if (!humanResourceSystem.getRoles().contains(this)){
            humanResourceSystem.addRole(this);
        }
    }

    public Set<RoleAssignment> getRoleAssignments() {
        return Collections.unmodifiableSet(this.roleAssignments);
    }

    public void setRoleAssignments(Set<RoleAssignment> roleAssignments) {
        this.roleAssignments = roleAssignments;
    }

    public void addRoleAssignment(RoleAssignment roleAssignment){
        if (this.roleAssignments.add(roleAssignment)){
            if (!this.equals(roleAssignment.getRole())){
                roleAssignment.setRole(this);
            }
        }
    }

    public void removeRoleAssignment(RoleAssignment roleAssignment){
        if (this.roleAssignments.remove(roleAssignment)){
            if (this.equals(roleAssignment.getRole())){
                roleAssignment.setRole(null);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new LinkedHashSet<>();
        if (this.humanResourceSystem != null)
            result.add(this.humanResourceSystem);
        return result;
    }
}
