package info.dbis.orac.datamodel.authentication;

import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * OrgUnit
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@NamedQueries({
        @NamedQuery(
                name = OrgUnit.QUERY__ALL,
                query = "select ou from OrgUnit ou"
        )
})
public class OrgUnit extends OrgEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.authentication.OrgUnit.QUERY__ALL";

    // Attributes

    @NotNull
    @Size(min = 1)
    private String name = "";

    private String description = "";

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private HumanResourceSystem humanResourceSystem;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private OrgUnit parentOrgUnit;

    @OneToMany(mappedBy = "parentOrgUnit", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<OrgUnit> subOrgUnits = new LinkedHashSet<>();

    @OneToMany(mappedBy = "orgUnit", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<OrgRole> orgRoles = new LinkedHashSet<>();

    @OneToMany(mappedBy = "orgUnit", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<UserOrgRoleAssociation> userOrgRoleAssociations = new LinkedHashSet<>();

    public OrgUnit(){
        super();
    }

    public OrgUnit(Agent creator, String name, String description, HumanResourceSystem humanResourceSystem){
        super(creator);
        this.setName(name);
        this.setDescription(description);
        this.setHumanResourceSystem(humanResourceSystem);
    }

    public OrgUnit(Agent creator, String name, String description, OrgUnit parentOrgUnit){
        super(creator);
        this.setName(name);
        this.setDescription(description);
        this.setParentOrgUnit(parentOrgUnit);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null){
            this.name = name;
        }
        else {
            this.name = "";
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description != null){
            this.description = description;
        }
        else {
            this.description = "";
        }
    }

    public HumanResourceSystem getHumanResourceSystem() {
        return this.humanResourceSystem;
    }

    public void setHumanResourceSystem(HumanResourceSystem humanResourceSystem) {
        if (this.humanResourceSystem != null){
            this.humanResourceSystem.removeOrgUnit(this);
        }
        this.humanResourceSystem = humanResourceSystem;
        if (humanResourceSystem != null){
            if (!humanResourceSystem.getOrgUnits().contains(this)){
                humanResourceSystem.addOrgUnit(this);
            }
        }
    }

    public OrgUnit getParentOrgUnit() {
        return parentOrgUnit;
    }

    public void setParentOrgUnit(OrgUnit parentOrgUnit) {
        if (this.parentOrgUnit != null){
            this.parentOrgUnit.removeSubOrgUnit(this);
        }
        this.parentOrgUnit = parentOrgUnit;
        if (this.parentOrgUnit != null) {
            if (!this.parentOrgUnit.getSubOrgUnits().contains(this)) {
                this.parentOrgUnit.addSubOrgUnit(this);
            }
        }
    }

    public Set<OrgUnit> getSubOrgUnits() {
        return Collections.unmodifiableSet(subOrgUnits);
    }

    private void setSubOrgUnits(Set<OrgUnit> subOrgUnits) {
        this.subOrgUnits = subOrgUnits;
    }

    public void addSubOrgUnit(OrgUnit orgUnit){
        if (this.subOrgUnits.add(orgUnit)){
            if (!this.equals(orgUnit.getParentOrgUnit())){
                orgUnit.setParentOrgUnit(this);
            }
        }
    }

    public void removeSubOrgUnit(OrgUnit orgUnit){
        if (this.subOrgUnits.remove(orgUnit)){
            if (this.equals(orgUnit.getParentOrgUnit())) {
                orgUnit.setParentOrgUnit(null);
            }
        }
    }

    public Set<OrgRole> getOrgRoles() {
        return Collections.unmodifiableSet(orgRoles);
    }

    private void setOrgRoles(Set<OrgRole> orgroles) {
        this.orgRoles = orgroles;
    }

    public void addOrgRole(OrgRole orgRole){
        if (this.orgRoles.add(orgRole)){
            if (!this.equals(orgRole.getOrgUnit())) {
                orgRole.setOrgUnit(this);
            }
        }
    }

    public void removeOrgRole(OrgRole orgRole){
        if (this.orgRoles.remove(orgRole)){
            if (this.equals(orgRole.getOrgUnit())) {
                orgRole.setOrgUnit(null);
            }
        }
    }

    public Set<UserOrgRoleAssociation> getUserOrgRoleAssociations() {
        return Collections.unmodifiableSet(userOrgRoleAssociations);
    }

    private void setUserOrgRoleAssociations(Set<UserOrgRoleAssociation> userOrgRoleAssociations) {
        this.userOrgRoleAssociations = userOrgRoleAssociations;
    }

    public void addUserOrgRoleAssociation(UserOrgRoleAssociation userOrgRoleAssociation){
        if (this.userOrgRoleAssociations.add(userOrgRoleAssociation)){
            if (!this.equals(userOrgRoleAssociation.getOrgUnit())){
                userOrgRoleAssociation.setOrgUnit(this);
            }
        }
    }

    public void removeUserOrgRoleAssociation(UserOrgRoleAssociation userOrgRoleAssociation){
        if (this.userOrgRoleAssociations.remove(userOrgRoleAssociation)){
            if (this.equals(userOrgRoleAssociation.getOrgUnit())){
                userOrgRoleAssociation.setOrgUnit(null);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> result = new LinkedHashSet<>();
        if (this.parentOrgUnit != null){
            result.add(parentOrgUnit);
        }
        else{
            result.add(this.humanResourceSystem);
        }
        return result;
    }

}
