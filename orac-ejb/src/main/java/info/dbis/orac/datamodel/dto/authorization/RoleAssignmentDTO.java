package info.dbis.orac.datamodel.dto.authorization;

import info.dbis.orac.datamodel.authorization.RoleAssignment;
import info.dbis.orac.datamodel.dto.GuardedEntityDTO;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * RoleAssignmentDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class RoleAssignmentDTO extends GuardedEntityDTO {
    Set<Long> agentIds;

    Set<Long> orgUnitIds;

    Set<Long> orgRoleIds;

    Set<Long> abilitiesIds;

    Long roleId;

    Set<RoleScopeAssignmentDTO> roleScopeAssignments;

    public RoleAssignmentDTO() {
        super();
    }

    public RoleAssignmentDTO(RoleAssignment roleAssignment){
        super(roleAssignment);
        this.agentIds = new LinkedHashSet<>();
        roleAssignment.getAgents().forEach(agent -> this.agentIds.add(agent.getId()));

        this.orgUnitIds = new LinkedHashSet<>();
        roleAssignment.getOrgUnits().forEach(orgUnit -> this.orgUnitIds.add(orgUnit.getId()));

        this.orgRoleIds = new LinkedHashSet<>();
        roleAssignment.getOrgRoles().forEach(orgRole -> this.orgRoleIds.add(orgRole.getId()));

        this.abilitiesIds = new LinkedHashSet<>();
        roleAssignment.getAbilities().forEach(ability -> this.abilitiesIds.add(ability.getId()));

        this.roleId = roleAssignment.getRole().getId();

        this.roleScopeAssignments = new LinkedHashSet<>();
        roleAssignment.getRoleScopeAssignments().forEach(roleScopeAssignment -> this.roleScopeAssignments.add(new RoleScopeAssignmentDTO(roleScopeAssignment)));
    }

    public Set<Long> getAgentIds() {
        return agentIds;
    }

    public void setAgentIds(Set<Long> agentIds) {
        this.agentIds = agentIds;
    }

    public Set<Long> getOrgUnitIds() {
        return orgUnitIds;
    }

    public void setOrgUnitIds(Set<Long> orgUnitIds) {
        this.orgUnitIds = orgUnitIds;
    }

    public Set<Long> getOrgRoleIds() {
        return orgRoleIds;
    }

    public void setOrgRoleIds(Set<Long> orgRoleIds) {
        this.orgRoleIds = orgRoleIds;
    }

    public Set<Long> getAbilitiesIds() {
        return abilitiesIds;
    }

    public void setAbilitiesIds(Set<Long> abilitiesIds) {
        this.abilitiesIds = abilitiesIds;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Set<RoleScopeAssignmentDTO> getRoleScopeAssignments() {
        return roleScopeAssignments;
    }

    public void setRoleScopeAssignments(Set<RoleScopeAssignmentDTO> roleScopeAssignments) {
        this.roleScopeAssignments = roleScopeAssignments;
    }

}
