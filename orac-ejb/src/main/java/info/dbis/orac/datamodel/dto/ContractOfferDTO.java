package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.ContractOffer;

/**
 * ContractOfferDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class ContractOfferDTO extends GuardedEntityDTO {

    private String contractOfferText;

    private Long recruitmentProcessId;

    public ContractOfferDTO() {
        super();
    }

    public ContractOfferDTO(ContractOffer contractOffer) {
        super(contractOffer);
        this.setContractOfferText(contractOffer.getContractOfferText());
        this.setRecruitmentProcessId(contractOffer.getRecruitmentProcess().getId());
    }

    public String getContractOfferText() {
        return contractOfferText;
    }

    public void setContractOfferText(String contractOfferText) {
        this.contractOfferText = contractOfferText;
    }

    public Long getRecruitmentProcessId() {
        return recruitmentProcessId;
    }

    public void setRecruitmentProcessId(Long recruitmentProcessId) {
        this.recruitmentProcessId = recruitmentProcessId;
    }
}
