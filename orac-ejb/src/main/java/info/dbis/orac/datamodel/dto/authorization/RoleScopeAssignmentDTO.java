package info.dbis.orac.datamodel.dto.authorization;

import info.dbis.orac.datamodel.authorization.RoleScopeAssignment;
import info.dbis.orac.datamodel.dto.GuardedEntityDTO;

/**
 * RoleScopeAssignmentDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class RoleScopeAssignmentDTO extends GuardedEntityDTO {

    private Long roleScopeId;

    private Long guardedEntityId;

    public RoleScopeAssignmentDTO() {
        super();
    }

    public RoleScopeAssignmentDTO(RoleScopeAssignment roleScopeAssignment){
        super(roleScopeAssignment);
        this.roleScopeId = roleScopeAssignment.getRoleScope().getId();
        this.guardedEntityId = roleScopeAssignment.getGuardedEntityId();
    }

    public Long getRoleScopeId() {
        return roleScopeId;
    }

    public void setRoleScopeId(Long roleScopeId) {
        this.roleScopeId = roleScopeId;
    }

    public Long getGuardedEntityId() {
        return guardedEntityId;
    }

    public void setGuardedEntityId(Long guardedEntityId) {
        this.guardedEntityId = guardedEntityId;
    }
}