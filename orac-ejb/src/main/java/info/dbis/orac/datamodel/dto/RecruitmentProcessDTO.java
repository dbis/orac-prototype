package info.dbis.orac.datamodel.dto;

import info.dbis.orac.datamodel.RecruitmentProcess;

/**
 * RecruitmentProcessDTO
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class RecruitmentProcessDTO extends GuardedEntityDTO {

    private Long humanResourceSystemId;

    private JobOfferDTO jobOfferDTO;

    public RecruitmentProcessDTO() {
        super();
    }

    public RecruitmentProcessDTO(RecruitmentProcess recruitmentProcess) {
        super(recruitmentProcess);
        this.setHumanResourceSystemId(recruitmentProcess.getHumanResourceSystem().getId());
        this.setJobOfferDTO(new JobOfferDTO(recruitmentProcess.getJobOffer()));
    }

    public Long getHumanResourceSystemId() {
        return humanResourceSystemId;
    }

    public void setHumanResourceSystemId(Long humanResourceSystemId) {
        this.humanResourceSystemId = humanResourceSystemId;
    }

    public JobOfferDTO getJobOfferDTO() {
        return jobOfferDTO;
    }

    public void setJobOfferDTO(JobOfferDTO jobOfferDTO) {
        this.jobOfferDTO = jobOfferDTO;
    }
}
