package info.dbis.orac.datamodel;

import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * JobOffer
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
@Entity
@NamedQueries({
        @NamedQuery(
                name = JobOffer.QUERY__ALL,
                query = "select jo from JobOffer jo"
        ),
        @NamedQuery(
                name = JobOffer.QUERY__BY__RECRUITMENT_PROCESS,
                query = "select jo from JobOffer jo join fetch jo.recruitmentProcess rp " +
                        "where rp.persistenceID = :recruitmentProcessId"
        ),
        @NamedQuery(
                name = JobOffer.QUERY__BY__HUMAN_RESOURCE_SYSTEM,
                query = "select jo from JobOffer jo join jo.recruitmentProcess rp join rp.humanResourceSystem hrs " +
                        "where hrs.persistenceID = :humanResourceSystemId"
        )
})
public class JobOffer extends GuardedEntity {

    public final static String QUERY__ALL = "info.dbis.orac.datamodel.JobOffer.QUERY__ALL";
    public final static String QUERY__BY__RECRUITMENT_PROCESS = "info.dbis.orac.datamodel.JobOffer.QUERY__BY__RECRUITMENT_PROCESS";
    public final static String QUERY__BY__HUMAN_RESOURCE_SYSTEM = "info.dbis.orac.datamodel.JobOffer.QUERY__BY__HUMAN_RESOURCE_SYSTEM";

    @Lob
    private String jobOfferText;

    @NotNull
    @OneToOne(mappedBy = "jobOffer", cascade = {CascadeType.PERSIST})
    private RecruitmentProcess recruitmentProcess;

    @OneToMany(mappedBy = "jobOffer", cascade = {CascadeType.PERSIST})
    private Set<Document> documents = new LinkedHashSet<>();

    public JobOffer() {
        super();
    }

    public JobOffer(Agent creator, String jobOfferText, RecruitmentProcess recruitmentProcess) {
        super(creator);
        this.jobOfferText = jobOfferText;
        this.setRecruitmentProcess(recruitmentProcess);
    }

    public String getJobOfferText() {
        return jobOfferText;
    }

    public void setJobOfferText(String jobOfferText) {
        this.jobOfferText = jobOfferText;
    }

    public RecruitmentProcess getRecruitmentProcess() {
        return this.recruitmentProcess;
    }

    public void setRecruitmentProcess(RecruitmentProcess recruitmentProcess) {
        this.recruitmentProcess = recruitmentProcess;
        if (recruitmentProcess != null){
            if (!this.equals(recruitmentProcess.getJobOffer())){
                recruitmentProcess.setJobOffer(this);
            }
        }
    }

    public Set<Document> getDocuments() {
        return Collections.unmodifiableSet(this.documents);
    }

    private void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }

    public void addDocument(Document document){
        if (this.documents.add(document)){
            if (!this.equals(document.getJobOffer())){
                document.setJobOffer(this);
            }
        }
    }

    public void removeDocument(Document document){
        if (this.documents.remove(document)){
            if (this.equals(document.getJobOffer())){
                document.setJobOffer(null);
            }
        }
    }

    @Override
    public Set<GuardedEntity> getContextGuardedEntities() {
        Set<GuardedEntity> contextGuardedEntities = new LinkedHashSet<>();
        contextGuardedEntities.add(this.recruitmentProcess);
        return contextGuardedEntities;
    }
}
