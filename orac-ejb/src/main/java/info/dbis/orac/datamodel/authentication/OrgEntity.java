package info.dbis.orac.datamodel.authentication;

import info.dbis.orac.datamodel.authorization.GuardedEntity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * OrgEntity is the very basic class for all organization entities. This class is derived from the
 * the base class GuardedEntity.
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class OrgEntity extends GuardedEntity {

    public OrgEntity() {
        super();
    }

    public OrgEntity(Agent creator) {
        super(creator);
    }

}