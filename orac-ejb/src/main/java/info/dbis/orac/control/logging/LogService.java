package info.dbis.orac.control.logging;

import info.dbis.orac.authorization.AuthorizationEntityGraphCache;
import info.dbis.orac.control.AuthenticationService;
import info.dbis.orac.control.AuthorizationService;
import info.dbis.orac.datamodel.authorization.GuardedEntity;
import info.dbis.orac.datamodel.logging.PersistenceLogEvent;
import info.dbis.orac.datamodel.logging.PersistenceLogEventType;
import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.AuthorizationException;
import info.dbis.orac.exception.ControllerException;
import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * LogServiceImpl
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Stateless
public class LogService {

    @NotNull
    private Logger logger;

    @NotNull
    private PersistenceService persistenceService;

    @NotNull
    private AuthenticationService authenticationService;

    @NotNull
    private AuthorizationService authorizationService;

    @NotNull
    private AuthorizationEntityGraphCache authorizationEntityGraphCache;

    @Inject
    private void setLogger(final Logger logger) {
        this.logger = logger;
    }

    @Inject
    private void setPersistenceService(final PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Inject
    private void setAuthenticationService(final AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @Inject
    public void setAuthorizationService(final AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @Inject
    private void setAuthorizationEntityGraphCache(final AuthorizationEntityGraphCache authorizationEntityGraphCache) {
        this.authorizationEntityGraphCache = authorizationEntityGraphCache;
    }

    @Asynchronous
    public void processPersistenceLogEvent(PersistenceLogEvent persistenceLogEvent) throws AuthenticationException, AuthorizationException, PersistenceException, ControllerException {
        try {
            if (persistenceLogEvent.getPersistenceLogEventType().equals(PersistenceLogEventType.REMOVED)) {
                GuardedEntity guardedEntity = null;
                try {
                    guardedEntity = this.persistenceService.get(persistenceLogEvent.getGuardedEntityClazz(), persistenceLogEvent.getGuardedEntityId());
                }
                catch (PersistenceException pe){
                    logger.info("Object with class="+persistenceLogEvent.getGuardedEntityClazz().getName()+" and id="+persistenceLogEvent.getGuardedEntityId()+" not found!");
                }
                if (guardedEntity != null){
                    this.authorizationEntityGraphCache.removeGuardedEntity(guardedEntity);
                    this.authorizationService.updateRoleScopeAssignmentsAfterEntityRemoval(persistenceLogEvent.getGuardedEntityId(), persistenceLogEvent.getGuardedEntityClazz());
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
