package info.dbis.orac.control;

import com.lambdaworks.crypto.SCryptUtil;
import info.dbis.orac.datamodel.*;
import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authentication.SystemAgent;
import info.dbis.orac.datamodel.authentication.User;
import info.dbis.orac.datamodel.authorization.*;
import info.dbis.orac.exception.ControllerException;
import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;
import javafx.application.Application;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.logging.Logger;

/**
 * Bootstrapper to set up the prototype
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
@Singleton
@Startup
public class BootstrappingService {

    @Inject
    private Logger logger;

    @Inject
    private PersistenceService persistenceService;

    private long systemId;

    @PostConstruct
    private void startup() {

        // Do startup checks
        logger.info("== STARTUP CHECKS ==");
        try {
            if (checkNeedForBootstrapping()){
                logger.info("== BOOTSTRAPPING REQUIRED ==");
                this.bootstrapApplication();
            }
            else{
                logger.info("== NO BOOTSTRAPPING REQUIRED ==");

                // but check for newly added privileges
                this.checkForNewPrivilegesWhileBootingSystem();
            }
        }
        catch (Exception e) {
            logger.info("BOOTSTRAPPING was interrupted by the following exception!");
            e.printStackTrace();
        }
    }

    private boolean checkNeedForBootstrapping() throws PersistenceException {
        // Check users
        List<HumanResourceSystem> humanResourceSystems = this.persistenceService.findWithNamedQuery(HumanResourceSystem.QUERY__ALL);
        if (humanResourceSystems.size() != 1)
            return true;

        HumanResourceSystem humanResourceSystem = humanResourceSystems.get(0);
        this.systemId = humanResourceSystem.getId();
        return !humanResourceSystem.isBootstrapped();
    }

    private void bootstrapApplication() throws PersistenceException, ControllerException {
        try{
            logger.info("SystemAgent to be created.");
            SystemAgent systemAgent = new SystemAgent();
            systemAgent.setCreatorId(systemAgent.getId());
            systemAgent = this.persistenceService.persist(systemAgent);
            logger.info("SystemAgent with persistenceID = "+systemAgent.getId()+" has been created successfully.");

            // Initialize HumanResourceSystem
            logger.info("HumanResourceSystem to be created.");
            HumanResourceSystem humanResourceSystem = new HumanResourceSystem(systemAgent);
            humanResourceSystem = this.persistenceService.persist(humanResourceSystem);
            this.systemId = humanResourceSystem.getId();
            logger.info("HumanResourceSystem with ID=" + humanResourceSystem.getId()+" has been created successfully.");

            // Initialize users
            logger.info("User John Smith to be created.");
            User johnSmith = new User(systemAgent, "John", "Smith", "john.smith@dbis.info", true, "", "Ulm", "+49 123 456789", "http://www.dbis.de", true, "", humanResourceSystem);
            // see http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
            String generatedSecuredPasswordHashJohnSmith = SCryptUtil.scrypt("password", 16, 16, 16);
            johnSmith.setPassword(generatedSecuredPasswordHashJohnSmith);
            johnSmith = this.persistenceService.persist(johnSmith);
            logger.info("User John Smith with ID=" + johnSmith.getId()+" has been created successfully.");

            logger.info("Privilege Management to be established.");

            // add and insert all the new privileges into the database
            this.checkForNewPrivilegesWhileBootingSystem();

            // get all privileges
            List<Privilege> existingPrivileges = this.persistenceService.findWithNamedQuery(Privilege.QUERY__ALL);
            Set<Privilege> privileges;
            try{
                privileges = getNewlyAddedPrivileges(systemAgent, existingPrivileges);
            }
            catch (ClassNotFoundException cnfe){
                throw new ControllerException("Privileges could not be loaded from source code.");
            }

            // Initialize ORAC demo entities

            // first get Privileges

            // Privileges of Role 'HR Manager' (onCreationDefault/scopeManager)
            Set<Privilege> managerKeyRoleScopePrivileges = new HashSet<>();
            Set<Privilege> managerKeyRoleScopeHierarchicalPrivileges = new HashSet<>();

            // Privileges of Role 'HR Employee'
            Set<Privilege> employeeKeyScopePrivileges = new HashSet<>();
            Set<Privilege> employeeKeyScopeHierarchicalPrivileges = new HashSet<>();

            // Privileges of Role 'HR System User' (onAssignmentDefault: default role)
            Set<Privilege> userKeyScopePrivileges = new HashSet<>();
            Set<Privilege> userKeyScopeHierarchicalPrivileges = new HashSet<>();

            // Privileges of the additional RoleScopes of the Role 'HR System User'
            Set<Privilege> userAddScopeUserPrivileges = new HashSet<>();

            // Privileges of Role 'Recruiter'
            Set<Privilege> recruiterKeyRoleScopePrivileges = new HashSet<>();
            Set<Privilege> recruiterKeyRoleScopeHierarchicalPrivileges = new HashSet<>();

            // Privileges of Role 'Applicant'
            Set<Privilege> applicantKeyRoleScopePrivileges = new HashSet<>();
            Set<Privilege> applicantKeyRoleScopeHierarchicalPrivileges = new HashSet<>();

            // Privileges of Role 'Reviewer'
            Set<Privilege> reviewerKeyRoleScopePrivileges = new HashSet<>();
            Set<Privilege> reviewerKeyRoleScopeHierarchicalPrivileges = new HashSet<>();

            // Privileges of the additional RoleScope of the Role 'Reviewer'
            Set<Privilege> reviewerAddRoleScopePrivileges = new HashSet<>();

            // Privileges of Role 'Candidate'
            Set<Privilege> candidateKeyRoleScopePrivileges = new HashSet<>();
            Set<Privilege> candidateKeyRoleScopeHierarchicalPrivileges = new HashSet<>();


            for (Privilege privilege : privileges){
                // do we consider the HumanResourceSystem class?
                if (privilege.getTargetEntityClazz().equals(HumanResourceSystem.class)){
                    // assign all privileges regarding the HumanResourceSystem object to the key scope of the Roles 'Manager', 'Employee' and 'User'
                    // actually, this only includes reading the system object
                    managerKeyRoleScopePrivileges.add(privilege);
                    employeeKeyScopePrivileges.add(privilege);
                    userKeyScopePrivileges.add(privilege);
                }
                else{
                    // assign ALL OTHER privileges, which are not targeting the HumanResourceSystem object, as hierarchical ones to the 'Manager' role
                    // this ensures that the manager role owns all privileges of the system (so, the role is comparable to an admin role)
                    managerKeyRoleScopeHierarchicalPrivileges.add(privilege);
                }

                // do we consider the User class?
                if (privilege.getTargetEntityClazz().equals(User.class)){
                    if (privilege.getContextEntityClazz().equals(HumanResourceSystem.class)){
                        // assign the listing and search privileges to the Role 'User' as hierarchical rights
                        if (privilege.getActionType().equals(Privilege.ActionType.LISTING) || privilege.getActionType().equals(Privilege.ActionType.SEARCH)){
                            employeeKeyScopeHierarchicalPrivileges.add(privilege);
                            userKeyScopeHierarchicalPrivileges.add(privilege);
                        }
                    }

                    // assign the privileges matching the following action types to the additional scope of the Role 'User'
                    // by this assignment, a user may update his personal details
                    if (privilege.getActionType().equals(Privilege.ActionType.READ) || privilege.getActionType().equals(Privilege.ActionType.UPDATE) || privilege.getActionType().equals(Privilege.ActionType.REMOVE)){
                        userAddScopeUserPrivileges.add(privilege);
                    }
                }

                // do we consider the RecruitmentProcess class?
                if (privilege.getTargetEntityClazz().equals(RecruitmentProcess.class)){
                    if (privilege.getActionType().equals(Privilege.ActionType.ADD)){
                        employeeKeyScopeHierarchicalPrivileges.add(privilege);
                    }
                    if (privilege.getActionType().equals(Privilege.ActionType.READ)){
                        recruiterKeyRoleScopePrivileges.add(privilege);
                        employeeKeyScopeHierarchicalPrivileges.add(privilege);
                        userKeyScopeHierarchicalPrivileges.add(privilege);
                    }
                    if (privilege.getActionType().equals(Privilege.ActionType.UPDATE) || privilege.getActionType().equals(Privilege.ActionType.REMOVE)){
                        // assign all privileges regarding a RecruitmentProcess to the roles 'Recruiter'
                        recruiterKeyRoleScopePrivileges.add(privilege);
                    }

                    if (privilege.getActionType().equals(Privilege.ActionType.LISTING)) {
                        employeeKeyScopeHierarchicalPrivileges.add(privilege);
                        userKeyScopeHierarchicalPrivileges.add(privilege);
                    }
                }

                // do we consider the JobApplication class?
                if (privilege.getTargetEntityClazz().equals(JobOffer.class)){
                    // assign all privileges regarding RecruitmentProcesses to the roles 'Recruiter' hierarchically
                    recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);

                    // ensure that all users may read and list job offers
                    if (privilege.getActionType().equals(Privilege.ActionType.READ) || privilege.getActionType().equals(Privilege.ActionType.LISTING)) {
                        employeeKeyScopeHierarchicalPrivileges.add(privilege);
                        userKeyScopeHierarchicalPrivileges.add(privilege);
                    }
                }

                // do we consider the JobApplication class?
                if (privilege.getTargetEntityClazz().equals(JobApplication.class)){
                    if (privilege.getActionType().equals(Privilege.ActionType.ADD)) {
                        // all system users may create an application
                        employeeKeyScopeHierarchicalPrivileges.add(privilege);
                        userKeyScopeHierarchicalPrivileges.add(privilege);
                    }
                    if (privilege.getActionType().equals(Privilege.ActionType.READ)) {
                        // only some users are allowed to read applications
                        recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        applicantKeyRoleScopePrivileges.add(privilege);
                        reviewerKeyRoleScopePrivileges.add(privilege);
                    }
                    if (privilege.getActionType().equals(Privilege.ActionType.LISTING)) {
                        recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                    }
                    if (privilege.getActionType().equals(Privilege.ActionType.UPDATE) || privilege.getActionType().equals(Privilege.ActionType.REMOVE)) {
                        recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        applicantKeyRoleScopePrivileges.add(privilege);
                    }
                    if (privilege.getActionType().equals(Privilege.ActionType.ADVANCED)) {
                        recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                    }
                }

                // do we consider the JobApplicationReview class?
                if (privilege.getTargetEntityClazz().equals(JobApplicationReview.class)){
                    recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);

                    if (privilege.getActionType().equals(Privilege.ActionType.ADD)) {
                        reviewerKeyRoleScopeHierarchicalPrivileges.add(privilege);
                    }
                    else {
                        recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                    }
                }

                // do we consider the ContractOffer class?
                if (privilege.getTargetEntityClazz().equals(ContractOffer.class)){
                    // assign all privileges regarding JobOffer as hierarchical ones to the Role 'Recruiter'
                    recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);

                    if (privilege.getActionType().equals(Privilege.ActionType.READ)) {
                        candidateKeyRoleScopePrivileges.add(privilege);
                    }
                }

                // do we consider the Document class?
                if (privilege.getTargetEntityClazz().equals(ContractOffer.class)){
                    if (privilege.getContextEntityClazz().equals(JobOffer.class)){
                        if (privilege.getActionType().equals(Privilege.ActionType.ADD)) {
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.READ) || privilege.getActionType().equals(Privilege.ActionType.LISTING)) {
                            employeeKeyScopeHierarchicalPrivileges.add(privilege);
                            userKeyScopeHierarchicalPrivileges.add(privilege);
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.UPDATE) || privilege.getActionType().equals(Privilege.ActionType.REMOVE)){
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                    }
                    if (privilege.getContextEntityClazz().equals(JobApplication.class)){
                        if (privilege.getActionType().equals(Privilege.ActionType.ADD)) {
                            applicantKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.READ) || privilege.getActionType().equals(Privilege.ActionType.LISTING)) {
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                            applicantKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.UPDATE)){
                            applicantKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.REMOVE)){
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                            applicantKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                    }
                    if (privilege.getContextEntityClazz().equals(JobApplicationReview.class)){
                        if (privilege.getActionType().equals(Privilege.ActionType.ADD)) {
                            reviewerKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.READ) || privilege.getActionType().equals(Privilege.ActionType.LISTING)) {
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                            reviewerKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.UPDATE)){
                            reviewerKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.REMOVE)){
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                            reviewerKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                    }
                    if (privilege.getContextEntityClazz().equals(ContractOffer.class)){
                        if (privilege.getActionType().equals(Privilege.ActionType.ADD)) {
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.READ) || privilege.getActionType().equals(Privilege.ActionType.LISTING)) {
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                            candidateKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.UPDATE)){
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                        if (privilege.getActionType().equals(Privilege.ActionType.REMOVE)){
                            recruiterKeyRoleScopeHierarchicalPrivileges.add(privilege);
                        }
                    }
                }

                humanResourceSystem.addPrivilege(privilege);
                this.persistenceService.persist(privilege);
            }

            logger.info("Privilege Management has been established.");

            // Initialize Roles

            logger.info("Role Management to be established.");

            // Initialize HumanResourceSystem Administrator Role

            List<Role> roles = this.persistenceService.findWithNamedQuery(Role.QUERY__ALL);
            if (roles.size() != 0) {
                throw new ControllerException("Existing Roles found.");
            }

            logger.info("Role 'HR Manager' to be created.");

            RoleScope keyRoleScopeForManager = new RoleScope(systemAgent, HumanResourceSystem.class, true, true, false);
            RoleScopePrivilegeAssociation rspaForManagerKeyRoleScope = new RoleScopePrivilegeAssociation(systemAgent, keyRoleScopeForManager);
            rspaForManagerKeyRoleScope.addEntityPrivileges(managerKeyRoleScopePrivileges);
            rspaForManagerKeyRoleScope.addHierarchicalPrivileges(managerKeyRoleScopeHierarchicalPrivileges);

            Role managerRole = new Role(systemAgent, "HR Manager", keyRoleScopeForManager);
            humanResourceSystem.addRole(managerRole);
            this.persistenceService.persist(managerRole);

            logger.info("Role 'HR Manager' with id="+managerRole.getId()+" has been created.");

            logger.info("Role 'HR Employee' to be created.");

            RoleScope keyRoleScopeForEmployee = new RoleScope(systemAgent, HumanResourceSystem.class, false, false, false);
            RoleScopePrivilegeAssociation rspaForEmployeeKeyRoleScope = new RoleScopePrivilegeAssociation(systemAgent, keyRoleScopeForEmployee);
            rspaForEmployeeKeyRoleScope.addEntityPrivileges(employeeKeyScopePrivileges);
            rspaForEmployeeKeyRoleScope.addHierarchicalPrivileges(employeeKeyScopeHierarchicalPrivileges);

            Role employeeRole = new Role(systemAgent, "HR Employee", keyRoleScopeForEmployee);
            humanResourceSystem.addRole(employeeRole);
            this.persistenceService.persist(employeeRole);

            logger.info("Role 'HR Employee' with id="+managerRole.getId()+" has been created.");

            logger.info("Role 'HR System User' to be created.");

            RoleScope keyRoleScopeForUser = new RoleScope(systemAgent, HumanResourceSystem.class, false, false, true);
            RoleScopePrivilegeAssociation rspaForUserKeyRoleScope = new RoleScopePrivilegeAssociation(systemAgent, keyRoleScopeForUser);
            rspaForUserKeyRoleScope.addEntityPrivileges(userKeyScopePrivileges);
            rspaForUserKeyRoleScope.addHierarchicalPrivileges(userKeyScopeHierarchicalPrivileges);

            RoleScope addRoleScopeForUser = new RoleScope(systemAgent, User.class, false, false, true);
            RoleScopePrivilegeAssociation rspaForUserAddRoleScope = new RoleScopePrivilegeAssociation(systemAgent, addRoleScopeForUser);
            rspaForUserAddRoleScope.addEntityPrivileges(userAddScopeUserPrivileges);

            Role userRole = new Role(systemAgent, "HR System User", keyRoleScopeForUser);
            userRole.addAdditionalScope(addRoleScopeForUser);
            humanResourceSystem.addRole(userRole);
            this.persistenceService.persist(userRole);

            logger.info("Role 'HR System User' with id="+userRole.getId()+" has been created.");

            logger.info("Role 'Recruiter' to be created.");

            RoleScope keyRoleScopeForRecruiter = new RoleScope(systemAgent, RecruitmentProcess.class, true, true, true);
            List<Class<? extends GuardedEntity>> contextEntityClassesForRecruiter = new LinkedList<>();
            contextEntityClassesForRecruiter.add(HumanResourceSystem.class);
            keyRoleScopeForRecruiter.setContextEntitiesClazzes(contextEntityClassesForRecruiter);
            RoleScopePrivilegeAssociation rspaForRecruiterKeyRoleScope = new RoleScopePrivilegeAssociation(systemAgent, keyRoleScopeForRecruiter);
            rspaForRecruiterKeyRoleScope.addEntityPrivileges(recruiterKeyRoleScopePrivileges);
            rspaForRecruiterKeyRoleScope.addHierarchicalPrivileges(recruiterKeyRoleScopeHierarchicalPrivileges);

            Role recruiterRole = new Role(systemAgent, "Recruiter", keyRoleScopeForRecruiter);
            humanResourceSystem.addRole(recruiterRole);
            this.persistenceService.persist(recruiterRole);

            logger.info("Role 'Recruiter' with id="+recruiterRole.getId()+" has been created.");

            logger.info("Role 'Applicant' to be created.");

            RoleScope keyRoleScopeForApplicant = new RoleScope(systemAgent, JobApplication.class, false, true, false);
            List<Class<? extends GuardedEntity>> contextEntityClassesForApplicant  = new LinkedList<>();
            contextEntityClassesForApplicant.add(RecruitmentProcess.class);
            keyRoleScopeForApplicant.setContextEntitiesClazzes(contextEntityClassesForApplicant);
            RoleScopePrivilegeAssociation rspaForApplicantKeyRoleScope = new RoleScopePrivilegeAssociation(systemAgent, keyRoleScopeForApplicant);
            rspaForApplicantKeyRoleScope.addEntityPrivileges(applicantKeyRoleScopePrivileges);
            rspaForApplicantKeyRoleScope.addHierarchicalPrivileges(applicantKeyRoleScopeHierarchicalPrivileges);

            Role applicantRole = new Role(systemAgent, "Applicant", keyRoleScopeForApplicant);
            humanResourceSystem.addRole(applicantRole);
            this.persistenceService.persist(applicantRole);

            logger.info("Role 'Applicant' with id="+applicantRole.getId()+" has been created.");

            logger.info("Role 'Reviewer' to be created.");

            RoleScope keyRoleScopeForReviewer = new RoleScope(systemAgent, JobApplication.class, false, false, true);
            RoleScopePrivilegeAssociation rspaForReviewerKeyRoleScope = new RoleScopePrivilegeAssociation(systemAgent, keyRoleScopeForReviewer);
            rspaForReviewerKeyRoleScope.addEntityPrivileges(reviewerKeyRoleScopePrivileges);
            rspaForReviewerKeyRoleScope.addHierarchicalPrivileges(reviewerKeyRoleScopeHierarchicalPrivileges);

            RoleScope addRoleScopeForReviewer = new RoleScope(systemAgent, JobApplicationReview.class, false, true, false);
            RoleScopePrivilegeAssociation rspaForReviewerAddRoleScope = new RoleScopePrivilegeAssociation(systemAgent, addRoleScopeForReviewer);
            rspaForReviewerAddRoleScope.addEntityPrivileges(reviewerAddRoleScopePrivileges);

            Role reviewerRole = new Role(systemAgent, "Reviewer", keyRoleScopeForReviewer);
            recruiterRole.addAdditionalScope(addRoleScopeForReviewer);
            humanResourceSystem.addRole(reviewerRole);
            this.persistenceService.persist(reviewerRole);

            logger.info("Role 'Reviewer' with id="+recruiterRole.getId()+" has been created.");

            logger.info("Role 'Candidate' to be created.");

            RoleScope keyRoleScopeForCandidate = new RoleScope(systemAgent, ContractOffer.class, false, false, true);
            RoleScopePrivilegeAssociation rspaForCandidateKeyRoleScope = new RoleScopePrivilegeAssociation(systemAgent, keyRoleScopeForCandidate);
            rspaForCandidateKeyRoleScope.addEntityPrivileges(candidateKeyRoleScopePrivileges);
            rspaForCandidateKeyRoleScope.addHierarchicalPrivileges(candidateKeyRoleScopeHierarchicalPrivileges);

            Role candidateRole = new Role(systemAgent, "Candidate", keyRoleScopeForCandidate);
            humanResourceSystem.addRole(candidateRole);
            this.persistenceService.persist(candidateRole);

            logger.info("Role 'Candidate' with id="+candidateRole.getId()+" has been created.");

            // Create first RoleAssignment for HumanResourceSystem Administrator

            logger.info("RoleAssignment for 'HR Manager' and user '"+johnSmith.getFirstName()+" "+johnSmith.getLastName()+"' to be created.");

            RoleAssignment systemAdminRoleAssignment = new RoleAssignment(systemAgent, humanResourceSystem, managerRole, johnSmith);
            RoleScopeAssignment rsaSystemAdminRoleSystem = new RoleScopeAssignment(systemAgent, systemAdminRoleAssignment, keyRoleScopeForManager, humanResourceSystem);

            this.persistenceService.persist(systemAdminRoleAssignment);
            this.persistenceService.persist(rsaSystemAdminRoleSystem);

            logger.info("RoleAssignment for Role 'HR Manager' and user '"+johnSmith.getFirstName()+" "+johnSmith.getLastName()+"' with id="+ systemAdminRoleAssignment.getId()+" has been created.");

            humanResourceSystem.setBootstrappedTrue();
        }
        catch (PersistenceException pe){
            pe.printStackTrace();
        }
    }

    public long getSystemId() {
        return systemId;
    }

    public void checkForNewPrivilegesWhileBootingSystem() {

        // Establish Privilege Management
        logger.info("Access Control: bootstrapping check for newly added privileges is starting");

        try {
            List<HumanResourceSystem> humanResourceSystems = this.persistenceService.findWithNamedQuery(HumanResourceSystem.QUERY__ALL);
            if (humanResourceSystems.size() == 1){
                HumanResourceSystem humanResourceSystem = humanResourceSystems.get(0);
                // if HumanResourceSystem is bootstrapped, then check on new permissions
                if (humanResourceSystem.isBootstrapped()){
                    List<SystemAgent> systemAgents = this.persistenceService.findWithNamedQuery(SystemAgent.QUERY__ALL);
                    if (systemAgents.size() == 1){
                        SystemAgent systemAgent = systemAgents.get(0);

                        List<Privilege> existingPrivileges = this.persistenceService.findWithNamedQuery(Privilege.QUERY__ALL);

                        Set<Privilege> addedPrivileges = this.getNewlyAddedPrivileges(systemAgent, existingPrivileges);
                        if (addedPrivileges.size() > 0)
                            logger.info("Access Control: "+addedPrivileges.size()+" newly added privileges found");
                        else
                            logger.info("Access Control: no newly added privileges found");
                        for (Privilege privilege : addedPrivileges){
                            humanResourceSystem.addPrivilege(privilege);
                            this.persistenceService.persist(privilege);
                        }
                        if (addedPrivileges.size() > 0)
                            logger.info("Access Control: "+addedPrivileges.size()+" newly added privileges persisted");
                    }
                }
            }

        } catch (PersistenceException pe) {
            logger.info("PersistenceException occured while establishing Privilege Management");
            pe.printStackTrace();
        }
        catch (ClassNotFoundException cnfe){
            logger.info("ClassNotFoundException occured while establishing Privilege Management");
            cnfe.printStackTrace();
        }

        logger.info("Access Control: Bootstrapping Check for newly added Privileges has been finished");
    }

    public Set<Privilege> getNewlyAddedPrivileges(SystemAgent systemAgent, List<Privilege> persistedPrivileges) throws ClassNotFoundException, PersistenceException {

        // using permission codes to compare privileges against each other
        // a permission code consists of the unique concatenation of targetEntityClazz + ":" + contextEntityClazz + ":" + actionType + ":" + action
        // the following map is used to store the existing privileges
        Map<String, Privilege> persistedPermissionsMap = new HashMap<>();
        persistedPrivileges.forEach(privilege -> {
            if (!privilege.getPermissionCode().equals("")){
                if (!persistedPermissionsMap.containsKey(privilege.getPermissionCode())){
                    persistedPermissionsMap.put(privilege.getPermissionCode(), privilege);
                }
            }
        });


        // Now get all permissions present in the source code
        Map<String, Privilege> sourceCodePrivilegesMap = new HashMap<>();

        // these two store abstract and concrete classes found in the targetEntityClazz or contextEntityClazz values
        Set<RequiresAccessControlEntity> accessControlRequiredAnnotationsWithAbstractScope = new HashSet<>();
        Set<Class<? extends GuardedEntity>> ordinaryClazzes = new HashSet<>();

        // set the packages to be considered
        Reflections reflections = new Reflections("info.dbis.orac.control", new MethodAnnotationsScanner());

        // go through all methods with RequireAccessControl annotations found
        for (Method method : reflections.getMethodsAnnotatedWith(RequiresAccessControl.class)) {

            // get the annotation of the current method
            RequiresAccessControl rac = method.getAnnotation(RequiresAccessControl.class);

            // do we consider an annotated method with no abstract class deposited in the targetEntityClazz or contextEntityClazz values
            if (!Modifier.isAbstract(rac.targetObjectClazz().getModifiers()) && !Modifier.isAbstract(rac.contextObjectClazz().getModifiers())){
                // calculate the current permission code
                String permissionCode = rac.targetObjectClazz().getName() + ":" + rac.contextObjectClazz().getName() + ":" + rac.actionType() + ":" + method.getName();

                // add the Privilege
                sourceCodePrivilegesMap.put(permissionCode, getPrivilegeFromRequiresAccessControlAnnotation(systemAgent, rac.targetObjectClazz(), rac.contextObjectClazz(), rac.actionType(), method.getName()));

                // adding the values of targetEntityClazz and/or contextEntityClazz to the set of ordinary classes
                if (!ordinaryClazzes.contains(rac.targetObjectClazz())){
                    ordinaryClazzes.add(rac.targetObjectClazz());
                }
                if (!ordinaryClazzes.contains(rac.contextObjectClazz())){
                    ordinaryClazzes.add(rac.contextObjectClazz());
                }
            }
            else{
                // add the current annotation to the set of abstractly defined permissions to be refined below
                accessControlRequiredAnnotationsWithAbstractScope.add(new RequiresAccessControlEntity(rac, method));

                // adding the values of targetEntityClazz and/or contextEntityClazz to the set of ordinary classes
                if (!Modifier.isAbstract(rac.targetObjectClazz().getModifiers())){
                    if (!ordinaryClazzes.contains(rac.targetObjectClazz())){
                        ordinaryClazzes.add(rac.targetObjectClazz());
                    }
                }
                if (!Modifier.isAbstract(rac.contextObjectClazz().getModifiers())){
                    if (!ordinaryClazzes.contains(rac.contextObjectClazz())){
                        ordinaryClazzes.add(rac.contextObjectClazz());
                    }
                }
            }
        }

        // we now have to go through all abstractly defined permissions in order to refine them properly
        for (RequiresAccessControlEntity requiresAccessControlEntity : accessControlRequiredAnnotationsWithAbstractScope){
            // check against all ordinary classes
            for (Class possibleScope : ordinaryClazzes) {
                // are both the targetEntityClazz and the contextEntityClazz defined with an abstract class?
                if (Modifier.isAbstract(requiresAccessControlEntity.targetEntityClazz.getModifiers()) && Modifier.isAbstract(requiresAccessControlEntity.contextEntityClazz.getModifiers())) {
                    // is the current possible scope class a concrete class of the abstract class deposited in the targetEntityClazz value?
                    if (requiresAccessControlEntity.targetEntityClazz.isAssignableFrom(possibleScope)) {
                        // does the targetEntityClazz equal the contextEntityClazz?
                        if (requiresAccessControlEntity.targetEntityClazz.equals(requiresAccessControlEntity.contextEntityClazz)) {
                            // create privilege
                            Privilege privilege = getPrivilegeFromRequiresAccessControlAnnotation(systemAgent, possibleScope, possibleScope, requiresAccessControlEntity.actionType, requiresAccessControlEntity.action);
                            String permissionCode = possibleScope.getName() + ":" + possibleScope.getName() + ":" + requiresAccessControlEntity.actionType + ":" + requiresAccessControlEntity.action;
                            // add it to the found privileges
                            sourceCodePrivilegesMap.put(permissionCode, privilege);
                        } else {
                            // go through all possible classes again to find the ones refining the abstract contextEntityClazz
                            for (Class possibleContextScope : ordinaryClazzes) {
                                if (requiresAccessControlEntity.contextEntityClazz.isAssignableFrom(possibleScope)) {
                                    // create privilege
                                    Privilege privilege = getPrivilegeFromRequiresAccessControlAnnotation(systemAgent, possibleScope, possibleContextScope, requiresAccessControlEntity.actionType, requiresAccessControlEntity.action);
                                    String permissionCode = possibleScope.getName() + ":" + possibleContextScope.getName() + ":" + requiresAccessControlEntity.actionType + ":" + requiresAccessControlEntity.action;
                                    // add it to the found privileges
                                    sourceCodePrivilegesMap.put(permissionCode, privilege);
                                }
                            }
                        }
                    }
                }
                else { // either the targetEntityClazz or the contextEntityClazz is abstractly defined
                    // is the targetEntityClazz abstractly defined?
                    if (Modifier.isAbstract(requiresAccessControlEntity.targetEntityClazz.getModifiers()) && !Modifier.isAbstract(requiresAccessControlEntity.contextEntityClazz.getModifiers())){
                        if (requiresAccessControlEntity.targetEntityClazz.isAssignableFrom(possibleScope)){
                            // create privilege
                            Privilege privilege = getPrivilegeFromRequiresAccessControlAnnotation(systemAgent, possibleScope, requiresAccessControlEntity.contextEntityClazz, requiresAccessControlEntity.actionType, requiresAccessControlEntity.action);
                            String permissionCode = possibleScope.getName() + ":" + requiresAccessControlEntity.contextEntityClazz.getName() + ":" + requiresAccessControlEntity.actionType + ":" + requiresAccessControlEntity.action;
                            // add it to the found privileges
                            sourceCodePrivilegesMap.put(permissionCode, privilege);
                        }
                    }
                    // is the contextEntityClazz abstractly defined?
                    if (!Modifier.isAbstract(requiresAccessControlEntity.targetEntityClazz.getModifiers()) && Modifier.isAbstract(requiresAccessControlEntity.contextEntityClazz.getModifiers())) {
                        if (requiresAccessControlEntity.contextEntityClazz.isAssignableFrom(possibleScope)) {
                            // create privilege
                            Privilege privilege = getPrivilegeFromRequiresAccessControlAnnotation(systemAgent, requiresAccessControlEntity.targetEntityClazz, possibleScope, requiresAccessControlEntity.actionType, requiresAccessControlEntity.action);
                            String permissionCode = requiresAccessControlEntity.targetEntityClazz.getName() + ":" + possibleScope.getName() + ":" + requiresAccessControlEntity.actionType + ":" + requiresAccessControlEntity.action;
                            // add it to the found privileges
                            sourceCodePrivilegesMap.put(permissionCode, privilege);
                        }
                    }
                }
            }
        }

        // Check differences (Sets don't have duplicates)
        Set<String> missingPermissionsCodes = new HashSet<>(sourceCodePrivilegesMap.keySet());
        missingPermissionsCodes.removeAll(persistedPermissionsMap.keySet());

        Set<Privilege> addedPrivileges = new HashSet<>();

        if (missingPermissionsCodes.size() > 0){
            for (String missingPermissionCode : missingPermissionsCodes){
                Privilege privilege = sourceCodePrivilegesMap.get(missingPermissionCode);
                addedPrivileges.add(privilege);
            }
        }
        return addedPrivileges;
    }

    private class RequiresAccessControlEntity{

        RequiresAccessControlEntity(RequiresAccessControl rac, Method method){
            targetEntityClazz = rac.targetObjectClazz();
            contextEntityClazz = rac.contextObjectClazz();
            actionType = rac.actionType();
            action = method.getName();
        }

        final Class<? extends GuardedEntity> targetEntityClazz;
        final Class<? extends GuardedEntity> contextEntityClazz;
        final RequiresAccessControl.ActionType actionType;
        final String action;
    }

    private Privilege getPrivilegeFromRequiresAccessControlAnnotation(Agent systemAgent, Class targetScope, Class contextScope, RequiresAccessControl.ActionType actionType, String action){

        String name;
        String targetScopeShortName = targetScope.getName().substring((targetScope.getName().lastIndexOf(".")+1), targetScope.getName().length());
        String contextScopeShortName = contextScope.getName().substring((contextScope.getName().lastIndexOf(".")+1), contextScope.getName().length());
        String attributeName;
        switch (actionType){
            case ADD:
                name = "Add " + targetScopeShortName + " to " + contextScopeShortName + " (" + action + ")";
                break;
            case ADD_LINK:
                name = "Add link from " + targetScopeShortName + " to " + contextScopeShortName + " (" + action + ")";
                break;
            case READ:
                name = "Read " + targetScopeShortName + " completely" + " (" + action + ")";
                break;
            case READ_ATTRIBUTE:
                attributeName = action.replace("get", "");
                name = "Read attribute " + attributeName + " of " + targetScopeShortName + " (" + action + ")";
                break;
            case UPDATE:
                name = "Update " + targetScopeShortName + " completely" + " (" + action + ")";
                break;
            case UPDATE_ATTRIBUTE:
                attributeName = action.replace("set", "");
                name = "Update attribute " + attributeName + " of " + targetScopeShortName + " (" + action + ")";
                break;
            case REMOVE:
                name = "Remove " + targetScopeShortName + " from " + contextScopeShortName;
                break;
            case REMOVE_LINK:
                name = "Remove link from " + targetScopeShortName + " to " + contextScopeShortName + " (" + action + ")";
                break;
            case LISTING:
                name = "List " + targetScopeShortName + "(s) of " + contextScopeShortName;
                break;
            case SEARCH:
                name = "Search " + targetScopeShortName + "(s) of " + contextScopeShortName;
                break;
            case ADVANCED:
                name = "Perform " + action + " of/on " + targetScopeShortName;
                break;
            default:
                name = targetScopeShortName + ":" + contextScopeShortName + ":" + actionType.name() + ":" + action;
                break;
        }

        return new Privilege(systemAgent, name, targetScope, contextScope, Privilege.ActionType.valueOf(actionType.name()), action);
    }
}
