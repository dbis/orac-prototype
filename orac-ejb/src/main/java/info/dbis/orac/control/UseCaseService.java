package info.dbis.orac.control;

import info.dbis.orac.common.QueryParameter;
import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authentication.User;
import info.dbis.orac.datamodel.authorization.RoleAssignment;
import info.dbis.orac.datamodel.dto.*;
import info.dbis.orac.datamodel.dto.authentication.UserDTO;
import info.dbis.orac.datamodel.dto.authorization.RoleAssignmentDTO;
import info.dbis.orac.datamodel.dto.authorization.RoleDTO;
import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.AuthorizationException;
import info.dbis.orac.exception.ControllerException;
import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;

import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * UseCaseService
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class UseCaseService {

    @Inject
    private AuthorizationService authorizationService;

    @Inject
    private SystemService systemService;

    @Inject
    private ProcessService processService;

    @Inject
    private PersistenceService persistenceService;

    @Inject
    private Logger logger;

    public UseCaseDTO createUseCase(Long agentId, Long roleAssignmentId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {

        // get agent (in this case, it is assumed that is a user)
        User agent = this.persistenceService.get(User.class, agentId);

        // get current RoleAssignment
        List<RoleAssignment> roleAssignments = this.persistenceService.findWithNamedQuery(RoleAssignment.QUERY__ID, QueryParameter.with("persistenceId", roleAssignmentId).parameters());
        RoleAssignment currentRoleAssignment = roleAssignments.get(0);

        logger.info("=== Establishing Use Case ===");

        // create new Users
        logger.info("Add new users");

        UserDTO maxMueller = new UserDTO(agent, "Max", "Mueller", "max.mueller@dbis.info", true, "", "Ulm", "+49 123 456789", "http://www.dbis.de", true, "");
        maxMueller.setPassword("password");
        maxMueller = this.systemService.addUser(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), maxMueller);
        logger.info("User Max Mueller with ID=" + maxMueller.getId()+" has been created successfully.");

        UserDTO kateListander = new UserDTO(agent, "Kate", "Listander", "kate.listander@dbis.info", false, "", "Ulm", "+49 123 456789", "http://www.dbis.de", true, "");
        kateListander.setPassword("password");
        kateListander = this.systemService.addUser(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), kateListander);
        logger.info("User Kate Listander with ID=" + kateListander.getId()+" has been created successfully.");

        UserDTO sarahDost = new UserDTO(agent, "Sarah", "Dost", "kate.dost@dbis.info", false, "", "Ulm", "+49 123 456789", "http://www.dbis.de", true, "");
        sarahDost.setPassword("password");
        sarahDost = this.systemService.addUser(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), sarahDost);
        logger.info("User Sarah Dost with ID=" + sarahDost.getId()+" has been created successfully.");

        UserDTO kimHirst = new UserDTO(agent, "Kim", "Hirst", "kim.hirst@dbis.info", false, "", "Ulm", "+49 123 456789", "http://www.dbis.de", true, "");
        kimHirst.setPassword("password");
        kimHirst = this.systemService.addUser(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), kimHirst);
        logger.info("User Kim Hirst with ID=" + kimHirst.getId()+" has been created successfully.");

        // make Max Mueller and Kate Listander HR employees
        logger.info("Make Max Mueller and Kate Listander HR employees");

        Set<RoleDTO> roles = this.authorizationService.getRoles(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId());
        RoleDTO employeeRoleDTO = null;
        for (RoleDTO roleDTO : roles){
            if (roleDTO.getName().equals("HR Employee")){
                employeeRoleDTO = roleDTO;
                break;
            }
        }

        if (employeeRoleDTO == null){
            throw new ControllerException("Employee role could not be found!");
        }

        RoleAssignmentDTO maxMuellerEmployeeRoleAssignmentDTO = new RoleAssignmentDTO();
        maxMuellerEmployeeRoleAssignmentDTO.setRoleId(employeeRoleDTO.getId());
        Set<Long> maxMuellerEmployeeRoleAssignmentAgentIds = new LinkedHashSet<>();
        maxMuellerEmployeeRoleAssignmentAgentIds.add(maxMueller.getId());
        maxMuellerEmployeeRoleAssignmentDTO.setAgentIds(maxMuellerEmployeeRoleAssignmentAgentIds);
        maxMuellerEmployeeRoleAssignmentDTO = this.authorizationService.addRoleAssignment(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), maxMuellerEmployeeRoleAssignmentDTO, this.systemService.getHumanResourceSystemId());

        RoleAssignmentDTO kateListanderEmployeeRoleAssignmentDTO = new RoleAssignmentDTO();
        kateListanderEmployeeRoleAssignmentDTO.setRoleId(employeeRoleDTO.getId());
        Set<Long> kateListanderEmployeeRoleAssignmentAgentIds = new LinkedHashSet<>();
        kateListanderEmployeeRoleAssignmentAgentIds.add(kateListander.getId());
        kateListanderEmployeeRoleAssignmentDTO.setAgentIds(kateListanderEmployeeRoleAssignmentAgentIds);
        kateListanderEmployeeRoleAssignmentDTO = this.authorizationService.addRoleAssignment(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), kateListanderEmployeeRoleAssignmentDTO, this.systemService.getHumanResourceSystemId());
        logger.info("Max Mueller and Kate Listander are now HR employees");

        // Let Max Mueller initiate a recruitment process and be a Recruiter (onCreationDefault)
        logger.info("Max Mueller shall now initiate a Recruitment Process");
        JobOfferDTO jobOfferDTO = new JobOfferDTO();
        jobOfferDTO.setJobOfferText("PhD Position for Process Management available");
        RecruitmentProcessDTO recruitmentProcessDTO = new RecruitmentProcessDTO();
        recruitmentProcessDTO.setJobOfferDTO(jobOfferDTO);
        recruitmentProcessDTO = this.processService.addRecruitmentProcess(maxMueller.getId(), maxMuellerEmployeeRoleAssignmentDTO.getId(), this.systemService.getHumanResourceSystemId(), recruitmentProcessDTO);
        logger.info("Max Mueller has initiated a Recruitment Process with the ID="+recruitmentProcessDTO.getId());

        // get newly created RoleAssignment
        Set<RoleAssignmentDTO> maxMuellerRecruitmentProcessRoleAssignmentDTOs = this.authorizationService.getRoleAssignmentsOfAgentAndGuardedEntity(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), maxMueller.getId(), recruitmentProcessDTO.getId());
        RoleAssignmentDTO maxMuellerRecruitmentProcessRoleAssignmentDTO;
        if (maxMuellerRecruitmentProcessRoleAssignmentDTOs.size() != 1){
            throw new ControllerException("There is an error in the automatic creation of RoleAssignments");
        }
        maxMuellerRecruitmentProcessRoleAssignmentDTO = maxMuellerRecruitmentProcessRoleAssignmentDTOs.iterator().next();
        logger.info("Max Mueller has automatically got the object-specific Role of a Recruiter with the ID="+maxMuellerRecruitmentProcessRoleAssignmentDTO.getRoleId()+" in relation to the Recruitment Process with the ID="+recruitmentProcessDTO.getId());

        // let Sarah Dost be an applicant
        logger.info("Sarah Dost shall now submit an application!");
        RoleAssignmentDTO sarahDostDefaultRoleAssignmentDTO = this.authorizationService.getDefaultRoleAssignmentOfAgent(sarahDost.getId());
        JobApplicationDTO sarahDostJobApplicationDTO = new JobApplicationDTO();
        sarahDostJobApplicationDTO.setJobApplicationText("I want to have this position!");
        sarahDostJobApplicationDTO = this.processService.addJobApplication(sarahDost.getId(), sarahDostDefaultRoleAssignmentDTO.getId(), recruitmentProcessDTO.getId(), sarahDostJobApplicationDTO);
        logger.info("Sarah Dost has submitted an application and is now an applicant!");

        // get newly created RoleAssignment
        Set<RoleAssignmentDTO> sarahDostJobApplicationRoleAssignmentDTOs = this.authorizationService.getRoleAssignmentsOfAgentAndGuardedEntity(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), sarahDost.getId(), sarahDostJobApplicationDTO.getId());
        RoleAssignmentDTO sarahDostJobApplicationRoleAssignmentDTO;
        if (sarahDostJobApplicationRoleAssignmentDTOs.size() != 1){
            throw new ControllerException("There is an error in the the creation of the RoleAssignment for the Role Applicant");
        }
        sarahDostJobApplicationRoleAssignmentDTO = sarahDostJobApplicationRoleAssignmentDTOs.iterator().next();
        logger.info("Sarah Dost may update her application via the Role Applicant and the corresponding roleAssignment with the ID="+sarahDostJobApplicationRoleAssignmentDTO.getId());

        // let Kim Hirst be an applicant as well
        logger.info("Kim Hirst shall now submit an application!");
        RoleAssignmentDTO kimHirstDefaultRoleAssignmentDTO = this.authorizationService.getDefaultRoleAssignmentOfAgent(kimHirst.getId());
        JobApplicationDTO kimHirstJobApplicationDTO = new JobApplicationDTO();
        kimHirstJobApplicationDTO.setJobApplicationText("I'd like to have this position as I'm very much into research!");
        kimHirstJobApplicationDTO = this.processService.addJobApplication(kimHirst.getId(), kimHirstDefaultRoleAssignmentDTO.getId(), recruitmentProcessDTO.getId(), kimHirstJobApplicationDTO);
        logger.info("Kim Hirst has submitted an application (ID="+kimHirstJobApplicationDTO.getId()+") and is now an applicant!");

        // get newly created RoleAssignment
        Set<RoleAssignmentDTO> kimHirstJobApplicationRoleAssignmentDTOs = this.authorizationService.getRoleAssignmentsOfAgentAndGuardedEntity(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), kimHirst.getId(), kimHirstJobApplicationDTO.getId());
        RoleAssignmentDTO kimHirstJobApplicationRoleAssignmentDTO;
        if (kimHirstJobApplicationRoleAssignmentDTOs.size() != 1){
            throw new ControllerException("There is an error in the the creation of the RoleAssignment for the Role Applicant");
        }
        kimHirstJobApplicationRoleAssignmentDTO = kimHirstJobApplicationRoleAssignmentDTOs.iterator().next();
        logger.info("Kim Hirst may update her application via the Role Applicant and the corresponding roleAssignment with the ID="+sarahDostJobApplicationRoleAssignmentDTO.getId());


        // make Kate Listander a Reviewer
        logger.info("Let Kate Listander review the application of Sarah Dost!");
        RoleAssignmentDTO kateListanderReviewerRoleAssignmentDTO = this.processService.addReviewerToJobApplication(maxMueller.getId(), maxMuellerRecruitmentProcessRoleAssignmentDTO.getId(), sarahDostJobApplicationDTO.getId(), kateListander.getId());
        logger.info("Kate Listander is now a reviewer to assess the application of Sarah Dost (roleAssignmentId="+kateListanderReviewerRoleAssignmentDTO.getId()+")!");

        // make Kate Listander a Reviewer
        logger.info("Let Max Mueller review the application of Kim Hirst!");
        RoleAssignmentDTO maxMuellerReviewerRoleAssignmentDTO = this.processService.addReviewerToJobApplication(maxMueller.getId(), maxMuellerRecruitmentProcessRoleAssignmentDTO.getId(), kimHirstJobApplicationDTO.getId(), maxMueller.getId());
        logger.info("Max Mueller is now a reviewer to assess the application of Kim Hirst (roleAssignmentId="+maxMuellerReviewerRoleAssignmentDTO.getId()+")!");

        logger.info("Let Kate Listander create a review for the application of Sarah Dost!");
        JobApplicationReviewDTO jobApplicationReviewOfSarahDostDTO = new JobApplicationReviewDTO();
        jobApplicationReviewOfSarahDostDTO.setJobApplicationReviewText("The job application is quite short.");
        jobApplicationReviewOfSarahDostDTO = this.processService.addJobApplicationReview(kateListander.getId(), kateListanderReviewerRoleAssignmentDTO.getId(), sarahDostJobApplicationDTO.getId(), jobApplicationReviewOfSarahDostDTO);
        logger.info("Kate Listander has created a review (ID="+jobApplicationReviewOfSarahDostDTO.getId()+") for the application of Sarah Dost!");

        logger.info("Let Max Mueller create a review for the application of Kim Hirst!");
        JobApplicationReviewDTO jobApplicationReviewOfKimHirstDTO = new JobApplicationReviewDTO();
        jobApplicationReviewOfKimHirstDTO.setJobApplicationReviewText("The job application is short, but there is motivation.");
        jobApplicationReviewOfKimHirstDTO = this.processService.addJobApplicationReview(maxMueller.getId(), maxMuellerReviewerRoleAssignmentDTO.getId(), kimHirstJobApplicationDTO.getId(), jobApplicationReviewOfKimHirstDTO);
        logger.info("Max Mueller has created a review (ID="+jobApplicationReviewOfKimHirstDTO.getId()+") for the application of Kim Hirst!");

        logger.info("Let Max Mueller finally create a contract offer for Kim Hirst!");
        ContractOfferDTO kimHirstContractOfferDTO = new ContractOfferDTO();
        kimHirstContractOfferDTO.setContractOfferText("We would like to offer the PhD position to you. ...");
        kimHirstContractOfferDTO = this.processService.addContractOffer(maxMueller.getId(), maxMuellerRecruitmentProcessRoleAssignmentDTO.getId(), recruitmentProcessDTO.getId(), kimHirstContractOfferDTO, kimHirst.getId());
        logger.info("Max Mueller has created a contract offer (ID="+kimHirstContractOfferDTO.getId()+") for Kim Hirst!");

        // get newly created RoleAssignment
        Set<RoleAssignmentDTO> kimHirstContractOfferRoleAssignmentDTOs = this.authorizationService.getRoleAssignmentsOfAgentAndGuardedEntity(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), kimHirst.getId(), kimHirstContractOfferDTO.getId());
        RoleAssignmentDTO kimHirstContractOfferRoleAssignmentDTO;
        if (kimHirstContractOfferRoleAssignmentDTOs.size() != 1){
            throw new ControllerException("There is an error in the the creation of the RoleAssignment for the Role Candidate");
        }
        kimHirstContractOfferRoleAssignmentDTO = kimHirstContractOfferRoleAssignmentDTOs.iterator().next();
        logger.info("Kim Hirst may retrieve the contract offer via the Role Candidate and the corresponding roleAssignment with the ID="+kimHirstContractOfferRoleAssignmentDTO.getId());

        // return all relevant objects to illustrate the use case

        Set<JobApplicationDTO> jobApplicationDTOS = new LinkedHashSet<>();
        jobApplicationDTOS.add(sarahDostJobApplicationDTO);
        jobApplicationDTOS.add(kimHirstJobApplicationDTO);
        Set<JobApplicationReviewDTO> jobApplicationReviewDTOS = new LinkedHashSet<>();
        jobApplicationReviewDTOS.add(jobApplicationReviewOfSarahDostDTO);
        jobApplicationReviewDTOS.add(jobApplicationReviewOfKimHirstDTO);
        Set<ContractOfferDTO> contractOfferDTOS = new LinkedHashSet<>();
        contractOfferDTOS.add(kimHirstContractOfferDTO);

        Set<UserDTO> userDTOS = new LinkedHashSet<>();
        userDTOS.add(new UserDTO(agent));
        userDTOS.add(maxMueller);
        userDTOS.add(kateListander);
        userDTOS.add(kimHirst);
        userDTOS.add(sarahDost);

        Set<RoleAssignmentDTO> useCaseRoleAssignmentDTOS = new LinkedHashSet<>();
        useCaseRoleAssignmentDTOS.add(new RoleAssignmentDTO(currentRoleAssignment));
        useCaseRoleAssignmentDTOS.add(maxMuellerEmployeeRoleAssignmentDTO);
        useCaseRoleAssignmentDTOS.add(kateListanderEmployeeRoleAssignmentDTO);
        useCaseRoleAssignmentDTOS.add(sarahDostJobApplicationRoleAssignmentDTO);
        useCaseRoleAssignmentDTOS.add(kimHirstJobApplicationRoleAssignmentDTO);
        useCaseRoleAssignmentDTOS.add(kimHirstContractOfferRoleAssignmentDTO);

        return new UseCaseDTO(recruitmentProcessDTO, jobApplicationDTOS, jobApplicationReviewDTOS, contractOfferDTOS, userDTOS,
                roles, useCaseRoleAssignmentDTOS);
    }

}
