package info.dbis.orac.control;

import info.dbis.orac.common.QueryParameter;
import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authentication.Ability;
import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authentication.OrgRole;
import info.dbis.orac.datamodel.authentication.OrgUnit;
import info.dbis.orac.datamodel.authorization.*;
import info.dbis.orac.datamodel.dto.authorization.RoleAssignmentDTO;
import info.dbis.orac.datamodel.dto.authorization.RoleDTO;
import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.AuthorizationException;
import info.dbis.orac.exception.ControllerException;
import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;

import javax.inject.Inject;
import java.util.*;

/**
 * AuthorizationService
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
public class AuthorizationService {

    @Inject
    private PersistenceService persistenceService;

    // get Roles

    @RequiresAccessControl(targetObjectClazz = Role.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.LISTING)
    public Set<RoleDTO> getRoles(Long agentId, Long roleAssignmentId, Long humanResourceSystemId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        List<Role> roles = this.persistenceService.findWithNamedQuery(Role.QUERY__HUMAN_RESOURCE_SYSTEM_ID, QueryParameter.with("humanResourceSystemId", humanResourceSystemId).parameters());

        // get and return Roles
        Set<RoleDTO> roleDTOs = new HashSet<>();
        for (Role role : roles) {
            roleDTOs.add(new RoleDTO(role));
        }

        return roleDTOs;
    }

    // RoleAssignment-specific basic services
    // In theory, everybody may create a RoleAssignment. It's checked in the method whether someone
    public RoleAssignmentDTO addRoleAssignment(Long agentId, Long roleAssignmentId, Long humanResourceSystemId, RoleAssignmentDTO roleAssignmentDTO, Long guardedEntityId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Agent
        Agent creator = this.persistenceService.get(Agent.class, agentId);

        // get current RoleAssignment
        RoleAssignment currentRoleAssignment = this.persistenceService.get(RoleAssignment.class, roleAssignmentId);

        // get HumanResourceSystem
        HumanResourceSystem humanResourceSystem = this.persistenceService.get(HumanResourceSystem.class, humanResourceSystemId);

        // get Role
        Role role;
        if (roleAssignmentDTO.getRoleId() != null) {
            role = this.persistenceService.get(Role.class, roleAssignmentDTO.getRoleId());
        } else {
            throw new ControllerException("You must set the id of an existing Role");
        }

        // get GuardedEntity
        GuardedEntity guardedEntity = this.persistenceService.get(role.getKeyRoleScope().getTargetEntityClazz(), guardedEntityId);

        boolean guardedEntitiyIsHumanResourceSystem = false;
        if (role.getKeyRoleScope().getTargetEntityClazz().equals(HumanResourceSystem.class)) {
            guardedEntitiyIsHumanResourceSystem = true;
        }

        // get Agents or OrgEntities
        Set<Agent> agents = new HashSet<>();
        Set<OrgRole> orgRoles = new HashSet<>();
        Set<OrgUnit> orgUnits = new HashSet<>();
        Set<Ability> abilities = new HashSet<>();

        if (roleAssignmentDTO.getAgentIds() != null) {
            for (Long aId : roleAssignmentDTO.getAgentIds()) {
                Agent agent = this.persistenceService.get(Agent.class, aId);
                agents.add(agent);
            }
        } else {
            if (roleAssignmentDTO.getOrgUnitIds() != null || roleAssignmentDTO.getOrgRoleIds() != null || roleAssignmentDTO.getAbilitiesIds() != null) {
                if (guardedEntitiyIsHumanResourceSystem) {
                    throw new ControllerException("You are not allowed to reference OrgEntities while referring to the HumanResourceSystem!");
                }
                for (Long orgUnitId : roleAssignmentDTO.getOrgUnitIds()) {
                    OrgUnit orgUnit = this.persistenceService.get(OrgUnit.class, orgUnitId);
                    orgUnits.add(orgUnit);
                }
                for (Long orgRoleId : roleAssignmentDTO.getOrgRoleIds()) {
                    OrgRole orgRole = this.persistenceService.get(OrgRole.class, orgRoleId);
                    orgRoles.add(orgRole);
                }
                for (Long abilityId : roleAssignmentDTO.getAbilitiesIds()) {
                    Ability ability = this.persistenceService.get(Ability.class, abilityId);
                    abilities.add(ability);
                }
            } else {
                if (guardedEntitiyIsHumanResourceSystem) {
                    throw new ControllerException("At least, you must either set the id of one Agent!");
                } else {
                    throw new ControllerException("At least, you must either set the id of an Agent or the ids of some OrgUnits, OrgRoles, or Abilities!");
                }
            }
        }

        RoleAssignment newRoleAssignment;

        if (agents.size() > 0) {
            newRoleAssignment = new RoleAssignment(creator, humanResourceSystem, role, agents);
        } else {
            newRoleAssignment = new RoleAssignment(creator, humanResourceSystem, role, orgUnits, orgRoles, abilities);
        }

        // create new RoleScopeAssignment
        RoleScopeAssignment keyRoleScopeAssignment = new RoleScopeAssignment(creator, newRoleAssignment, role.getKeyRoleScope(), guardedEntity);

        // check if the RoleAssignment can be created based on the Privileges the Agent has
        boolean privilegeComplianceCheckOk = this.privilegeComplianceCheck(currentRoleAssignment, newRoleAssignment);

        if (!privilegeComplianceCheckOk) {
            throw new AuthorizationException("You are not allowed to create this RoleAssignment! You do not obtain the Privileges granted in the new RoleAssignment on yourself.");
        }

        this.persistenceService.persist(newRoleAssignment);
        this.persistenceService.persist(keyRoleScopeAssignment);

        return new RoleAssignmentDTO(newRoleAssignment);

    }

    @RequiresAccessControl(targetObjectClazz = RoleAssignment.class, contextObjectClazz = RoleAssignment.class, actionType = RequiresAccessControl.ActionType.READ)
    public RoleAssignmentDTO getRoleAssignment(Long agentId, Long roleAssignmentId, Long targetRoleAssignmentId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get RoleAssignment
        RoleAssignment roleAssignment = this.persistenceService.get(RoleAssignment.class, targetRoleAssignmentId);

        // return RoleAssignment
        return new RoleAssignmentDTO(roleAssignment);
    }

    public RoleAssignmentDTO getDefaultRoleAssignmentOfAgent(Long agentId) throws PersistenceException, ControllerException, AuthenticationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get RoleAssignment(s)
        List<RoleAssignment> roleAssignments = this.persistenceService.findWithNamedQuery(RoleAssignment.QUERY__AGENT__TARGET_ENTITY_CLAZZ__ON_ASSIGNMENT_DEFAULT, QueryParameter.with("agent", agent).and("targetEntityClazz", HumanResourceSystem.class).and("onAssignmentDefault", true).parameters());

        if (roleAssignments.size() != 1) {
            throw new ControllerException("The default RoleAssignment could not be retrieved for this Agent!");
        }

        // Map RoleAssignment
        return new RoleAssignmentDTO(roleAssignments.get(0));
    }

    @RequiresAccessControl(targetObjectClazz = RoleAssignment.class, contextObjectClazz = RoleAssignment.class, actionType = RequiresAccessControl.ActionType.REMOVE)
    public void removeRoleAssignment(Long agentId, Long roleAssignmentId, Long targetRoleAssignmentId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get current RoleAssignment
        RoleAssignment currentRoleAssignment = this.persistenceService.get(RoleAssignment.class, roleAssignmentId);

        // get target RoleAssignment
        RoleAssignment targetRoleAssignment = this.persistenceService.get(RoleAssignment.class, targetRoleAssignmentId);

        // check if the RoleAssignment can be removed based on the Privileges the Agent currently obtains
        boolean privilegeComplianceCheckOk = this.privilegeComplianceCheck(currentRoleAssignment, targetRoleAssignment);

        if (!privilegeComplianceCheckOk) {
            throw new AuthorizationException("You are not allowed to remove this RoleAssignment! You do not obtain the Privileges granted in the this RoleAssignment.");
        }

        for (RoleScopeAssignment roleScopeAssignment : targetRoleAssignment.getRoleScopeAssignments()) {
            targetRoleAssignment.removeRoleScopeAssignment(roleScopeAssignment);
            this.persistenceService.delete(RoleScopeAssignment.class, roleScopeAssignment.getId());
        }

        this.persistenceService.delete(RoleAssignment.class, targetRoleAssignmentId);
    }

    @RequiresAccessControl(targetObjectClazz = RoleAssignment.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.LISTING)
    public Set<RoleAssignmentDTO> getRoleAssignments(Long agentId, Long roleAssignmentId, Long humanResourceSystemId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get HumanResourceSystem
        HumanResourceSystem humanResourceSystem = this.persistenceService.get(HumanResourceSystem.class, humanResourceSystemId);

        Set<RoleAssignmentDTO> roleAssignmentDTOS = new HashSet<>();
        for (RoleAssignment roleAssignment : humanResourceSystem.getRoleAssignments()) {
            roleAssignmentDTOS.add(new RoleAssignmentDTO(roleAssignment));
        }
        return roleAssignmentDTOS;
    }

    public Set<RoleAssignmentDTO> getRoleAssignmentsOfCurrentAgent(Long agentId) throws PersistenceException, ControllerException, AuthenticationException {
        // get RoleAssignment(s)
        List<RoleAssignment> roleAssignments = this.persistenceService.findWithNamedQuery(RoleAssignment.QUERY__AGENT_ID, QueryParameter.with("agentId", agentId).parameters());

        Set<RoleAssignmentDTO> roleAssignmentDTOS = new HashSet<>();
        for (RoleAssignment roleAssignment : roleAssignments) {
            roleAssignmentDTOS.add(new RoleAssignmentDTO(roleAssignment));
        }
        return roleAssignmentDTOS;
    }

    public Set<RoleAssignmentDTO> getRoleAssignmentsOfCurrentAgentAndGuardedEntity(Long agentId, Long guardedEntityId) throws PersistenceException, ControllerException, AuthenticationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get RoleAssignment(s)
        List<RoleAssignment> roleAssignments = this.persistenceService.findWithNamedQuery(RoleAssignment.QUERY__AGENT__GUARDED_ENTITY_ID, QueryParameter.with("agent", agent).and("guardedEntityId", guardedEntityId).parameters());

        Set<RoleAssignmentDTO> roleAssignmentDTOS = new HashSet<>();
        for (RoleAssignment roleAssignment : roleAssignments) {
            roleAssignmentDTOS.add(new RoleAssignmentDTO(roleAssignment));
        }
        return roleAssignmentDTOS;
    }

    @RequiresAccessControl(targetObjectClazz = RoleAssignment.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.LISTING)
    public Set<RoleAssignmentDTO> getRoleAssignmentsOfAgentAndGuardedEntity(Long agentId, Long roleAssignmentId, Long humanResourceSystemId, Long targetAgentId, Long guardedEntityId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // TODO: add dedicated access control checks here

        // get target Agent
        Agent targetAgent = this.persistenceService.get(Agent.class, targetAgentId);

        // get RoleAssignments
        List<RoleAssignment> roleAssignments = this.persistenceService.findWithNamedQuery(RoleAssignment.QUERY__AGENT__GUARDED_ENTITY_ID, QueryParameter.with("agent", targetAgent).and("guardedEntityId", guardedEntityId).parameters());

        // Map Privileges and check permissions
        Set<RoleAssignmentDTO> roleAssignmentDTOs = new HashSet<>();
        for (RoleAssignment roleAssignment : roleAssignments) {
            roleAssignmentDTOs.add(new RoleAssignmentDTO(roleAssignment));
        }

        return roleAssignmentDTOs;
    }

    public void updateRoleScopeAssignmentsAfterEntityRemoval(Long guardedEntityId, Class<? extends GuardedEntity> guardedEntityClazz) throws PersistenceException, ControllerException {
        // get RoleScopeAssignments
        List<RoleScopeAssignment> roleScopeAssignments = this.persistenceService.findWithNamedQuery(RoleScopeAssignment.QUERY__GUARDED_ENTITY_ID, QueryParameter.with("guardedEntityId", guardedEntityId).parameters());

        // iterate RoleScopeAssignments
        for (RoleScopeAssignment roleScopeAssignment : roleScopeAssignments){
            // if we consider the key RoleScope -> delete RoleAssignment as well
            if (roleScopeAssignment.getRoleAssignment().getRole().getKeyRoleScope().equals(roleScopeAssignment.getRoleScope())){
                // delete RoleAssignment with all RoleScopeAssignments
                RoleAssignment roleAssignment = roleScopeAssignment.getRoleAssignment();
                for (RoleScopeAssignment rsa : roleAssignment.getRoleScopeAssignments()){
                    rsa.setRoleScope(null);
                }
                roleAssignment.setHumanResourceSystem(null);
                roleAssignment.setRole(null);
                this.persistenceService.delete(RoleAssignment.class, roleAssignment.getId()); // RoleScopeAssignments are deleted through cascading
            }
            else { // only delete the RoleScopeAssignment
                roleScopeAssignment.setRoleScope(null);
                this.persistenceService.delete(RoleScopeAssignment.class, roleScopeAssignment.getId());
            }
        }
    }

    /**
     * @param roleAssignment
     * @param targetRoleAssignment
     * @return
     * @throws PersistenceException
     * @throws ControllerException
     */

    private boolean privilegeComplianceCheck(RoleAssignment roleAssignment, RoleAssignment targetRoleAssignment) throws PersistenceException, ControllerException {
        for (RoleScopeAssignment roleScopeAssignment : targetRoleAssignment.getRoleScopeAssignments()) {
            // get referenced GuardedEntity
            GuardedEntity guardedEntity = this.persistenceService.get(roleScopeAssignment.getRoleScope().getTargetEntityClazz(), roleScopeAssignment.getGuardedEntityId());

            // get all Privileges connected to the RoleScopeAssignment
            Set<Privilege> entityPrivileges = roleScopeAssignment.getRoleScope().getRoleScopePrivilegeAssociation().getEntityPrivileges();
            if (roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation() != null){
                entityPrivileges.addAll(roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation().getEntityPrivileges());
            }
            Set<Privilege> hierarchicalPrivileges = roleScopeAssignment.getRoleScope().getRoleScopePrivilegeAssociation().getHierarchicalPrivileges();
            if (roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation() != null){
                hierarchicalPrivileges.addAll(roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation().getHierarchicalPrivileges());
            }

            // check coverage of Privileges
            if (!this.privilegeComplianceCheck(roleAssignment, guardedEntity, entityPrivileges, hierarchicalPrivileges)) {
                return false;
            }
        }
        return true;
    }

    /**
     * This method check whether the Privileges referenced by a RoleAssigment match the entityPrivileges and hierarchicalPrivileges which shall be granted or removed in relation to the GuardedEntity
     *
     * @param roleAssignment
     * @param guardedEntity
     * @param entityPrivileges
     * @param hierarchicalPrivileges
     * @return boolean privilegeComplianceCheckOK
     */

    private boolean privilegeComplianceCheck(RoleAssignment roleAssignment, GuardedEntity guardedEntity, Set<Privilege> entityPrivileges, Set<Privilege> hierarchicalPrivileges) throws PersistenceException, ControllerException {
        Set<Privilege> nonCoveredEntityPrivileges = new HashSet<>(entityPrivileges);
        Set<Privilege> nonCoveredHierarchicalPrivileges = new HashSet<>(hierarchicalPrivileges);

        // check if Role
        for (RoleScopeAssignment roleScopeAssignment : roleAssignment.getRoleScopeAssignments()) {
            if (roleScopeAssignment.getGuardedEntityId() == guardedEntity.getId()) {
                nonCoveredEntityPrivileges.removeAll(roleScopeAssignment.getRoleScope().getRoleScopePrivilegeAssociation().getEntityPrivileges());
                nonCoveredHierarchicalPrivileges.removeAll(roleScopeAssignment.getRoleScope().getRoleScopePrivilegeAssociation().getHierarchicalPrivileges());
                if (roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation() != null){
                    nonCoveredEntityPrivileges.removeAll(roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation().getEntityPrivileges());
                    nonCoveredHierarchicalPrivileges.removeAll(roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation().getHierarchicalPrivileges());
                }
            } else {
                if (isParentGuardedEntity(roleScopeAssignment.getGuardedEntityId(), guardedEntity)) {
                    nonCoveredEntityPrivileges.removeAll(roleScopeAssignment.getRoleScope().getRoleScopePrivilegeAssociation().getHierarchicalPrivileges());
                    nonCoveredHierarchicalPrivileges.removeAll(roleScopeAssignment.getRoleScope().getRoleScopePrivilegeAssociation().getHierarchicalPrivileges());

                    if (roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation() != null){
                        nonCoveredEntityPrivileges.removeAll(roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation().getHierarchicalPrivileges());
                        nonCoveredHierarchicalPrivileges.removeAll(roleScopeAssignment.getAdditionalRoleScopePrivilegeAssociation().getHierarchicalPrivileges());
                    }

                }
            }
        }

        return (nonCoveredEntityPrivileges.size() == 0) && (nonCoveredHierarchicalPrivileges.size() == 0);
    }

    /**
     * This method is a helper one to detect whether a certain GuardedEntity childGuardedEntity has a parental, contextual GuardedEntity with th ID parentGuardedEntityId
     *
     * @param parentGuardedEntityId
     * @param childGuardedEntity
     * @return boolean isParent
     * @throws ControllerException
     */

    private boolean isParentGuardedEntity(Long parentGuardedEntityId, GuardedEntity childGuardedEntity) throws ControllerException {
        Set<GuardedEntity> parentGuardedEntites = childGuardedEntity.getContextGuardedEntities();
        int deadlockPrevention = 10000;
        do {
            Set<GuardedEntity> newParentGuardedEntities = new HashSet<>();
            for (GuardedEntity parentGuardedEntity : parentGuardedEntites) {
                if (parentGuardedEntityId == parentGuardedEntity.getId()) {
                    return true;
                } else {
                    newParentGuardedEntities.addAll(parentGuardedEntity.getContextGuardedEntities());
                }
            }
            parentGuardedEntites = newParentGuardedEntities;
        }
        while (!parentGuardedEntites.isEmpty() && deadlockPrevention-- > 0);

        throw new ControllerException("There is a severe error in the design of entities. Please contact an administrator immediately!");
    }

}
