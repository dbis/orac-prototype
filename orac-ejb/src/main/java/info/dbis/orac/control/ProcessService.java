package info.dbis.orac.control;

import info.dbis.orac.common.QueryParameter;
import info.dbis.orac.datamodel.*;
import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authorization.RequiresAccessControl;
import info.dbis.orac.datamodel.authorization.Role;
import info.dbis.orac.datamodel.authorization.RoleScopeAssignment;
import info.dbis.orac.datamodel.dto.*;
import info.dbis.orac.datamodel.dto.authorization.RoleAssignmentDTO;
import info.dbis.orac.datamodel.dto.authorization.RoleScopeAssignmentDTO;
import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.AuthorizationException;
import info.dbis.orac.exception.ControllerException;
import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * ProcessService
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Stateless
public class ProcessService {

    @Inject
    private PersistenceService persistenceService;

    @Inject
    private SystemService systemService;

    @Inject
    private AuthorizationService authorizationService;

    @RequiresAccessControl(targetObjectClazz = RecruitmentProcess.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.ADD)
    public RecruitmentProcessDTO addRecruitmentProcess(Long agentId, Long roleAssignmentId, Long humanResourceSystemId, RecruitmentProcessDTO recruitmentProcessDTO) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get HumanResourceSystem
        HumanResourceSystem humanResourceSystem = this.persistenceService.get(HumanResourceSystem.class, humanResourceSystemId);

        // create RecruitmentProcess
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(agent, humanResourceSystem);

        // create JobOffer
        JobOffer jobOffer = new JobOffer(agent, recruitmentProcessDTO.getJobOfferDTO().getJobOfferText(), recruitmentProcess);

        // persist RecruitmentProcess and JobOffer
        recruitmentProcess = this.persistenceService.persist(recruitmentProcess);
        this.persistenceService.persist(jobOffer);

        // return RecruitmentProcess
        return new RecruitmentProcessDTO(recruitmentProcess);
    }

    @RequiresAccessControl(targetObjectClazz = RecruitmentProcess.class, contextObjectClazz = RecruitmentProcess.class, actionType = RequiresAccessControl.ActionType.READ)
    public RecruitmentProcessDTO getRecruitmentProcess(Long agentId, Long roleAssignmentId, Long recruitmentProcessId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get RecruitmentProcess
        RecruitmentProcess recruitmentProcess = this.persistenceService.get(RecruitmentProcess.class, recruitmentProcessId);

        // return RecruitmentProcess
        return new RecruitmentProcessDTO(recruitmentProcess);
    }

    @RequiresAccessControl(targetObjectClazz = RecruitmentProcess.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.ADD)
    public void removeRecruitmentProcess(Long agentId, Long roleAssignmentId, Long recruitmentProcessId, Long humanResourceSystemId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get RecruitmentProcess
        RecruitmentProcess recruitmentProcess = this.persistenceService.get(RecruitmentProcess.class, recruitmentProcessId);

        // get HumanResourceSystem
        HumanResourceSystem humanResourceSystem = recruitmentProcess.getHumanResourceSystem();

        // sanity check
        if (!humanResourceSystemId.equals(humanResourceSystem.getId())){
            throw new ControllerException("The given HumanResourceSystem does not contain the given RecruitmentProcess!");
        }

        // remove RecruitmentProcess
        humanResourceSystem.removeRecruitmentProcess(recruitmentProcess);
        this.persistenceService.delete(RecruitmentProcess.class, recruitmentProcessId);
    }

    @RequiresAccessControl(targetObjectClazz = RecruitmentProcess.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.LISTING)
    public List<RecruitmentProcessDTO> getRecruitmentProcesses(Long agentId, Long roleAssignmentId, Long humanResourceSystemId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get RecruitmentProcesses
        List<RecruitmentProcess> recruitmentProcesses = this.persistenceService.findWithNamedQuery(RecruitmentProcess.QUERY__BY__HUMAN_RESOURCE_SYSTEM, QueryParameter.with("humanResourceSystemId", humanResourceSystemId).parameters());

        // map RecruitmentProcesses
        List<RecruitmentProcessDTO> recruitmentProcessDTOList = new LinkedList<>();
        recruitmentProcesses.forEach(recruitmentProcess -> recruitmentProcessDTOList.add(new RecruitmentProcessDTO(recruitmentProcess)));

        // return RecruitmentProcesses
        return recruitmentProcessDTOList;
    }

    @RequiresAccessControl(targetObjectClazz = JobOffer.class, contextObjectClazz = RecruitmentProcess.class, actionType = RequiresAccessControl.ActionType.READ)
    public JobOfferDTO getJobOffer(Long agentId, Long roleAssignmentId, Long jobOfferId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get JobOffer
        JobOffer jobOffer = this.persistenceService.get(JobOffer.class, jobOfferId);

        // return JobOffer
        return new JobOfferDTO(jobOffer);
    }

    @RequiresAccessControl(targetObjectClazz = JobOffer.class, contextObjectClazz = JobOffer.class, actionType = RequiresAccessControl.ActionType.UPDATE)
    public JobOfferDTO updateJobOffer(Long agentId, Long roleAssignmentId, Long jobOfferId, JobOfferDTO jobOfferDTO) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get JobOffer
        JobOffer jobOffer = this.persistenceService.get(JobOffer.class, jobOfferId);

        // update JobOffer
        if (jobOfferDTO.getJobOfferText() != null){
            jobOffer.setJobOfferText(jobOfferDTO.getJobOfferText());
        }

        // return JobOffer
        return new JobOfferDTO(jobOffer);
    }

    @RequiresAccessControl(targetObjectClazz = JobOffer.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.LISTING)
    public List<JobOfferDTO> getJobOffers(Long agentId, Long roleAssignmentId, Long humanResourceSystemId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get JobOffers
        List<JobOffer> jobOfferList = this.persistenceService.findWithNamedQuery(JobOffer.QUERY__BY__HUMAN_RESOURCE_SYSTEM, QueryParameter.with("humanResourceSystemId", humanResourceSystemId).parameters());

        // map JobOffers
        List<JobOfferDTO> jobOfferDTOList = new LinkedList<>();
        jobOfferList.forEach(jobOffer -> jobOfferDTOList.add(new JobOfferDTO(jobOffer)));

        // return JobOffers
        return jobOfferDTOList;
    }

    @RequiresAccessControl(targetObjectClazz = JobApplication.class, contextObjectClazz = RecruitmentProcess.class, actionType = RequiresAccessControl.ActionType.ADD)
    public JobApplicationDTO addJobApplication(Long agentId, Long roleAssignmentId, Long recruitmentProcessId, JobApplicationDTO jobApplicationDTO) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get RecruitmentProcess
        RecruitmentProcess recruitmentProcess = this.persistenceService.get(RecruitmentProcess.class, recruitmentProcessId);

        // create JobApplication
        JobApplication jobApplication = new JobApplication(agent, jobApplicationDTO.getJobApplicationText(), recruitmentProcess);

        // persist JobApplication
        jobApplication = this.persistenceService.persist(jobApplication);

        // return JobApplication
        return new JobApplicationDTO(jobApplication);
    }

    @RequiresAccessControl(targetObjectClazz = JobApplication.class, contextObjectClazz = JobApplication.class, actionType = RequiresAccessControl.ActionType.READ)
    public JobApplicationDTO getJobApplication(Long agentId, Long roleAssignmentId, Long jobApplicationId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get JobApplication
        JobApplication jobApplication = this.persistenceService.get(JobApplication.class, jobApplicationId);

        // return JobApplication
        return new JobApplicationDTO(jobApplication);
    }

    @RequiresAccessControl(targetObjectClazz = JobApplication.class, contextObjectClazz = JobApplication.class, actionType = RequiresAccessControl.ActionType.UPDATE)
    public JobApplicationDTO updateJobApplication(Long agentId, Long roleAssignmentId, Long jobApplicationId, JobApplicationDTO jobApplicationDTO) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get JobApplication
        JobApplication jobApplication = this.persistenceService.get(JobApplication.class, jobApplicationId);

        // update JobApplication
        if (jobApplicationDTO.getJobApplicationText() != null){
            jobApplication.setJobApplicationText(jobApplicationDTO.getJobApplicationText());
        }

        // return JobApplication
        return new JobApplicationDTO(jobApplication);
    }

    @RequiresAccessControl(targetObjectClazz = JobApplication.class, contextObjectClazz = RecruitmentProcess.class, actionType = RequiresAccessControl.ActionType.ADD)
    public void removeJobApplication(Long agentId, Long roleAssignmentId, Long jobApplicationId, Long recruitmentProcessId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get JobApplication
        JobApplication jobApplication = this.persistenceService.get(JobApplication.class, jobApplicationId);

        // get RecruitmentProcess
        RecruitmentProcess recruitmentProcess = jobApplication.getRecruitmentProcess();

        // sanity check
        if (!recruitmentProcessId.equals(recruitmentProcess.getId())){
            throw new ControllerException("The given RecruitmentProcess does not contain the given JobApplication!");
        }

        // remove JobApplication
        recruitmentProcess.removeJobApplication(jobApplication);
        this.persistenceService.delete(JobApplication.class, jobApplicationId);
    }

    @RequiresAccessControl(targetObjectClazz = JobApplication.class, contextObjectClazz = RecruitmentProcess.class, actionType = RequiresAccessControl.ActionType.LISTING)
    public List<JobApplicationDTO> getJobApplications(Long agentId, Long roleAssignmentId, Long recruitmentProcessId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get JobApplications
        List<JobApplication> jobApplications = this.persistenceService.findWithNamedQuery(JobApplication.QUERY__BY__RECRUITMENT_PROCESS, QueryParameter.with("recruitmentProcessId", recruitmentProcessId).parameters());

        // map JobApplications
        List<JobApplicationDTO> jobApplicationDTOList = new LinkedList<>();
        jobApplications.forEach(jobApplication -> jobApplicationDTOList.add(new JobApplicationDTO(jobApplication)));

        // return JobApplications
        return jobApplicationDTOList;
    }

    @RequiresAccessControl(targetObjectClazz = JobApplication.class, contextObjectClazz = JobApplication.class, actionType = RequiresAccessControl.ActionType.ADVANCED)
    public RoleAssignmentDTO addReviewerToJobApplication(Long agentId, Long roleAssignmentId, Long jobApplicationId, Long futureReviewerUserId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Reviewer-Role
        List<Role> roles =  this.persistenceService.findWithNamedQuery(Role.QUERY__ASSIGNMENT_DEFAULT__TARGET_CLAZZ, QueryParameter.with("targetEntityClazz", JobApplication.class).and("onAssignmentDefault", true).parameters());

        if (roles.size() == 1){
            RoleAssignmentDTO roleAssignmentDTO = new RoleAssignmentDTO();
            Set<Long> agentIds = new LinkedHashSet<>();
            agentIds.add(futureReviewerUserId);
            roleAssignmentDTO.setAgentIds(agentIds);

            roleAssignmentDTO.setRoleId(roles.get(0).getId());

            return this.authorizationService.addRoleAssignment(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), roleAssignmentDTO, jobApplicationId);
        }

        throw new ControllerException("The Role could not be assigned!");
    }

    @RequiresAccessControl(targetObjectClazz = JobApplicationReview.class, contextObjectClazz = JobApplication.class, actionType = RequiresAccessControl.ActionType.ADD)
    public JobApplicationReviewDTO addJobApplicationReview(Long agentId, Long roleAssignmentId, Long jobApplicationId, JobApplicationReviewDTO jobApplicationReviewDTO) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get JobApplication
        JobApplication jobApplication = this.persistenceService.get(JobApplication.class, jobApplicationId);

        // create JobApplicationReview
        JobApplicationReview jobApplicationReview = new JobApplicationReview(agent, jobApplicationReviewDTO.getJobApplicationReviewText(), jobApplication);

        // persist JobApplicationReview
        jobApplicationReview = this.persistenceService.persist(jobApplicationReview);

        // return JobApplication
        return new JobApplicationReviewDTO(jobApplicationReview);
    }

    @RequiresAccessControl(targetObjectClazz = JobApplicationReview.class, contextObjectClazz = JobApplicationReview.class, actionType = RequiresAccessControl.ActionType.READ)
    public JobApplicationReviewDTO getJobApplicationReview(Long agentId, Long roleAssignmentId, Long jobApplicationReviewId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get JobApplicationReview
        JobApplicationReview jobApplicationReview = this.persistenceService.get(JobApplicationReview.class, jobApplicationReviewId);

        // return JobApplicationReview
        return new JobApplicationReviewDTO(jobApplicationReview);
    }

    @RequiresAccessControl(targetObjectClazz = JobApplicationReview.class, contextObjectClazz = JobApplicationReview.class, actionType = RequiresAccessControl.ActionType.UPDATE)
    public JobApplicationReviewDTO updateJobApplicationReview(Long agentId, Long roleAssignmentId, Long jobApplicationReviewId, JobApplicationReviewDTO jobApplicationReviewDTO) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get JobApplicationReview
        JobApplicationReview jobApplicationReview = this.persistenceService.get(JobApplicationReview.class, jobApplicationReviewId);

        // update JobApplicationReview
        if (jobApplicationReviewDTO.getJobApplicationReviewText() != null){
            jobApplicationReview.setJobApplicationReviewText(jobApplicationReviewDTO.getJobApplicationReviewText());
        }

        // return JobApplicationReview
        return new JobApplicationReviewDTO(jobApplicationReview);
    }

    @RequiresAccessControl(targetObjectClazz = JobApplicationReview.class, contextObjectClazz = JobApplication.class, actionType = RequiresAccessControl.ActionType.ADD)
    public void removeJobApplicationReview(Long agentId, Long roleAssignmentId, Long jobApplicationReviewId, Long jobApplicationId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get JobApplicationReview
        JobApplicationReview jobApplicationReview = this.persistenceService.get(JobApplicationReview.class, jobApplicationReviewId);

        // get JobApplication
        JobApplication jobApplication = jobApplicationReview.getJobApplication();

        // sanity check
        if (!jobApplicationId.equals(jobApplication.getId())){
            throw new ControllerException("The given JobApplication does not contain the given JobApplicationReview!");
        }

        // remove JobApplicationReview
        jobApplication.removeJobApplicationReview(jobApplicationReview);
        this.persistenceService.delete(JobApplicationReview.class, jobApplicationReviewId);
    }

    @RequiresAccessControl(targetObjectClazz = JobApplicationReview.class, contextObjectClazz = JobApplication.class, actionType = RequiresAccessControl.ActionType.LISTING)
    public List<JobApplicationReviewDTO> getJobApplicationReviews(Long agentId, Long roleAssignmentId, Long jobApplicationId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get JobApplicationReviews
        List<JobApplicationReview> jobApplicationReviews = this.persistenceService.findWithNamedQuery(JobApplicationReview.QUERY__BY__JOB_APPLICATION, QueryParameter.with("jobApplicationId", jobApplicationId).parameters());

        // map JobApplicationReviews
        List<JobApplicationReviewDTO> jobApplicationReviewDTOList = new LinkedList<>();
        jobApplicationReviews.forEach(jobApplicationReview ->  jobApplicationReviewDTOList.add(new JobApplicationReviewDTO(jobApplicationReview)));

        // return JobApplicationReviews
        return jobApplicationReviewDTOList;
    }

    @RequiresAccessControl(targetObjectClazz = ContractOffer.class, contextObjectClazz = RecruitmentProcess.class, actionType = RequiresAccessControl.ActionType.ADD)
    public ContractOfferDTO addContractOffer(Long agentId, Long roleAssignmentId, Long recruitmentProcessId, ContractOfferDTO contractOfferDTO, Long applicantId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get RecruitmentProcess
        RecruitmentProcess recruitmentProcess = this.persistenceService.get(RecruitmentProcess.class, recruitmentProcessId);

        // create ContractOffer
        ContractOffer contractOffer = new ContractOffer(agent, contractOfferDTO.getContractOfferText(), recruitmentProcess);

        // persist ContractOffer
        contractOffer = this.persistenceService.persist(contractOffer);

        // assign applicant to contract offer via the role "Candidate"
        List<Role> roles = this.persistenceService.findWithNamedQuery(Role.QUERY__ASSIGNMENT_DEFAULT__TARGET_CLAZZ, QueryParameter.with("targetEntityClazz", ContractOffer.class).and("onAssignmentDefault", true).parameters());
        if (roles.size() != 1){
            throw new ControllerException("There is an error in the role management. Please contact an administrator!");
        }
        RoleAssignmentDTO roleAssignmentDTO = new RoleAssignmentDTO();
        roleAssignmentDTO.setRoleId(roles.get(0).getId());
        Set<Long> agentIds = new LinkedHashSet<>();
        agentIds.add(applicantId);
        roleAssignmentDTO.setAgentIds(agentIds);
        this.authorizationService.addRoleAssignment(agentId, roleAssignmentId, this.systemService.getHumanResourceSystemId(), roleAssignmentDTO, contractOffer.getId());

        // return ContractOffer
        return new ContractOfferDTO(contractOffer);
    }

    @RequiresAccessControl(targetObjectClazz = ContractOffer.class, contextObjectClazz = ContractOffer.class, actionType = RequiresAccessControl.ActionType.READ)
    public ContractOfferDTO getContractOffer(Long agentId, Long roleAssignmentId, Long contractOfferId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get ContractOffer
        ContractOffer contractOffer = this.persistenceService.get(ContractOffer.class, contractOfferId);

        // return ContractOffer
        return new ContractOfferDTO(contractOffer);
    }

    @RequiresAccessControl(targetObjectClazz = ContractOffer.class, contextObjectClazz = ContractOffer.class, actionType = RequiresAccessControl.ActionType.UPDATE)
    public ContractOfferDTO updateContractOffer(Long agentId, Long roleAssignmentId, Long contractOfferId, ContractOfferDTO contractOfferDTO) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get ContractOffer
        ContractOffer contractOffer = this.persistenceService.get(ContractOffer.class, contractOfferId);

        // Update contractOfferText if necessary
        if (contractOfferDTO.getContractOfferText() != null){
            contractOffer.setContractOfferText(contractOfferDTO.getContractOfferText());
        }

        // return ContractOffer
        return new ContractOfferDTO(contractOffer);
    }

    @RequiresAccessControl(targetObjectClazz = ContractOffer.class, contextObjectClazz = RecruitmentProcess.class, actionType = RequiresAccessControl.ActionType.ADD)
    public void removeContractOffer(Long agentId, Long roleAssignmentId, Long contractOfferId, Long recruitmentProcessId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Agent
        Agent agent = this.persistenceService.get(Agent.class, agentId);

        // get ContractOffer
        ContractOffer contractOffer = this.persistenceService.get(ContractOffer.class, contractOfferId);

        // get RecruitmentProcess
        RecruitmentProcess recruitmentProcess = contractOffer.getRecruitmentProcess();

        // sanity check
        if (!recruitmentProcessId.equals(recruitmentProcess.getId())){
            throw new ControllerException("The given RecruitmentProcess does not contain the given ContractOffer!");
        }

        // remove ContractOffer from RecruitmentProcess
        recruitmentProcess.removeContractOffer(contractOffer);
        this.persistenceService.delete(ContractOffer.class, contractOfferId);
    }

    @RequiresAccessControl(targetObjectClazz = ContractOffer.class, contextObjectClazz = RecruitmentProcess.class, actionType = RequiresAccessControl.ActionType.LISTING)
    public List<ContractOfferDTO> getContractOffers(Long agentId, Long roleAssignmentId, Long recruitmentProcessId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get ContractOffers
        List<ContractOffer> contractOffers = this.persistenceService.findWithNamedQuery(ContractOffer.QUERY__BY__RECRUITMENT_PROCESS, QueryParameter.with("recruitmentProcessId", recruitmentProcessId).parameters());

        List<ContractOfferDTO> contractOfferDTOList = new LinkedList<>();
        contractOffers.forEach(contractOffer -> contractOfferDTOList.add(new ContractOfferDTO(contractOffer)));

        return contractOfferDTOList;
    }

}
