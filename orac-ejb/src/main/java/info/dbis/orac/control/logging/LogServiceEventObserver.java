package info.dbis.orac.control.logging;

import info.dbis.orac.datamodel.logging.PersistenceLogEvent;
import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.AuthorizationException;
import info.dbis.orac.exception.ControllerException;
import info.dbis.orac.exception.PersistenceException;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;

/**
 * LogServiceEventObserverImpl
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Singleton
public class LogServiceEventObserver {

    @NotNull
    private LogService logService;

    @Inject
    private void setLogService(LogService logService) {
        this.logService = logService;
    }


    @Lock(LockType.READ)
    public void observePersistenceLogEvent(@Observes(during = TransactionPhase.AFTER_COMPLETION) PersistenceLogEvent persistenceLogEvent) throws AuthenticationException, AuthorizationException, PersistenceException, ControllerException {
        this.logService.processPersistenceLogEvent(persistenceLogEvent);
    }
}
