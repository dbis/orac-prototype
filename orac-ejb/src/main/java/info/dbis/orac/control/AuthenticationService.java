package info.dbis.orac.control;

import com.lambdaworks.crypto.SCryptUtil;
import info.dbis.orac.common.QueryParameter;
import info.dbis.orac.datamodel.authentication.User;
import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

/**
 * AuthenticationService
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Stateless
public class AuthenticationService {

    @Inject
    private PersistenceService persistenceService;

    @Inject
    private AuthenticationServiceCache authenticationServiceCache;

    /**
     * Authenticates a user based on email and password
     *
     * @param email
     * @param password
     * @throws AuthenticationException
     * @throws PersistenceException
     */
    public void authenticate(String email, String password) throws AuthenticationException, PersistenceException {

        if (email == null || "".equals(email)) {
            throw new AuthenticationException("Invalid or empty Email");
        }
        // identify account for login procedure
        List<User> users = persistenceService.findWithNamedQuery(User.QUERY__EMAIL, QueryParameter.with("email", email).parameters(),1);
        if (users.size() == 1){
            User user = users.get(0);
            if (SCryptUtil.check(password, user.getPassword())){
                this.authenticationServiceCache.setUser(user);
            }
            else {
                throw new AuthenticationException("Wrong password!");
            }
        }
        else
            throw new AuthenticationException("No user found");
    }

    /**
     * Checks if a user is authenticated
     *
     * @return isAuthenticated
     */
    public boolean isAuthenticated() {
        return this.authenticationServiceCache.getUser() != null;
    }

    /**
     * Returns id of current user
     *
     * @return usedId
     * @throws PersistenceException
     * @throws AuthenticationException
     */
    public Long getCurrentUserId() throws PersistenceException, AuthenticationException {
        if (this.authenticationServiceCache.getUser() != null){
            return this.authenticationServiceCache.getUser().getId();
        }
        throw new AuthenticationException("User is not authenticated!");
    }

}
