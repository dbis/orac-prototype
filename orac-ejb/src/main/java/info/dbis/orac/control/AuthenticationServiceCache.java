package info.dbis.orac.control;

import info.dbis.orac.datamodel.authentication.User;

import javax.enterprise.context.RequestScoped;

/**
 * AuthenticationServiceCache to cache user
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */
@RequestScoped
public class AuthenticationServiceCache {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
