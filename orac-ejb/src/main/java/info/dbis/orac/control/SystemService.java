package info.dbis.orac.control;

import com.lambdaworks.crypto.SCryptUtil;
import info.dbis.orac.common.QueryParameter;
import info.dbis.orac.datamodel.HumanResourceSystem;
import info.dbis.orac.datamodel.authentication.Agent;
import info.dbis.orac.datamodel.authentication.User;
import info.dbis.orac.datamodel.authorization.*;
import info.dbis.orac.datamodel.dto.HumanResourceSystemDTO;
import info.dbis.orac.datamodel.dto.authentication.UserDTO;
import info.dbis.orac.exception.AuthenticationException;
import info.dbis.orac.exception.AuthorizationException;
import info.dbis.orac.exception.ControllerException;
import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;

/**
 * SystemService
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Stateless
public class SystemService {

    @Inject
    private PersistenceService persistenceService;

    @Inject
    private BootstrappingService bootstrappingService;

    public long getHumanResourceSystemId() {
        return this.bootstrappingService.getSystemId();
    }

    @RequiresAccessControl(targetObjectClazz = HumanResourceSystem.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.READ)
    public HumanResourceSystemDTO getHumanResourceSystem(Long agentId, Long roleAssignmentId, Long humanResourceSystemId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get HumanResourceSystems
        HumanResourceSystem humanResourceSystem = this.persistenceService.get(HumanResourceSystem.class, humanResourceSystemId);

        return new HumanResourceSystemDTO(humanResourceSystem);
    }

    @RequiresAccessControl(targetObjectClazz = User.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.ADD)
    public UserDTO addUser(Long agentId, Long roleAssignmentId, Long humanResourceSystemId, UserDTO userDTO) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get creator
        Agent creator = this.persistenceService.get(Agent.class, agentId);

        // get HumanResourceSystems
        HumanResourceSystem humanResourceSystem = this.persistenceService.get(HumanResourceSystem.class, humanResourceSystemId);

        // create User
        User user = new User(creator, userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(), userDTO.isMale(), userDTO.getTitle(), userDTO.getLocation(),
                userDTO.getPhone(), userDTO.getWebsite(), userDTO.hasNewsletterSubscription(), userDTO.getBiography(), humanResourceSystem);

        // set decent password
        String generatedSecuredPasswordHashMaxMueller = SCryptUtil.scrypt(userDTO.getPassword(), 16, 16, 16);
        user.setPassword(generatedSecuredPasswordHashMaxMueller);

        // persist user
        user = persistenceService.persist(user);

        // create RoleAssignment to default Role
        List<Role> roles = this.persistenceService.findWithNamedQuery(Role.QUERY__ASSIGNMENT_DEFAULT__TARGET_CLAZZ, QueryParameter.with("targetEntityClazz", HumanResourceSystem.class).and("onAssignmentDefault", true).parameters());
        if (roles.size() != 1){
            throw new ControllerException("There is an error in the access control. Please contact an administrator!");
        }
        // get additional RoleScope
        RoleScope addRoleScope = null;
        for (RoleScope roleScope : roles.get(0).getAdditionalRoleScopes()){
            if (roleScope.getTargetEntityClazz().equals(User.class)){
                addRoleScope = roleScope;
                break;
            }
        }

        // Create and persist RoleAssignment and RoleScopeAssignments
        RoleAssignment userSystemRoleAssignment = new RoleAssignment(creator, humanResourceSystem, roles.get(0), user);
        RoleScopeAssignment rsaSystemUserKeyScopeRoleSystem = new RoleScopeAssignment(creator, userSystemRoleAssignment, roles.get(0).getKeyRoleScope(), humanResourceSystem);
        RoleScopeAssignment rsaSystemUserAddScopeRoleSystem = new RoleScopeAssignment(creator, userSystemRoleAssignment, addRoleScope, user);

        this.persistenceService.persist(userSystemRoleAssignment);
        this.persistenceService.persist(rsaSystemUserKeyScopeRoleSystem);
        this.persistenceService.persist(rsaSystemUserAddScopeRoleSystem);

        // Map entity to DTO and return
        return new UserDTO(user);
    }

    @RequiresAccessControl(targetObjectClazz = User.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.READ)
    public UserDTO getUser(Long agentId, Long roleAssignmentId, Long userId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get User
        User user = this.persistenceService.get(User.class, userId);

        return new UserDTO(user);
    }


    @RequiresAccessControl(targetObjectClazz = User.class, contextObjectClazz = HumanResourceSystem.class, actionType = RequiresAccessControl.ActionType.LISTING)
    public List<UserDTO> getUsers(Long agentId, Long roleAssignmentId, Long humanResourceSystemId) throws PersistenceException, ControllerException, AuthenticationException, AuthorizationException {
        // get Users
        List<User> users = this.persistenceService.findWithNamedQuery(User.QUERY__ALL);

        List<UserDTO> userDTOList = new LinkedList<>();
        users.forEach(user -> userDTOList.add(new UserDTO(user)));

        return userDTOList;
    }


}
