package info.dbis.orac.exception;

import info.dbis.orac.common.ORACGlobalConstants;

/**
 * AuthenticationException
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

public class AuthenticationException extends Exception  {

    private static final long serialVersionUID = ORACGlobalConstants.SERIAL_VERSION;

    public AuthenticationException() {
        super();
    }

    public AuthenticationException(String message) {
        super(message);
    }

    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }
}
