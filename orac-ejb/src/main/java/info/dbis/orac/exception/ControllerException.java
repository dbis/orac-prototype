package info.dbis.orac.exception;

import info.dbis.orac.common.ORACGlobalConstants;

/**
 * ControllerException
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

public class ControllerException extends Exception {

    private static final long serialVersionUID = ORACGlobalConstants.SERIAL_VERSION;

    public ControllerException() {
        super();
    }

    public ControllerException(String message) {
        super(message);
    }

    public ControllerException(String message, Throwable cause) {
        super(message, cause);
    }
}
