package info.dbis.orac.exception;

import info.dbis.orac.common.ORACGlobalConstants;

/**
 * AuthenticationException
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

public class AuthorizationException extends Exception  {

    private static final long serialVersionUID = ORACGlobalConstants.SERIAL_VERSION;

    public AuthorizationException() {
        super();
    }

    public AuthorizationException(String message) {
        super(message);
    }

    public AuthorizationException(String message, Throwable cause) {
        super(message, cause);
    }
}
