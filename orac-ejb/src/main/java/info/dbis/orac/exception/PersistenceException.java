package info.dbis.orac.exception;

import info.dbis.orac.common.ORACGlobalConstants;

/**
 * PersistenceException to log persistence problems properly
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

public class PersistenceException extends Exception {

    private static final long serialVersionUID = ORACGlobalConstants.SERIAL_VERSION;
    private final ErrorCode code;

    /**
     * Creates a new PersistenceException.
     *
     *
     */
    public PersistenceException() {
        super();
        this.code = ErrorCode.UNKNOWN;
    }

    /**
     * Creates a new PersistenceException.
     *
     * @param errorCode
     *            Error code to more precisely specify the cause of the problem.
     */
    public PersistenceException(final ErrorCode errorCode) {
        super();
        this.code = errorCode;
    }

    /**
     * Creates a new PersistenceException.
     *
     * @param message
     *            message that describes the exceptional case
     * @param cause
     *            {@link Throwable} that caused the problem
     */
    public PersistenceException(final String message, final Throwable cause) {
        super(message, cause);
        this.code = ErrorCode.UNKNOWN;
    }

    /**
     * Creates a new PersistenceException.
     *
     * @param message
     *            message that describes the exceptional case
     */
    public PersistenceException(final String message) {
        super(message);
        this.code = ErrorCode.UNKNOWN;
    }

    /**
     * Creates a new PersistenceException.
     *
     * @param cause
     *            {@link Throwable} that caused the problem
     */
    public PersistenceException(final Throwable cause) {
        super(cause);
        this.code = ErrorCode.UNKNOWN;
    }

    /**
     * Creates a new PersistenceException.
     *
     * @param message
     *            the detail message. The detail message is saved for later retrieval by the getMessage() method.
     * @param errorCode
     *            the error code for this exception
     */
    public PersistenceException(final String message, final ErrorCode errorCode) {
        super(message);
        this.code = errorCode;
    }

    /**
     * @return the error code associated with this exception.
     */
    public ErrorCode getCode() {
        return code;
    }

    /**
     * Error code indicating the exact cause of the problem.
     *
     */
    public enum ErrorCode {
        /** Indicates that an unknown error occurred. */
        UNKNOWN("An unknown error occurred. Please inspect the log file for further information."),

        /** Indicates a general problem with the persistence layer. **/
        DATA_PERSISTENCE_PROBLEM("A general error with data persistence occurred. Please inspect the log file for further information."),

        /** Indicates that no matching data set(s) could be found. **/
        DATA_NOT_FOUND("No matching data set(s) could be found - please check provided identifiers against data available in data store"),

        /** Indicates that not all mandatory data was provided. **/
        DATA_INCOMPLETE("Not all mandatory data was provided - please check!"),

        /** Indicates that some or all provided data was invalid. **/
        DATA_INVALID("Some or all provided data was invalid - please check!");

        private final String message;

        /**
         * @param msg
         *            The message to be displayed.
         */
        ErrorCode(final String msg) {
            this.message = msg;
        }

        /**
         * @return the message
         */
        public String getMessage() {
            return message;
        }

    }

}
