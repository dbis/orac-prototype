package info.dbis.orac.common;

/**
 * Created by Nicolas on 05.12.2014.
 */
public final class ORACGlobalConstants {

    /**
     * contains the serial version UID used for all classes which need it.
     * Should be changed before each (major) release of the ORAC prototype.
     */
    public static final long SERIAL_VERSION = 1L;

    /**
     * denote the base urls of the ORAC server and client
     * both should be transferred to a configuration file later
     */
    public static final String SERVER_BASE_URL = "http://localhost:8080";
    public static final String CLIENT_BASE_URL = "http://localhost:9000/app";

    /**
     * email settings required for sending notifications to users
     * should be transferred to a configuration file later
     */

    public static final String EMAIL_HOST_NAME = "v140660.kasserver.com";
    public static final int EMAIL_PORT = 465;
    public static final String EMAIL_USER_NAME = "m03be718";
    public static final String EMAIL_PASSWORD = "32h92oYSxtQduYVN";
    public static final String EMAIL_FROM_ADDRESS = "no-reply@procollab.de";

    /**
     * Utility class should not be instantiated and thus needs a private default
     * constructor.
     */
    private ORACGlobalConstants() {

    }
}
