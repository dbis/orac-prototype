package info.dbis.orac.model.impl;


import info.dbis.orac.datamodel.authorization.GuardedEntity;
import info.dbis.orac.datamodel.logging.PersistenceLogEvent;
import info.dbis.orac.datamodel.logging.PersistenceLogEventType;

import javax.enterprise.event.Event;
import javax.enterprise.util.TypeLiteral;
import javax.persistence.*;

/**
 * EntityListener
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

public class EntityListener {

    @PrePersist
    void prePersist(Object object) {
    }

    @PreUpdate
    void preUpdate(Object object) {
    }

    @PreRemove
    void preRemove(Object object) {
    }

    @PostPersist
    void postPersist(Object object) {
    }

    @PostLoad
    void postLoad(Object object) {

    }

    @PostUpdate
    public void postUpdate(Object object) {
    }

    @PostRemove
    void postRemove(Object object) {
        if (object instanceof GuardedEntity) {
            try {
                // Get Event<GuardedEntity> via CDI workaround [WFLY-2387](https://issues.jboss.org/browse/WFLY-2387)
                TypeLiteral<Event<PersistenceLogEvent>> type = new TypeLiteral<Event<PersistenceLogEvent>>() {};
                Event<PersistenceLogEvent> event = javax.enterprise.inject.spi.CDI.current().select(type).get();

                // Fire event
                Class clazz = object.getClass();
                GuardedEntity guardedEntity = (GuardedEntity) object;
                PersistenceLogEvent persistenceLogEvent = new PersistenceLogEvent(guardedEntity.getId(), clazz, PersistenceLogEventType.REMOVED);
                event.fire(persistenceLogEvent);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


}
