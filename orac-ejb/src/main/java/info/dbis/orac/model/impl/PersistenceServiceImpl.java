package info.dbis.orac.model.impl;

import info.dbis.orac.exception.PersistenceException;
import info.dbis.orac.model.api.PersistenceService;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 * PersistenceServiceImpl
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PersistenceServiceImpl implements PersistenceService {

    @Inject
    private Logger logger;

    @PersistenceContext(unitName = "oracPrototypeDS")
    protected EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public <T> T get(Class<T> type, Object id) throws PersistenceException {
        try {
            Object obj = this.entityManager.find(type, id);
            if (obj == null)
                throw new PersistenceException(PersistenceException.ErrorCode.DATA_NOT_FOUND);
            else
                return (T) obj;
        }
        catch (IllegalArgumentException e){
            e.printStackTrace();
            throw new PersistenceException(PersistenceException.ErrorCode.DATA_INVALID);
        }
    }

    public <T> T persist(T t) throws PersistenceException {
        try {
            this.entityManager.persist(t);
            this.entityManager.flush();
            this.entityManager.refresh(t);
            return t;
        }
        catch(EntityExistsException e){
            e.printStackTrace();
            throw new PersistenceException(PersistenceException.ErrorCode.DATA_PERSISTENCE_PROBLEM);
        }
        catch(IllegalArgumentException e){
            e.printStackTrace();
            throw new PersistenceException(PersistenceException.ErrorCode.DATA_INVALID);
        }
        catch(TransactionRequiredException e){
            throw new PersistenceException("Transaction required!", e);
        }
    }

    public <T> T update(T t) throws PersistenceException {
        try {
            T updatedT = this.entityManager.merge(t);
            //this.entityManager.flush();
            //this.entityManager.refresh(t);
            return updatedT;
        }
        catch(IllegalArgumentException e){
            e.printStackTrace();
            throw new PersistenceException(PersistenceException.ErrorCode.DATA_INVALID);
        }
        catch(TransactionRequiredException e){
            throw new PersistenceException("Transaction required!", e);
        }

    }

    public void delete(Class type, Object id) throws PersistenceException {
        try {
            Object ref = this.entityManager.getReference(type, id);
            this.entityManager.remove(ref);
        }
        catch(IllegalArgumentException e){
            e.printStackTrace();
            throw new PersistenceException(PersistenceException.ErrorCode.DATA_INVALID);
        }
        catch(EntityNotFoundException e){
            e.printStackTrace();
            throw new PersistenceException(PersistenceException.ErrorCode.DATA_NOT_FOUND);
        }
        catch(TransactionRequiredException e){
            throw new PersistenceException("Transaction required!", e);
        }
    }

    public <T> List<T> findWithNamedQuery(String namedQueryName) throws PersistenceException {
        return this.entityManager.createNamedQuery(namedQueryName).getResultList();
    }

    public <T> List<T> findWithNamedQuery(String namedQueryName, Map<String,Object> parameters) throws PersistenceException {
        return findWithNamedQuery(namedQueryName, parameters, 0);
    }

    public <T> List<T> findWithNamedQuery(String namedQueryName, int resultLimit) throws PersistenceException {
        return this.entityManager.createNamedQuery(namedQueryName).
                setMaxResults(resultLimit).
                getResultList();
    }

    public <T> List<T> findWithNamedQuery(String namedQueryName, Map<String,Object> parameters, int resultLimit) throws PersistenceException {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.entityManager.createNamedQuery(namedQueryName);
        if(resultLimit > 0)
            query.setMaxResults(resultLimit);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        return query.getResultList();
    }

    public <T> List<T> findByParameters(Class<T> type, Map<String,Object> parametersEqual, Map<String,Object> parametersLike) throws PersistenceException {
        return this.findByParameters(type, parametersEqual, parametersLike, -1);
    }

    public <T> List<T> findByParameters(Class<T> type, Map<String,Object> parametersEqual, Map<String,Object> parametersLike, int resultLimit) throws PersistenceException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        Metamodel m = entityManager.getMetamodel();
        EntityType<T> eT = m.entity(type);
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);

        List<Predicate> predicates = new ArrayList<>();
        parametersEqual.entrySet().forEach(parameterEqual -> {
            predicates.add(criteriaBuilder.equal(root.get(eT.getDeclaredAttribute(parameterEqual.getKey()).getName()), parameterEqual.getValue()));
        });

        parametersLike.entrySet().forEach(parameterLike -> {
            predicates.add(criteriaBuilder.equal(root.get(eT.getDeclaredAttribute(parameterLike.getKey()).getName()), "%"+parameterLike.getValue()+"%"));
        });

        criteriaQuery.where(predicates.toArray(new Predicate[]{}));
        TypedQuery<T> typedQuery;
        if (resultLimit > 0){
            typedQuery = entityManager.createQuery(criteriaQuery).setMaxResults(resultLimit);
        }
        else {
            typedQuery = entityManager.createQuery(criteriaQuery);
        }

        return typedQuery.getResultList();
    }

    public <T> List<T> findByNativeQuery(String sql, Class type) throws PersistenceException {
        return this.entityManager.createNativeQuery(sql, type).getResultList();
    }

    public EntityManager getEntityManager(){
        return this.entityManager;
    }
}
