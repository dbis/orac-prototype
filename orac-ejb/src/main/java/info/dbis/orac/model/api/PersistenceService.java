package info.dbis.orac.model.api;

import info.dbis.orac.exception.PersistenceException;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;

/**
 * PersistenceService
 *
 * @author Nicolas Mundbrod <nicolas.mundbrod@uni-ulm.de>
 */

@Local
public interface PersistenceService {

    // CRUD
    <T> T get(Class<T> type, Object id) throws PersistenceException;
    <T> T persist(T t) throws PersistenceException;
    <T> T update(T t) throws PersistenceException;
    void delete(Class type, Object id) throws PersistenceException;

    // Queries
    <T> List<T> findWithNamedQuery(String namedQueryName) throws PersistenceException;
    <T> List<T> findWithNamedQuery(String namedQueryName, int resultLimit) throws PersistenceException;
    <T> List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) throws PersistenceException;
    <T> List<T> findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) throws PersistenceException;
    <T> List<T> findByParameters(Class<T> type, Map<String, Object> parametersEqual, Map<String, Object> parametersLike) throws PersistenceException;
    <T> List<T> findByParameters(Class<T> type, Map<String, Object> parametersEqual, Map<String, Object> parametersLike, int resultLimit) throws PersistenceException;
    <T> List<T> findByNativeQuery(String sql, Class type) throws PersistenceException;

    // provide EntityManager in case of it is required to (should be the exception)
    EntityManager getEntityManager();
}
