# ORAC Prof-of-Concept Prototype #

This is the proof-of-concept prototype of the Object-specific Role-based Access Control (ORAC) approach. As soon as the paper is published, we will add the link to the paper. Please do not use the code of the ORAC prototype as a basis for any productive sytem.

## Technologies and Technical Architecture ##

The ORAC prototype is developed based on Java EE 7 platform and requires the JBoss Wildfly Application Server (10.x) for execution (you do not need to manually install Wildlfy however; see below). Furthermore, we used Maven for dependency management. In particular, the prototype was designed as a modular maven project based on MVC (model, view, control):

- orac-parent
- orac-ear
- orac-ejb
- orac-webapp

The first module (orac-parent) is used as the Maven parent module to define dependencies, properties and plugins shared by all other modules. The module orac-ear bundles the modules orac-ejb and orac-webapp in a ear archive that is deployable in a Java application server. The module orac-ejb contains the demo application and the proof-of-concept code of the ORAC approach. For ease of use, the classes are separated in meaningful packages, e.g., one for the data model, one for the persistence or one for the actual control logic. Finally, the module orac-webapp contains REST-based service to allow for the interaction with the prototype. 

## Starting the ORAC prototype ##

Importantly, we strongly recommend to first read our paper about Object-specific Role-based Access Control before regarding this prototype. A good understanding of the ORAC approach is crucial to deal with the prototype in any sense. 

- First, check out this GIT repository and to have a look on the source code in you favorite IDE (e.g., IntelliJ, Eclipse, Netbeans).
- You may run the prototype with maven. Therefore, go to orac-parent folder and execute mvn clean install. Then switch to the orac-ear folder and execute mvn cargo:run. Using Maven plugins, you may execute these commands from any  
- Based on the Maven commands, a JBoss Wildfly Application Server (10.x) is automatically downloaded, installed and used for the ORAC prototype. There is no need to manually install Wildfly. However, if you already have installed Wildfly 10.x on your computer, you of course may deploy the ORAC prototype on this instance instead.

## Using the ORAC prototype ##

At the end of the deployment process of the ORAC prototype, its BootstrappingService is automatically called to set up all necessary entities and services. You may see the output of the BootstrappingService in the terminal. 

After the prototype has been successfully booted, you may create the human resource use case:

- Call the POST REST service http://localhost:8080/orac/rest/system/useCase/ with the credentials of the demo user John Smith (username: john.smith@dbis.info; password: password) and the query parameter roleAssignmentId=602. In doing so, John Smith creates the entire use case in the role "HR Manager" in relation to the human resource system.
- As a result, you retrieve the relevant information of the entire use case encoded in JSON for your consideration. 
- You may then arbitrarily call services provided by the REST endpoints (see the package info.dbis.orac.view.rest in the orac-webapp). For example, you may call the GET REST service http://localhost:8080/orac/rest/system/recruitmentProcess/{recruitmentProcessId} with credentials of Max Mueller and his roleAssignmentId for the role HR employee denoted in the received use case information. Of course, you need to set the given recruitmentProcessId as well.
- To inspect ORAC more closely, please consider the class AccessControlInterceptor and use the debugging mode of your preferred IDE to evaluate the implemented algorithms at run time. 
